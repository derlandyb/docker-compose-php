# Ambiente de desenvolvimento PHP/Apache/MySQL baseado em Docker

Ambiente criado utilizando o Docker e o Docker Compose para criar containers integrados de desenvolvimento.

## Imagens utilizadas
[MySQL:5.7](https://hub.docker.com/_/mysql)

[php:7.2-apache](https://hub.docker.com/_/php)

## Antes de executar
Dentro do diretório database crie uma pasta dados. Esta pasta será usada pelo mysql para guardar os arquivos de banco de dados.

Se os seus projetos php estão em um diretório diferente de `/var/www/html`

Crie um arquivo chamado `docker-compose.override.yml` com a seguinte extrutura:
```
version: '3'
services:
  php:
    volumes: 
      - <your_project_path_here>:/var/www/html
``` 
Substitua `<your_project_path_here>` pelo caminho do diretório dos seu projetos PHP.

Se o apache e o mysql estiverem sendo executados na sua máquina, será preciso parar os serviços antes de executar o docker.

## Como usar

Instale o docker e o docker-compose de acordo com o seu SO seguindo as instruções da [documentação](https://docs.docker.com/install/).

Execute o comando: `docker-compose up -d` para rodar em background

Execute o comando: `docker-compose up` se quiser acompanhar o log gerado no processo de build das imagens e containers