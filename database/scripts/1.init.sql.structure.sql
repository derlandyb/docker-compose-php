-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema thweb_homologacao
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema thweb_homologacao
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `thweb_homologacao` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema thweb_homologacao
-- -----------------------------------------------------
USE `thweb_homologacao` ;

-- -----------------------------------------------------
-- Table `thweb_homologacao`.`aceitacao_termo_uso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`aceitacao_termo_uso` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `localizador` VARCHAR(150) NULL DEFAULT NULL,
  `matricula` VARCHAR(150) NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `token` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`acreditacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`acreditacao` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `cpf_cnpj` VARCHAR(20) NOT NULL,
  `id_especialidade` VARCHAR(10) NOT NULL,
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `resumo` VARCHAR(500) NOT NULL,
  `detalhamento` VARCHAR(500) NULL DEFAULT NULL,
  `data_inicial` DATETIME NULL DEFAULT NULL,
  `data_final` DATETIME NULL DEFAULT NULL,
  `codigo` VARCHAR(10) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxPrincipal` (`id_operadora` ASC, `cpf_cnpj` ASC, `id_especialidade` ASC, `sequencial_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`acreditacao_tmp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`acreditacao_tmp` (
  `id_operadora` INT(11) NOT NULL,
  `cpf_cnpj` VARCHAR(20) NOT NULL,
  `id_especialidade` VARCHAR(10) NOT NULL,
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `resumo` VARCHAR(50) NOT NULL,
  `detalhamento` VARCHAR(500) NULL DEFAULT NULL,
  `data_inicial` DATETIME NULL DEFAULT NULL,
  `data_final` DATETIME NULL DEFAULT NULL,
  `codigo` VARCHAR(10) NULL DEFAULT NULL,
  `operacao` INT(1) NOT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL)
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_painel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_painel` (
  `id_adm_painel` INT(11) NOT NULL AUTO_INCREMENT,
  `identificacao` VARCHAR(75) NOT NULL,
  `homologacao` TINYINT(1) NULL DEFAULT NULL,
  `producao` TINYINT(1) NULL DEFAULT NULL,
  `key` VARCHAR(50) NULL DEFAULT NULL,
  `ordem` TINYINT(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id_adm_painel`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_modulo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_modulo` (
  `id_adm_modulo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_adm_painel` INT(11) NOT NULL,
  `nome` VARCHAR(120) NOT NULL,
  `key` VARCHAR(50) NULL DEFAULT NULL,
  `ordem` TINYINT(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id_adm_modulo`),
  INDEX `fk_adm_modulo_adm_painel1_idx` (`id_adm_painel` ASC) VISIBLE,
  CONSTRAINT `fk_adm_modulo_adm_painel1`
    FOREIGN KEY (`id_adm_painel`)
    REFERENCES `thweb_homologacao`.`adm_painel` (`id_adm_painel`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_modulo_recurso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_modulo_recurso` (
  `id_adm_modulo_recurso` INT(11) NOT NULL AUTO_INCREMENT,
  `id_adm_modulo` INT(11) NOT NULL,
  `nome` VARCHAR(145) NOT NULL,
  `key` VARCHAR(50) NULL DEFAULT NULL,
  `ordem` TINYINT(3) NULL DEFAULT '0',
  PRIMARY KEY (`id_adm_modulo_recurso`),
  INDEX `fk_adm_modulo_recurso_adm_modulo1_idx` (`id_adm_modulo` ASC) VISIBLE,
  CONSTRAINT `fk_adm_modulo_recurso_adm_modulo1`
    FOREIGN KEY (`id_adm_modulo`)
    REFERENCES `thweb_homologacao`.`adm_modulo` (`id_adm_modulo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_painel_funcionalidade_extra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_painel_funcionalidade_extra` (
  `id_adm_painel_funcionalidade_extra` INT(11) NOT NULL AUTO_INCREMENT,
  `id_adm_painel` INT(11) NULL DEFAULT NULL,
  `identificacao` VARCHAR(45) NOT NULL,
  `valor_padrao` VARCHAR(255) NOT NULL DEFAULT '0',
  `obrigatorio` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_adm_painel_funcionalidade_extra`),
  INDEX `fk_painel_funcionalidade_extra_1_idx` (`id_adm_painel` ASC) VISIBLE,
  CONSTRAINT `fk_adm_painel_funcionalidade_extra_adm_painel`
    FOREIGN KEY (`id_adm_painel`)
    REFERENCES `thweb_homologacao`.`adm_painel` (`id_adm_painel`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`operadoras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`operadoras` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `codigo_legado` VARCHAR(4) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `descricao` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `endereco` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `numero` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `complemento` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `id_cidades` INT(10) NOT NULL,
  `id_estados` INT(10) NOT NULL,
  `telefone_primario` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `telefone_secundario` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `site_url` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `nome_logomarca` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `data_inclusao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_bloqueio` TIMESTAMP NULL DEFAULT NULL,
  `usuario_acesso` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `data_ultimo_acesso` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_ultimo_acesso` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `distancia` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `razao_social` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `nome_fantasia` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `cnpj` VARCHAR(18) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `cep` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `ans` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `senha` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `direciona` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `cor_background` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `cor_tooltip_rolagem` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_cor` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_cor_custom1` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_cor_custom2` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_marca` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_marca_file` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_marca_top` INT(10) NOT NULL,
  `pers_marca_left` INT(10) NOT NULL,
  `pers_topo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_topo_file` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_topo_altura` INT(11) NOT NULL,
  `pers_topo_repeat` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_botao` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_titulo_princ` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_titulo_sec` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_tipo_inclusao` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `pers_tipo_aba` INT(1) NOT NULL,
  `pers_html` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `iframe_personalizado` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `iframe_largura` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `iframe_altura` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `iframe_url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `subdominio` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `link_appstore` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `link_googleplay` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_plano` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_estado` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_municipio` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_bairro` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_tipo_prestador` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_especialidade` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_nome` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `label_cep` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `url_amigavel` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_icon` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_icon4` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_startup_screen` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_topo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_topo_interno` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_bg_url` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mobile_bg_color` VARCHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `inscricao_estadual` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `email_inconsistencias` VARCHAR(250) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `numero_ans` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `facebook` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `twitter` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `hash_manutencao` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `noticia_html` INT(11) NULL DEFAULT NULL,
  `link_share` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `versao` INT(11) NOT NULL,
  `versao_app` INT(3) NOT NULL,
  `utiliza_secoes` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `exibir_legenda` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `agrupa_espec` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `box_shadow` TINYINT(1) NOT NULL,
  `site_cliente` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `exibe_tela_webapp` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `exibir_tooltip_rolagem` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `force_especialidade` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `exibir_busca_nome` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `exibir_nome_cr` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `procedure_pos_carga_prestador` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `procedure_pos_carga_acreditacao` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `procedure_pos_carga_corpo_clinico` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `formatacao_texto` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `delimitador_importador` VARCHAR(3) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT ';',
  `mostra_data_rede_credenciada` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'N',
  `busca_medicamentos` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `link_area_cliente` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_area_credenciado` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_area_empresa` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_facebook` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_google_plus` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_twitter` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_linkedin` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_instagram` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `link_youtube` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `horario_atendimento` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `telefone_contato_primario` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `telefone_contato_secundario` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `rede_descredenciada_url` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `utiliza_mensageria` TINYINT(4) NOT NULL,
  `range_protocolo_de` INT(11) NULL DEFAULT NULL,
  `range_protocolo_ate` INT(11) NULL DEFAULT NULL,
  `id_cliente_grupo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `codigo` (`codigo_legado` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_perfil` (
  `id_adm_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `observacao` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(2) NOT NULL DEFAULT '1',
  `producao` TINYINT(1) NULL DEFAULT '1',
  `homologacao` TINYINT(1) NULL DEFAULT '1',
  `data_inclusao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `permissoes` JSON NULL DEFAULT NULL,
  PRIMARY KEY (`id_adm_perfil`),
  INDEX `fk_adm_perfil_operadoras1_idx` (`id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_adm_perfil_operadoras1`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_tipo_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_tipo_usuario` (
  `id_adm_tipo_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(47) NULL DEFAULT NULL,
  PRIMARY KEY (`id_adm_tipo_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cliente_grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cliente_grupo` (
  `id_cliente_grupo` INT(11) NOT NULL AUTO_INCREMENT,
  `identificacao` VARCHAR(145) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente_grupo`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_usuario` (
  `id_adm_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(245) NOT NULL,
  `email` VARCHAR(245) NOT NULL,
  `telefone` VARCHAR(12) NULL DEFAULT NULL,
  `ramal` VARCHAR(5) NULL DEFAULT NULL,
  `senha` VARCHAR(140) NOT NULL,
  `hash_recuperar_senha` VARCHAR(140) NULL DEFAULT NULL,
  `avatar` VARCHAR(245) NULL DEFAULT NULL,
  `validade_hash_recuperar_senha` DATETIME NULL DEFAULT NULL,
  `data_ultimo_login` DATETIME NULL DEFAULT NULL,
  `cliente_grupo_owner` TINYINT(1) NOT NULL COMMENT 'Representa o dono de um grupo de operadoras',
  `id_adm_tipo_usuario` INT(11) NOT NULL COMMENT 'Inicialmente administradores',
  `status` TINYINT(2) NOT NULL DEFAULT '1',
  `id_cliente_grupo` INT(11) NOT NULL,
  `data_inclusao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_adm_usuario`),
  INDEX `fk_adm_usuario_adm_tipo_usuario1_idx` (`id_adm_tipo_usuario` ASC) VISIBLE,
  INDEX `fk_adm_usuario_cliente_grupo1_idx` (`id_cliente_grupo` ASC) VISIBLE,
  CONSTRAINT `fk_adm_usuario_adm_tipo_usuario1`
    FOREIGN KEY (`id_adm_tipo_usuario`)
    REFERENCES `thweb_homologacao`.`adm_tipo_usuario` (`id_adm_tipo_usuario`),
  CONSTRAINT `fk_adm_usuario_cliente_grupo1`
    FOREIGN KEY (`id_cliente_grupo`)
    REFERENCES `thweb_homologacao`.`cliente_grupo` (`id_cliente_grupo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_usuario_funcionalidade_extra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_usuario_funcionalidade_extra` (
  `id_adm_usuario_funcionalidade_extra` INT(11) NOT NULL AUTO_INCREMENT,
  `id_extra` INT(11) NOT NULL,
  `valor` VARCHAR(255) NULL DEFAULT NULL,
  `id_adm_usuario` INT(11) NOT NULL,
  `id_cliente` INT(11) NOT NULL,
  `identificacao` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_adm_usuario_funcionalidade_extra`),
  INDEX `fk_usuario_funcionalidade_extra_usuario_idx` (`id_adm_usuario` ASC) VISIBLE,
  INDEX `fk_usuario_funcionalidade_extra_painel_idx` (`id_extra` ASC) VISIBLE,
  INDEX `fk_adm_usuario_funcionalidade_extra_1_idx` (`id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_adm_usuario_funcionalidade_extra_1`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_adm_usuario_funcionalidade_extra_adm_usuario`
    FOREIGN KEY (`id_adm_usuario`)
    REFERENCES `thweb_homologacao`.`adm_usuario` (`id_adm_usuario`),
  CONSTRAINT `fk_adm_usuario_funcionalidade_extra_painel_funcionalidade`
    FOREIGN KEY (`id_extra`)
    REFERENCES `thweb_homologacao`.`adm_painel_funcionalidade_extra` (`id_adm_painel_funcionalidade_extra`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`adm_usuario_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`adm_usuario_perfil` (
  `id_adm_usuario_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_adm_perfil` INT(11) NOT NULL,
  `id_adm_usuario` INT(11) NOT NULL,
  PRIMARY KEY (`id_adm_usuario_perfil`),
  INDEX `fk_adm_usuario_perfil_adm_perfil1_idx` (`id_adm_perfil` ASC) VISIBLE,
  INDEX `fk_adm_usuario_perfil_adm_usuario1_idx` (`id_adm_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_adm_usuario_perfil_adm_perfil1`
    FOREIGN KEY (`id_adm_perfil`)
    REFERENCES `thweb_homologacao`.`adm_perfil` (`id_adm_perfil`),
  CONSTRAINT `fk_adm_usuario_perfil_adm_usuario1`
    FOREIGN KEY (`id_adm_usuario`)
    REFERENCES `thweb_homologacao`.`adm_usuario` (`id_adm_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`analytics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`analytics` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag` VARCHAR(100) NOT NULL DEFAULT '',
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `conteudo` VARCHAR(150) NULL DEFAULT NULL,
  `id_device` VARCHAR(255) NULL DEFAULT NULL,
  `data` DATETIME NOT NULL,
  `plataforma` VARCHAR(50) NOT NULL DEFAULT '',
  `versao_app` VARCHAR(50) NOT NULL DEFAULT '',
  `versao_configurador` VARCHAR(50) NOT NULL DEFAULT '',
  `versao_sistema` VARCHAR(50) NOT NULL DEFAULT '',
  `dados_genericos` TEXT NULL DEFAULT NULL,
  `usuario_conv_id` VARCHAR(100) NULL DEFAULT NULL,
  `usuario_conv_desc` VARCHAR(100) NULL DEFAULT NULL,
  `usuario_tipo` VARCHAR(1) NULL DEFAULT NULL,
  `usuario_login` VARCHAR(200) NULL DEFAULT NULL,
  `usuario_nome` VARCHAR(150) NULL DEFAULT NULL,
  `usuario_sexo` VARCHAR(1) NULL DEFAULT NULL,
  `usuario_nascimento` DATE NULL DEFAULT NULL,
  `usuario_contrato` VARCHAR(100) NULL DEFAULT NULL,
  `usuario_uf` VARCHAR(2) NULL DEFAULT NULL,
  `usuario_cidade` VARCHAR(100) NULL DEFAULT NULL,
  `usuario_lat` VARCHAR(150) NULL DEFAULT NULL,
  `usuario_lon` VARCHAR(150) NULL DEFAULT NULL,
  `usuario_especialidade` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`api_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`api_usuario` (
  `id_api_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `public_key` TEXT NULL DEFAULT NULL,
  `tipo_usuario` INT(11) NULL DEFAULT NULL COMMENT '1 = Usuário Normal\\n2 = Usuário Administrador (tem acesso a todos as aplicações)',
  `max_requisicoes_min` INT(11) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `telefone` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(145) NULL DEFAULT NULL,
  `senha` VARCHAR(80) NULL DEFAULT NULL,
  `tipo_seguranca` INT(11) NULL DEFAULT NULL,
  `status` INT(2) NULL DEFAULT NULL,
  `private_key` TEXT NULL DEFAULT NULL,
  `login` VARCHAR(75) NULL DEFAULT NULL,
  PRIMARY KEY (`id_api_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`api_token`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`api_token` (
  `id_api_token` INT(11) NOT NULL AUTO_INCREMENT,
  `id_api_usuario` INT(11) NOT NULL,
  `token` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_validade` DATETIME NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  PRIMARY KEY (`id_api_token`),
  INDEX `fk_api_token_api_usuario1_idx` (`id_api_usuario` ASC) VISIBLE,
  INDEX `index_token` (`token` ASC) VISIBLE,
  CONSTRAINT `fk_api_token_api_usuario1`
    FOREIGN KEY (`id_api_usuario`)
    REFERENCES `thweb_homologacao`.`api_usuario` (`id_api_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`api_requisicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`api_requisicao` (
  `id_api_requisicao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_api_token` INT(11) NOT NULL,
  `data_requisicao` DATETIME NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT '1',
  `metodo` VARCHAR(145) NULL DEFAULT NULL,
  `request` TEXT NULL DEFAULT NULL,
  `response` TEXT NULL DEFAULT NULL,
  `mshash` VARCHAR(75) NULL DEFAULT NULL,
  PRIMARY KEY (`id_api_requisicao`),
  INDEX `fk_api_requisicao_api_token1_idx` (`id_api_token` ASC) VISIBLE,
  CONSTRAINT `fk_api_requisicao_api_token1`
    FOREIGN KEY (`id_api_token`)
    REFERENCES `thweb_homologacao`.`api_token` (`id_api_token`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`api_usuario_aplicacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`api_usuario_aplicacao` (
  `id_api_usuario_aplicacao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_api_usuario` INT(11) NOT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_api_usuario_aplicacao`),
  INDEX `fk_api_usuario_aplicacao_api_usuario1_idx` (`id_api_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_api_usuario_aplicacao_api_usuario1`
    FOREIGN KEY (`id_api_usuario`)
    REFERENCES `thweb_homologacao`.`api_usuario` (`id_api_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`aplicacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`aplicacao` (
  `id_aplicacao` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL DEFAULT NULL COMMENT 'ObrigatÃ³rio',
  `versao` VARCHAR(20) NULL DEFAULT NULL,
  `detalhes` TEXT NULL DEFAULT NULL COMMENT 'Detalhemento da aplicação',
  `produto` INT(11) NULL DEFAULT NULL COMMENT 'ObrigatÃ³rio\\n\\nIndica se a aplicaÃ§Ã£o Ã© um produto da Mobile SaÃºde.\\n\\n0 = NÃ£o\\n1 = Sim',
  `ios` SMALLINT(1) NULL DEFAULT '0',
  `android` SMALLINT(1) NULL DEFAULT '0',
  `winphone` SMALLINT(1) NULL DEFAULT '0',
  `web` SMALLINT(1) NULL DEFAULT '0',
  `prop_modelo` TEXT NULL DEFAULT NULL,
  `producao` SMALLINT(1) NULL DEFAULT '1' COMMENT 'Coluna pra indicar se o aplicativo é de produção ou homologação',
  `automatico` TINYINT(4) NULL DEFAULT '0',
  PRIMARY KEY (`id_aplicacao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`aplicacao_funcionalidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`aplicacao_funcionalidade` (
  `id_aplicacao_funcionalidade` INT(11) NOT NULL AUTO_INCREMENT,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `app_enum_id` VARCHAR(45) NULL DEFAULT NULL,
  `descricao` VARCHAR(45) NULL DEFAULT NULL,
  `broadcast` TINYINT(1) NULL DEFAULT '0',
  `consome_franquia` TINYINT(1) NULL DEFAULT '0',
  `inbox` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id_aplicacao_funcionalidade`),
  INDEX `app_enum_id` (`app_enum_id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_contatos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_contatos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `ordem` INT(11) NOT NULL,
  `descricao` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `icone` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `imagem` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `file` VARCHAR(155) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `area_beneficiario` INT(11) NULL DEFAULT '1',
  `area_cooperado` INT(11) NULL DEFAULT '0',
  `url_imagem` VARCHAR(255) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_contatos_itens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_contatos_itens` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_contato` INT(11) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `descricao` VARCHAR(255) NULL DEFAULT NULL,
  `endereco` VARCHAR(255) NULL DEFAULT NULL,
  `label_contato1` VARCHAR(255) NULL DEFAULT NULL,
  `telefone_contato1` VARCHAR(15) NULL DEFAULT NULL,
  `email_contato1` VARCHAR(255) NULL DEFAULT NULL,
  `label_contato2` VARCHAR(255) NULL DEFAULT NULL,
  `telefone_contato2` VARCHAR(15) NULL DEFAULT NULL,
  `email_contato2` VARCHAR(255) NULL DEFAULT NULL,
  `label_contato3` VARCHAR(255) NULL DEFAULT NULL,
  `telefone_contato3` VARCHAR(15) NULL DEFAULT NULL,
  `email_contato3` VARCHAR(255) NULL DEFAULT NULL,
  `label_contato4` VARCHAR(255) NULL DEFAULT NULL,
  `telefone_contato4` VARCHAR(15) NULL DEFAULT NULL,
  `email_contato4` VARCHAR(255) NULL DEFAULT NULL,
  `label_contato5` VARCHAR(255) NULL DEFAULT NULL,
  `telefone_contato5` VARCHAR(15) NULL DEFAULT NULL,
  `email_contato5` VARCHAR(255) NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT NULL,
  `link_contato1` VARCHAR(255) NULL DEFAULT NULL,
  `link_contato2` VARCHAR(255) NULL DEFAULT NULL,
  `link_contato3` VARCHAR(255) NULL DEFAULT NULL,
  `link_contato4` VARCHAR(255) NULL DEFAULT NULL,
  `link_contato5` VARCHAR(255) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `textolivre_contato1` TEXT NULL DEFAULT NULL,
  `textolivre_contato2` TEXT NULL DEFAULT NULL,
  `textolivre_contato3` TEXT NULL DEFAULT NULL,
  `textolivre_contato4` TEXT NULL DEFAULT NULL,
  `textolivre_contato5` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`perfil_acesso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`perfil_acesso` (
  `id_perfil_acesso` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(75) NOT NULL,
  PRIMARY KEY (`id_perfil_acesso`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_contatos_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_contatos_perfil` (
  `id_area_contatos_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_area_contatos` INT(11) NULL DEFAULT NULL,
  `id_perfil_acesso` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_area_contatos_perfil`),
  INDEX `fk_perfil` (`id_perfil_acesso` ASC) VISIBLE,
  CONSTRAINT `fk_perfil`
    FOREIGN KEY (`id_perfil_acesso`)
    REFERENCES `thweb_homologacao`.`perfil_acesso` (`id_perfil_acesso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_manuais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_manuais` (
  `id_area_manuais` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `ordem` INT(5) NOT NULL,
  `icone` VARCHAR(10) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `arquivo` VARCHAR(255) NOT NULL,
  `file` VARCHAR(255) NOT NULL,
  `id_area_manuais_tipo` INT(11) NULL DEFAULT NULL,
  `arquivo_tamanho` VARCHAR(45) NULL DEFAULT NULL,
  `arquivo_url` TEXT NULL DEFAULT NULL,
  `arquivo_versao` INT(11) NULL DEFAULT '1',
  `capa` VARCHAR(145) NULL DEFAULT NULL,
  `data_inicial` DATETIME NULL DEFAULT NULL,
  `data_final` DATETIME NULL DEFAULT NULL,
  `capa_url` TEXT NULL DEFAULT NULL,
  `url_publicacao` TEXT NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_area_manuais`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_manuais_itens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_manuais_itens` (
  `id_area_manuais_itens` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_area_manuais` INT(11) NOT NULL,
  `ordem` INT(5) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `imagem` VARCHAR(255) NOT NULL,
  `file` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_area_manuais_itens`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_manuais_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_manuais_perfil` (
  `id_area_manuais_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_area_manuais` INT(11) NOT NULL,
  `id_perfil_acesso` INT(11) NOT NULL,
  PRIMARY KEY (`id_area_manuais_perfil`),
  INDEX `fk_area_manuais_perfil_acesso` (`id_perfil_acesso` ASC) VISIBLE,
  CONSTRAINT `fk_area_manuais_perfil_acesso`
    FOREIGN KEY (`id_perfil_acesso`)
    REFERENCES `thweb_homologacao`.`perfil_acesso` (`id_perfil_acesso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_manuais_segmento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_manuais_segmento` (
  `id_area_manuais_segmento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_msg_cliente_segmento` INT(11) NULL DEFAULT NULL,
  `id_area_manuais` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(75) NULL DEFAULT NULL,
  `descricao` VARCHAR(145) NULL DEFAULT NULL,
  `valor` VARCHAR(75) NULL DEFAULT NULL,
  `operador` VARCHAR(5) NULL DEFAULT NULL,
  `valor_descricao` VARCHAR(145) NULL DEFAULT NULL,
  PRIMARY KEY (`id_area_manuais_segmento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`area_manuais_tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`area_manuais_tipo` (
  `id_area_manuais_tipo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `titulo` VARCHAR(245) NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT '0',
  `area_beneficiario` TINYINT(4) NULL DEFAULT '1',
  `area_cooperado` TINYINT(4) NULL DEFAULT '0',
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_area_manuais_tipo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_grupo_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_grupo_campo` (
  `id_grupo_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `ordem` INT(2) NOT NULL,
  PRIMARY KEY (`id_grupo_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_campo` (
  `id_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_grupo_campo` INT(11) NOT NULL COMMENT 'ID do grupo que o campo pertence',
  `label` VARCHAR(200) NOT NULL,
  `tamanho` INT(2) NOT NULL DEFAULT '12' COMMENT 'DE 1 A 12',
  `valor_esperado` VARCHAR(255) NULL DEFAULT NULL,
  `texto_ajuda` TEXT NULL DEFAULT NULL COMMENT 'Texto descritivo que está ao lado do campo',
  `obrigatorio` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `visivel` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `editavel` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `nome` VARCHAR(255) NOT NULL COMMENT 'valor que fica no name do campo',
  `classe` VARCHAR(100) NULL DEFAULT NULL COMMENT 'CAMPO_CLASSE_CEP => \"cep\"\\nCAMPO_CLASSE_TELEFONE => \"telefone\"\\nCAMPO_CLASSE_CELULAR => \"celular\"\\nCAMPO_CLASSE_EMAIL => \"email\"\\nCAMPO_CLASSE_CPF => \"cpf\'\"',
  `tipo` VARCHAR(100) NOT NULL DEFAULT 'text' COMMENT 'CAMPO_TIPO_TEXT => \"text\"\\nCAMPO_TIPO_EMAIL => \"email\"\\nCAMPO_TIPO_FILE => \"file\"\\nCAMPO_TIPO_OPTION => \"select\"',
  `valor_padrao` VARCHAR(255) NULL DEFAULT NULL,
  `ref_beneficiario` VARCHAR(100) NULL DEFAULT NULL,
  `max_itens` INT(11) NOT NULL DEFAULT '1',
  `validacao_msg` TEXT NULL DEFAULT NULL,
  `validacao_regex` VARCHAR(255) NULL DEFAULT NULL,
  `ordem` INT(2) NULL DEFAULT NULL,
  `ref_nao_editavel` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_campo`),
  INDEX `id_grupo_campo_idx` (`id_grupo_campo` ASC) VISIBLE,
  CONSTRAINT `id_grupo_campo`
    FOREIGN KEY (`id_grupo_campo`)
    REFERENCES `thweb_homologacao`.`atual_cadast_grupo_campo` (`id_grupo_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_campo_obrigatoriedade_cond`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_campo_obrigatoriedade_cond` (
  `id_campo_obrigatoriedade_cond` INT(11) NOT NULL AUTO_INCREMENT,
  `id_campo` INT(11) NULL DEFAULT NULL,
  `id_campo_obrigatorio` INT(11) NULL DEFAULT NULL,
  `tipo_obrigatoriedade` VARCHAR(75) NULL DEFAULT NULL,
  PRIMARY KEY (`id_campo_obrigatoriedade_cond`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_campo_opcoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_campo_opcoes` (
  `id_campo_opcoes` INT(11) NOT NULL AUTO_INCREMENT,
  `id_campo` INT(11) NOT NULL COMMENT 'ID do campo que a opção pertence',
  `label` VARCHAR(100) NOT NULL,
  `valor` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_campo_opcoes`),
  INDEX `id_campo_idx` (`id_campo` ASC) VISIBLE,
  CONSTRAINT `id_campo`
    FOREIGN KEY (`id_campo`)
    REFERENCES `thweb_homologacao`.`atual_cadast_campo` (`id_campo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_config` (
  `id_config` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `email_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `nome_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `email_destinatario` VARCHAR(100) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `texto_email_solicitado_operadora` TEXT NULL DEFAULT NULL,
  `texto_email_solicitado_beneficiario` TEXT NULL DEFAULT NULL,
  `texto_email_aprovado_beneficiario` TEXT NULL DEFAULT NULL,
  `texto_email_recusado_beneficiario` TEXT NULL DEFAULT NULL,
  `css_personalizado` TEXT NULL DEFAULT NULL,
  `email_contato` VARCHAR(100) NULL DEFAULT NULL,
  `telefone` VARCHAR(45) NULL DEFAULT NULL,
  `permite_atualizacao_dependentes` TINYINT(1) NULL DEFAULT '0',
  `assunto_solicitado_operadora` TEXT NULL DEFAULT NULL,
  `assunto_solicitado_beneficiario` TEXT NULL DEFAULT NULL,
  `assunto_aprovado_beneficiario` TEXT NULL DEFAULT NULL,
  `assunto_recusado_beneficiario` TEXT NULL DEFAULT NULL,
  `url_callback` VARCHAR(255) NULL DEFAULT NULL,
  `callback_authorization` VARCHAR(255) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `callback_email` VARCHAR(255) NULL DEFAULT NULL,
  `data_ultimo_alerta_erros` DATETIME NULL DEFAULT NULL,
  `solicitacoes_intervalo` INT(11) NOT NULL DEFAULT '1' COMMENT 'Define o tempo, em horas, que o beneficiário deve esperar para fazer uma nova solicitação após já ter feito uma.',
  `solicitacoes_intervalo_mensagem` TEXT NULL DEFAULT NULL COMMENT 'Mensagem de retorno para o beneficiário caso ainda não tenha passado o intervalo entre uma solicitação e outra.',
  PRIMARY KEY (`id_config`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_histor_protocol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_histor_protocol` (
  `id_histor_protocol` INT(11) NOT NULL AUTO_INCREMENT,
  `data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_anterior` TINYINT(1) NULL DEFAULT NULL COMMENT 'NULL;\\nSTATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `status_atual` TINYINT(1) NOT NULL COMMENT 'STATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `protocolo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_histor_protocol`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_protocolo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_protocolo` (
  `id_protocolo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `protocolo` VARCHAR(100) NOT NULL,
  `data` DATETIME NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'STATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `matricula` VARCHAR(45) NOT NULL,
  `tipo` VARCHAR(1) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  `matricula_titular` VARCHAR(45) NOT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  `login` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`atual_cadast_protocolo_dados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`atual_cadast_protocolo_dados` (
  `id_protocolo_dados` INT(11) NOT NULL AUTO_INCREMENT,
  `id_protocolo` INT(11) NOT NULL,
  `id_campo` INT(11) NOT NULL,
  `valor` TEXT NULL DEFAULT NULL,
  `data_atualizacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `label` VARCHAR(200) NOT NULL,
  `nome` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id_protocolo_dados`),
  INDEX `id_protocolo_idx` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `id_protocolo`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`atual_cadast_protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`bck_admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`bck_admin` (
  `id_usuario` INT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `usuario` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `usuarios` TINYINT(1) NOT NULL DEFAULT '0',
  `banners` TINYINT(1) NOT NULL DEFAULT '0',
  `operadoras` TINYINT(1) NOT NULL DEFAULT '0',
  `parametros` TINYINT(1) NOT NULL DEFAULT '0',
  `produtos` TINYINT(1) NOT NULL DEFAULT '0',
  `msconfig` TINYINT(4) NOT NULL DEFAULT '0',
  `financeiro` TINYINT(1) NOT NULL,
  `email` VARCHAR(200) NULL DEFAULT 'mobilesaude@mobilesaude.com.br',
  PRIMARY KEY (`id_usuario`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1
PACK_KEYS = 0;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`bck_banners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`bck_banners` (
  `id_banner` INT(5) NOT NULL AUTO_INCREMENT,
  `ativo` INT(1) NOT NULL DEFAULT '0',
  `posicao` INT(3) NOT NULL,
  `banner` VARCHAR(255) NOT NULL,
  `externo` INT(1) NOT NULL DEFAULT '0',
  `link` VARCHAR(255) NOT NULL DEFAULT '',
  `imagem` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_banner`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`bck_operadoras_banners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`bck_operadoras_banners` (
  `id_banner` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) UNSIGNED NOT NULL,
  `descricao` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `ordem` INT(3) NOT NULL,
  `imagem` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `imagem_file` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id_banner`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`bck_parametros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`bck_parametros` (
  `id_parametro` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email_contato` VARCHAR(255) NOT NULL,
  `email_suporte` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id_parametro`))
ENGINE = MyISAM
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`beneficiario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`beneficiario` (
  `id_beneficiario` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `chave_importacao` VARCHAR(20) NOT NULL,
  `chave_beneficiario` VARCHAR(30) NULL DEFAULT NULL,
  `cpf` VARCHAR(11) NULL DEFAULT NULL,
  `codigo_contrato` VARCHAR(20) NULL DEFAULT NULL,
  `codigo_familia` VARCHAR(20) NULL DEFAULT NULL,
  `matricula` VARCHAR(30) NULL DEFAULT NULL,
  `matricula_funcionario` VARCHAR(40) NULL DEFAULT NULL,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `sexo` VARCHAR(1) NULL DEFAULT NULL,
  `data_nascimento` DATE NULL DEFAULT NULL,
  `celular` VARCHAR(15) NULL DEFAULT NULL,
  `telefone_residencial` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `endereco` VARCHAR(100) NULL DEFAULT NULL,
  `bairro` VARCHAR(50) NULL DEFAULT NULL,
  `cep` VARCHAR(10) NULL DEFAULT NULL,
  `cidade` VARCHAR(50) NULL DEFAULT NULL,
  `estado` VARCHAR(2) NULL DEFAULT NULL,
  `tipo_usuario` VARCHAR(1) NULL DEFAULT NULL,
  `grau_parentesco_id` VARCHAR(2) NULL DEFAULT NULL,
  `grau_parentesco_descricao` VARCHAR(20) NULL DEFAULT NULL,
  `data_inicio_cobertura` DATE NULL DEFAULT NULL,
  `convenio_id` VARCHAR(10) NULL DEFAULT NULL,
  `convenio_descricao` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_abrangencia` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_abrangencia_verso` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_acomodacao` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_padrao_conforto` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_participativo` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_segmentacao` VARCHAR(50) NULL DEFAULT NULL,
  `convenio_tipo_pessoa` VARCHAR(1) NULL DEFAULT NULL,
  `convenio_tipo_contrato` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_regulamentacao` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_vigencia` VARCHAR(20) NULL DEFAULT NULL,
  `cartao_via` VARCHAR(2) NULL DEFAULT NULL,
  `chave_contratante` VARCHAR(20) NULL DEFAULT NULL,
  `nome_contratante` VARCHAR(100) NULL DEFAULT NULL,
  `contrato_empresa_contratante` VARCHAR(20) NULL DEFAULT NULL,
  `numero_cns` VARCHAR(20) NULL DEFAULT NULL,
  `convenio_ans` VARCHAR(20) NULL DEFAULT NULL,
  `data_final_cpt` DATE NULL DEFAULT NULL,
  `rede_atendimento` VARCHAR(20) NULL DEFAULT NULL,
  `data_bloqueio` VARCHAR(8) NULL DEFAULT NULL,
  `ds_contrato` VARCHAR(100) NULL DEFAULT NULL,
  `data_inicio_vinculo` DATE NULL DEFAULT NULL,
  `extra_1` VARCHAR(100) NULL DEFAULT NULL,
  `extra_2` VARCHAR(100) NULL DEFAULT NULL,
  `extra_3` VARCHAR(100) NULL DEFAULT NULL,
  `extra_4` VARCHAR(100) NULL DEFAULT NULL,
  `extra_5` VARCHAR(100) NULL DEFAULT NULL,
  `extra_6` VARCHAR(100) NULL DEFAULT NULL,
  `extra_7` VARCHAR(100) NULL DEFAULT NULL,
  `extra_8` VARCHAR(100) NULL DEFAULT NULL,
  `extra_9` VARCHAR(100) NULL DEFAULT NULL,
  `extra_10` VARCHAR(100) NULL DEFAULT NULL,
  `cartao_validade` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id_beneficiario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`beneficiario_carencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`beneficiario_carencia` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `chave_beneficiario` VARCHAR(20) NOT NULL,
  `codigo_contrato` VARCHAR(20) NOT NULL,
  `codigo_familia` VARCHAR(10) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL,
  `matricula` VARCHAR(20) NOT NULL,
  `dsCarencia` VARCHAR(50) NOT NULL,
  `periodo` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`beneficiario_credencial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`beneficiario_credencial` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `chave_beneficiario` VARCHAR(20) NOT NULL,
  `login` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(10) NULL DEFAULT NULL,
  `tipo_hash` VARCHAR(10) NULL DEFAULT NULL,
  `id_importacao` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`beneficiario_permissao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`beneficiario_permissao` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `chave_beneficiario` VARCHAR(20) NOT NULL,
  `codigo_contrato` VARCHAR(20) NOT NULL,
  `codigo_familia` VARCHAR(20) NOT NULL,
  `cpf` VARCHAR(20) NOT NULL,
  `matricula` VARCHAR(20) NOT NULL,
  `permite_acesso` INT(1) NOT NULL DEFAULT '0',
  `acessa_cartao` INT(1) NOT NULL DEFAULT '0',
  `acessa_boleto` INT(1) NOT NULL DEFAULT '0',
  `acessa_extrato_utilizacao` INT(1) NOT NULL DEFAULT '0',
  `acessa_rede_credenciada` INT(1) NOT NULL DEFAULT '0',
  `acessa_agendamento` INT(1) NOT NULL DEFAULT '0',
  `acessa_atualizacao_cadastral` INT(1) NOT NULL DEFAULT '0',
  `acessa_reembolso` INT(1) NOT NULL DEFAULT '0',
  `acessa_declaracao` INT(1) NOT NULL DEFAULT '0',
  `acessa_solicitacao_cartao` INT(1) NOT NULL DEFAULT '0',
  `acessa_extrato_autorizacoes` INT(1) NOT NULL DEFAULT '0',
  `acessa_solicitacao_autorizacao` INT(1) NOT NULL DEFAULT '0',
  `acessa_busca_medicamento` INT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`beneficiario_utilizacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`beneficiario_utilizacao` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `competencia` VARCHAR(20) NOT NULL,
  `chave_beneficiario` VARCHAR(20) NOT NULL,
  `codigo_contrato` VARCHAR(20) NOT NULL,
  `codigo_procedimento` VARCHAR(10) NOT NULL,
  `descri_procedimento` VARCHAR(50) NOT NULL,
  `data_atendimento` VARCHAR(8) NOT NULL,
  `codigo_prestador` VARCHAR(10) NOT NULL,
  `nome_prestador` VARCHAR(100) NOT NULL,
  `cnpj` VARCHAR(14) NOT NULL,
  `cod_tp_servico` VARCHAR(10) NOT NULL,
  `ds_tp_servico` VARCHAR(100) NOT NULL,
  `qtdade` VARCHAR(3) NOT NULL,
  `vl_serv` VARCHAR(20) NOT NULL,
  `vl_cop` VARCHAR(20) NOT NULL DEFAULT '0.00',
  `id_operadora` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`callback_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`callback_config` (
  `id_callback_config` INT(11) NOT NULL AUTO_INCREMENT,
  `num_max_tentativas` INT(5) NOT NULL,
  `timeout` INT(11) NOT NULL,
  `tempo_espera` INT(11) NOT NULL,
  `qtd_envios` INT(11) NOT NULL DEFAULT '0',
  `qtd_erros_alerta` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_callback_config`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`callback_request`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`callback_request` (
  `id_callback_request` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_funcionalidade` INT(11) NOT NULL,
  `id_registro_cms` INT(11) NOT NULL,
  `protocolo` VARCHAR(255) NOT NULL,
  `url_callback` VARCHAR(255) NOT NULL,
  `num_tentativas` INT(11) NULL DEFAULT '0',
  `json` TEXT NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `authorization` VARCHAR(255) NULL DEFAULT NULL,
  `origem` TINYINT(1) NOT NULL,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `data_ultimo_envio` DATETIME NULL DEFAULT NULL,
  `id_callback_request_reenvio` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_callback_request`),
  INDEX `idx_operadora` (`id_operadora` ASC) VISIBLE,
  INDEX `idx_aplicacao` (`id_aplicacao` ASC) VISIBLE,
  INDEX `idx_funcionalidade` (`id_funcionalidade` ASC) VISIBLE,
  INDEX `idx_status` (`status` ASC) VISIBLE,
  INDEX `idx_data` (`data_criacao` ASC) VISIBLE,
  INDEX `idx_reenvio` (`id_callback_request_reenvio` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`callback_response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`callback_response` (
  `id_callback_response` INT(11) NOT NULL AUTO_INCREMENT,
  `id_callback_request` INT(11) NOT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `response` TEXT NULL DEFAULT NULL,
  `http_status` INT(11) NOT NULL,
  `data_criacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` TINYINT(1) NOT NULL,
  `tempo_resposta` INT(11) NOT NULL,
  PRIMARY KEY (`id_callback_response`),
  INDEX `fk_callback_response_1_idx` (`id_callback_request` ASC) VISIBLE,
  INDEX `idx_operadora` (`id_operadora` ASC) VISIBLE,
  INDEX `idx_aplicacao` (`id_aplicacao` ASC) VISIBLE,
  INDEX `idx_funcionalidade` (`id_funcionalidade` ASC) VISIBLE,
  INDEX `idx_status` (`status` ASC) VISIBLE,
  INDEX `idx_data` (`data_criacao` ASC) VISIBLE,
  CONSTRAINT `fk_callback_response_1`
    FOREIGN KEY (`id_callback_request`)
    REFERENCES `thweb_homologacao`.`callback_request` (`id_callback_request`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`callback_response_api`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`callback_response_api` (
  `id_callback_response_api` INT(11) NOT NULL AUTO_INCREMENT,
  `id_callback_response` INT(11) NOT NULL,
  `id_callback_request` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `response_api` TEXT NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `data_criacao` DATETIME NOT NULL,
  PRIMARY KEY (`id_callback_response_api`),
  INDEX `id_callback_response` (`id_callback_response` ASC) VISIBLE,
  CONSTRAINT `callback_response_api_ibfk_1`
    FOREIGN KEY (`id_callback_response`)
    REFERENCES `thweb_homologacao`.`callback_response` (`id_callback_response`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`callback_status_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`callback_status_historico` (
  `id_callback_status_historico` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `data_registro` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `callback_status_anterior` TINYINT(1) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `origem` TINYINT(1) NULL DEFAULT NULL,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_callback_status_historico`),
  INDEX `idx_operadora` (`id_operadora` ASC) VISIBLE,
  INDEX `idx_aplicacao` (`id_aplicacao` ASC) VISIBLE,
  INDEX `idx_funcionalidade` (`id_funcionalidade` ASC) VISIBLE,
  INDEX `idx_data` (`data_registro` ASC) VISIBLE,
  INDEX `idx_status` (`callback_status` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`callback_token`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`callback_token` (
  `id_callback_token` INT(11) NOT NULL AUTO_INCREMENT,
  `id_funcionalidade` INT(11) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `token` TEXT NOT NULL,
  `email` TEXT NOT NULL,
  `usado` TINYINT(1) NOT NULL DEFAULT '0',
  `data_registro` DATETIME NOT NULL,
  `data_validade` DATETIME NOT NULL,
  `data_uso` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_callback_token`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_motivo_solicitacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_motivo_solicitacao` (
  `id_motivo_solic_cancelamento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `texto_complementar` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_motivo_solic_cancelamento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_status` (
  `id_status_cancelamento` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `imagem` VARCHAR(200) NULL DEFAULT NULL,
  `cor` VARCHAR(40) NULL DEFAULT NULL,
  PRIMARY KEY (`id_status_cancelamento`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`protocolo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`protocolo` (
  `id_protocolo` INT(11) NOT NULL AUTO_INCREMENT,
  `protocolo` VARCHAR(255) NOT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_solicitacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_solicitacao` (
  `id_solicitacao_cancelamento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_protocolo` INT(11) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_motivo_solic_cancelamento` INT(11) NOT NULL,
  `id_status_cancelamento` INT(11) NOT NULL,
  `matricula_titular` VARCHAR(100) NULL DEFAULT NULL,
  `nome_titular` VARCHAR(150) NULL DEFAULT NULL,
  `cpf_titular` VARCHAR(14) NULL DEFAULT NULL,
  `telefone_titular` VARCHAR(20) NULL DEFAULT NULL,
  `email_titular` VARCHAR(100) NULL DEFAULT NULL,
  `patrocinadora_titular` VARCHAR(100) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `texto_complementar` TEXT NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_solicitacao_cancelamento`),
  INDEX `id_motivo_solic_cancelamento` (`id_motivo_solic_cancelamento` ASC) VISIBLE,
  INDEX `id_status_cancelamento` (`id_status_cancelamento` ASC) VISIBLE,
  INDEX `id_protocolo` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `cancelamento_solicitacao_ibfk_1`
    FOREIGN KEY (`id_motivo_solic_cancelamento`)
    REFERENCES `thweb_homologacao`.`cancelamento_motivo_solicitacao` (`id_motivo_solic_cancelamento`),
  CONSTRAINT `cancelamento_solicitacao_ibfk_2`
    FOREIGN KEY (`id_status_cancelamento`)
    REFERENCES `thweb_homologacao`.`cancelamento_status` (`id_status_cancelamento`),
  CONSTRAINT `cancelamento_solicitacao_ibfk_3`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_beneficiario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_beneficiario` (
  `id_beneficiario_cancelamento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_solicitacao_cancelamento` INT(11) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  `matricula` VARCHAR(100) NOT NULL,
  `parentesco` VARCHAR(100) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `convenio_segmentacao` VARCHAR(100) NULL DEFAULT NULL,
  `numero_contrato` VARCHAR(50) NULL DEFAULT NULL,
  `texto_complementar` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_beneficiario_cancelamento`),
  INDEX `id_solicitacao_cancelamento` (`id_solicitacao_cancelamento` ASC) VISIBLE,
  CONSTRAINT `cancelamento_beneficiario_ibfk_1`
    FOREIGN KEY (`id_solicitacao_cancelamento`)
    REFERENCES `thweb_homologacao`.`cancelamento_solicitacao` (`id_solicitacao_cancelamento`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_configuracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_configuracao` (
  `id_config_cancelamento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `email` VARCHAR(200) NOT NULL DEFAULT '',
  `nome_remetente_email` VARCHAR(100) NULL DEFAULT NULL,
  `texto_confirmacao` VARCHAR(100) NULL DEFAULT NULL,
  `texto_advertencia` TEXT NULL DEFAULT NULL,
  `link_advertencia` VARCHAR(255) NULL DEFAULT NULL,
  `advertencia_financeiro` TEXT NULL DEFAULT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  `assunto_beneficiario` VARCHAR(255) NULL DEFAULT NULL,
  `url_callback` VARCHAR(255) NULL DEFAULT NULL,
  `callback_authorization` VARCHAR(255) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `callback_email` VARCHAR(255) NULL DEFAULT NULL,
  `data_ultimo_alerta_erros` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_config_cancelamento`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_historico` (
  `id_historico_cancelamento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_status_cancelamento` INT(11) NOT NULL,
  `id_solicitacao_cancelamento` INT(11) NOT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `descricao` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id_historico_cancelamento`),
  INDEX `id_status_cancelamento` (`id_status_cancelamento` ASC) VISIBLE,
  INDEX `id_solicitacao_cancelamento` (`id_solicitacao_cancelamento` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cancelamento_protocolo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cancelamento_protocolo` (
  `id_protocolo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `protocolo` VARCHAR(100) NOT NULL,
  `data` DATETIME NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'STATUS_SOLICITADO => 1;\\nSTATUS_EM_PROCESSAMENTO   => 2;\\nSTATUS_ENVIADO   => 3;',
  `matricula` VARCHAR(45) NOT NULL,
  `tipo` VARCHAR(1) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  `matricula_titular` VARCHAR(45) NOT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`carrossel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`carrossel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `data_inicio` DATETIME NULL DEFAULT NULL,
  `data_fim` DATETIME NULL DEFAULT NULL,
  `titulo` VARCHAR(250) NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `id_registro` VARCHAR(500) NULL DEFAULT NULL,
  `imagem` VARCHAR(255) NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT '0',
  `id_funcionalidade_web` INT(11) NULL DEFAULT NULL,
  `id_registro_web` VARCHAR(300) NULL DEFAULT NULL,
  `tipo` INT(1) NULL DEFAULT '1',
  `web` INT(11) NOT NULL DEFAULT '0',
  `app` INT(11) NOT NULL DEFAULT '1',
  `tipo_link_web` VARCHAR(11) NULL DEFAULT NULL,
  `area_beneficiario` TINYINT(4) NOT NULL DEFAULT '1',
  `area_cooperado` TINYINT(4) NOT NULL DEFAULT '0',
  `url_imagem` VARCHAR(255) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `segmentado` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`carrossel_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`carrossel_perfil` (
  `id_carrossel_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_carrossel` INT(11) NOT NULL,
  `id_perfil_acesso` INT(11) NOT NULL,
  PRIMARY KEY (`id_carrossel_perfil`),
  INDEX `fk_carrossel_perfil_acesso` (`id_perfil_acesso` ASC) VISIBLE,
  CONSTRAINT `fk_carrossel_perfil_acesso`
    FOREIGN KEY (`id_perfil_acesso`)
    REFERENCES `thweb_homologacao`.`perfil_acesso` (`id_perfil_acesso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`carrossel_segmento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`carrossel_segmento` (
  `id_carrossel_segmento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_msg_cliente_segmento` INT(11) NULL DEFAULT NULL,
  `id_carrossel` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(75) NULL DEFAULT NULL,
  `descricao` VARCHAR(145) NULL DEFAULT NULL,
  `valor` VARCHAR(75) NULL DEFAULT NULL,
  `operador` VARCHAR(5) NULL DEFAULT NULL,
  `valor_descricao` VARCHAR(145) NULL DEFAULT NULL,
  PRIMARY KEY (`id_carrossel_segmento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_motivo_solicitacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_motivo_solicitacao` (
  `id_motivo_solic_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(200) NULL DEFAULT NULL,
  `descricao` TEXT NOT NULL,
  `tem_anexo` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_motivo_solic_cartao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_status` (
  `id_status_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `imagem` VARCHAR(200) NULL DEFAULT NULL,
  `cor` VARCHAR(40) NULL DEFAULT NULL,
  PRIMARY KEY (`id_status_cartao`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_forma_entrega`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_forma_entrega` (
  `id_forma_entrega_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(200) NULL DEFAULT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  `exige_endereco` TINYINT(1) NOT NULL DEFAULT '0',
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  PRIMARY KEY (`id_forma_entrega_cartao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_solicitacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_solicitacao` (
  `id_solicitacao_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_protocolo` INT(11) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_motivo_solic_cartao` INT(11) NOT NULL,
  `id_forma_entrega_cartao` INT(11) NOT NULL,
  `id_status_cartao` INT(11) NOT NULL,
  `matricula_titular` VARCHAR(100) NULL DEFAULT NULL,
  `nome_titular` VARCHAR(150) NULL DEFAULT NULL,
  `cpf_titular` VARCHAR(14) NULL DEFAULT NULL,
  `telefone_titular` VARCHAR(20) NULL DEFAULT NULL,
  `estado_civil_titular` VARCHAR(10) NULL DEFAULT NULL,
  `email_titular` VARCHAR(100) NULL DEFAULT NULL,
  `patrocinadora_titular` VARCHAR(100) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `endereco_alternativo` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_solicitacao_cartao`),
  INDEX `id_motivo_solic_cartao` (`id_motivo_solic_cartao` ASC) VISIBLE,
  INDEX `id_forma_entrega_cartao` (`id_forma_entrega_cartao` ASC) VISIBLE,
  INDEX `id_status_cartao` (`id_status_cartao` ASC) VISIBLE,
  INDEX `id_protocolo` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `cartao_solicitacao_ibfk_1`
    FOREIGN KEY (`id_motivo_solic_cartao`)
    REFERENCES `thweb_homologacao`.`cartao_motivo_solicitacao` (`id_motivo_solic_cartao`),
  CONSTRAINT `cartao_solicitacao_ibfk_2`
    FOREIGN KEY (`id_status_cartao`)
    REFERENCES `thweb_homologacao`.`cartao_status` (`id_status_cartao`),
  CONSTRAINT `cartao_solicitacao_ibfk_3`
    FOREIGN KEY (`id_forma_entrega_cartao`)
    REFERENCES `thweb_homologacao`.`cartao_forma_entrega` (`id_forma_entrega_cartao`),
  CONSTRAINT `cartao_solicitacao_ibfk_4`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_arquivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_arquivo` (
  `id_arquivo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_solicitacao_cartao` INT(11) NOT NULL,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `diretorio` VARCHAR(100) NULL DEFAULT NULL,
  `url` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id_arquivo`),
  INDEX `id_solicitacao_cartao` (`id_solicitacao_cartao` ASC) VISIBLE,
  CONSTRAINT `cartao_arquivo_ibfk_1`
    FOREIGN KEY (`id_solicitacao_cartao`)
    REFERENCES `thweb_homologacao`.`cartao_solicitacao` (`id_solicitacao_cartao`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_beneficiario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_beneficiario` (
  `id_beneficiario_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_solicitacao_cartao` INT(11) NOT NULL,
  `nome` VARCHAR(200) NULL DEFAULT NULL,
  `matricula` VARCHAR(100) NULL DEFAULT NULL,
  `parentesco` VARCHAR(100) NULL DEFAULT NULL,
  `data_nascimento` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id_beneficiario_cartao`),
  INDEX `id_solicitacao_cartao` (`id_solicitacao_cartao` ASC) VISIBLE,
  CONSTRAINT `cartao_beneficiario_ibfk_1`
    FOREIGN KEY (`id_solicitacao_cartao`)
    REFERENCES `thweb_homologacao`.`cartao_solicitacao` (`id_solicitacao_cartao`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_configuracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_configuracao` (
  `id_config_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `telefone_contato` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(200) NOT NULL DEFAULT '',
  `nome_remetente_email` VARCHAR(100) NULL DEFAULT NULL,
  `texto_confirmacao` VARCHAR(100) NULL DEFAULT NULL,
  `texto_rodape` VARCHAR(255) NULL DEFAULT NULL,
  `url_callback` VARCHAR(255) NULL DEFAULT NULL,
  `callback_authorization` VARCHAR(255) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `callback_email` VARCHAR(255) NULL DEFAULT NULL,
  `data_ultimo_alerta_erros` DATETIME NULL DEFAULT NULL,
  `botao_cancelar` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_config_cartao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cartao_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cartao_historico` (
  `id_historico_cartao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_status_cartao` INT(11) NOT NULL,
  `id_solicitacao_cartao` INT(11) NOT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `descricao` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id_historico_cartao`),
  INDEX `id_status_cartao` (`id_status_cartao` ASC) VISIBLE,
  INDEX `id_solicitacao_cartao` (`id_solicitacao_cartao` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ci_sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ci_sessions` (
  `id` VARCHAR(128) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` BLOB NOT NULL,
  INDEX `ci_sessions_timestamp` (`timestamp` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ci_sessions_cms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ci_sessions_cms` (
  `id` VARCHAR(128) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` MEDIUMBLOB NOT NULL,
  INDEX `ci_sessions_timestamp` (`timestamp` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ci_sessions_mutua_beneficiario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ci_sessions_mutua_beneficiario` (
  `id` VARCHAR(128) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` MEDIUMBLOB NOT NULL,
  INDEX `ci_sessions_timestamp` (`timestamp` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cidades` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_estados` INT(10) NOT NULL,
  `nome` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `codigo_IBGE` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `codigo_uf` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idestados` (`id_estados` ASC) VISIBLE,
  INDEX `idxcidades1` (`codigo_IBGE` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cliente_aplicacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cliente_aplicacao` (
  `id_cliente_aplicacao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadoras` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `bundle` VARCHAR(256) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `bundle_antigo` VARCHAR(256) NULL DEFAULT NULL,
  `package` VARCHAR(256) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `identificador` VARCHAR(80) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `data_registro` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `parse_rest_key` VARCHAR(80) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_master_key` VARCHAR(80) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_app_id` VARCHAR(80) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_client_id` VARCHAR(80) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_android_action` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `apn_certificado` VARCHAR(300) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `gcm_server_key` VARCHAR(300) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `excluido` TINYINT(4) NULL DEFAULT '0',
  `unificado` TINYINT(1) NOT NULL DEFAULT '0',
  `link_appstore` VARCHAR(256) NULL DEFAULT NULL,
  `link_googleplay` VARCHAR(256) NULL DEFAULT NULL,
  `notificacao_serverless` TINYINT(1) NOT NULL DEFAULT '0',
  `custom_app` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id_cliente_aplicacao`),
  INDEX `fk_msg_cliente_aplicacao_msg_clientes1_idx` (`id_operadoras` ASC) VISIBLE,
  INDEX `fk_msg_cliente_aplicacao_msg_aplicacao1_idx` (`id_aplicacao` ASC) VISIBLE,
  INDEX `indice_identificador_excluido` (`identificador` ASC, `excluido` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cliente_aplicacao_preferencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cliente_aplicacao_preferencia` (
  `id_cliente_aplicacao_preferencia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_operadoras` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `versao` INT(11) NULL DEFAULT NULL,
  `funcionalidades` TEXT NULL DEFAULT NULL,
  `propriedades` TEXT NULL DEFAULT NULL,
  `situacao` TINYINT(4) NULL DEFAULT '0',
  `data_regsitro` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `excluido` TINYINT(4) NULL DEFAULT '0',
  PRIMARY KEY (`id_cliente_aplicacao_preferencia`),
  INDEX `fk_cliente_aplicacao_preferencia_x_cliente_aplicacao_idx` (`id_operadoras` ASC, `id_aplicacao` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cliente_aplicacao_propriedade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cliente_aplicacao_propriedade` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(144) NOT NULL DEFAULT '',
  `descricao` TEXT NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `valor_default` VARCHAR(255) NULL DEFAULT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cliente_preferencia_limite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cliente_preferencia_limite` (
  `id_cliente_preferencia_limite` INT(11) NOT NULL AUTO_INCREMENT,
  `bundle` VARCHAR(45) NULL DEFAULT NULL,
  `package` VARCHAR(45) NULL DEFAULT NULL,
  `versao` INT(11) NULL DEFAULT NULL,
  `versao_app_ios` DECIMAL(10,4) NULL DEFAULT NULL,
  `versao_app_android` DECIMAL(10,4) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente_preferencia_limite`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cliente_rota`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cliente_rota` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `endereco` VARCHAR(255) NULL DEFAULT NULL,
  `tipo` VARCHAR(10) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `chave` VARCHAR(50) NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `login` VARCHAR(100) NULL DEFAULT NULL,
  `senha` VARCHAR(100) NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `data_atualizacao` DATETIME NULL DEFAULT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_cliente` (
  `id_cms_cliente` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(75) NULL DEFAULT NULL,
  `cod_operadora` INT(11) NULL DEFAULT NULL,
  `logo_filename` VARCHAR(45) NULL DEFAULT NULL,
  `id_cliente_aplicacao` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_aplicacao_2` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_arquivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_arquivo` (
  `id_cms_arquivo` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_original` VARCHAR(255) NULL DEFAULT NULL,
  `tipo_arquivo` VARCHAR(75) NULL DEFAULT NULL,
  `embed` LONGTEXT NULL DEFAULT NULL,
  `url` LONGTEXT NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  `filename` VARCHAR(255) NULL DEFAULT NULL,
  `dir` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_arquivo`),
  INDEX `fk_cms_file_cms_cliente1_idx` (`cliente` ASC) VISIBLE,
  CONSTRAINT `cms_arquivo_ibfk_1`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_arquivo_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_arquivo_info` (
  `id_cms_arquivo_info` INT(11) NOT NULL AUTO_INCREMENT,
  `legenda` VARCHAR(255) NULL DEFAULT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  `titulo` VARCHAR(255) NULL DEFAULT NULL,
  `texto_alternativo` VARCHAR(255) NULL DEFAULT NULL,
  `idioma` VARCHAR(10) NULL DEFAULT NULL,
  `arquivo` INT(11) NULL DEFAULT NULL,
  `largura` INT(11) NULL DEFAULT NULL,
  `altura` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_arquivo_info`),
  INDEX `fk_cms_file_info_cms_file1_idx` (`arquivo` ASC) VISIBLE,
  CONSTRAINT `cms_arquivo_info_ibfk_1`
    FOREIGN KEY (`arquivo`)
    REFERENCES `thweb_homologacao`.`cms_arquivo` (`id_cms_arquivo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_ausencia_consultorio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_ausencia_consultorio` (
  `id_cms_ausencia_consultorio` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `data_inicio` DATETIME NOT NULL,
  `data_fim` DATETIME NOT NULL,
  `motivo` TEXT NULL DEFAULT NULL,
  `nome` VARCHAR(200) NULL DEFAULT NULL,
  `conselho_regional` VARCHAR(100) NULL DEFAULT NULL,
  `data_solicitacao` DATETIME NULL DEFAULT NULL,
  `id_protocolo` INT(11) NOT NULL,
  `excluido` TINYINT(1) NULL DEFAULT '0',
  `data_exclusao` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_ausencia_consultorio`),
  INDEX `id_protocolo_idx` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `id_protocolo_reference`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_ausencia_consultorio_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_ausencia_consultorio_config` (
  `id_configuracao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `assunto` VARCHAR(100) NOT NULL,
  `assunto_exclusao` VARCHAR(100) NOT NULL,
  `mensagem` TEXT NOT NULL,
  `mensagem_exclusao` TEXT NOT NULL,
  `email_destinatarios` VARCHAR(150) NOT NULL,
  `email_destinatarios_exclusao` VARCHAR(150) NOT NULL,
  `nome_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `nome_remetente_exclusao` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_configuracao`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_autor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_autor` (
  `id_cms_autor` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NULL DEFAULT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `foto` VARCHAR(255) NULL DEFAULT NULL,
  `social1` VARCHAR(255) NULL DEFAULT NULL,
  `social2` VARCHAR(255) NULL DEFAULT NULL,
  `social3` VARCHAR(255) NULL DEFAULT NULL,
  `social4` VARCHAR(255) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_autor`),
  INDEX `fk_cms_autor_cms_cliente1_idx` (`cliente` ASC) VISIBLE,
  CONSTRAINT `cms_autor_ibfk_1`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_tipo` (
  `id_cms_post_tipo` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(125) NULL DEFAULT NULL,
  `chave` VARCHAR(125) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  `tipo_global` TINYINT(4) NULL DEFAULT NULL,
  `id_cms_post_tipo_pai` INT(11) NULL DEFAULT NULL,
  `ambiente` VARCHAR(45) NULL DEFAULT NULL,
  `cliente_edita_filhos` TINYINT(1) NULL DEFAULT '0',
  `possui_segmentacao` TINYINT(1) NULL DEFAULT '0',
  `ordem` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_post_tipo`),
  INDEX `fk_cms_post_type_cms_cliente1_idx` (`cliente` ASC) VISIBLE,
  CONSTRAINT `cms_post_tipo_ibfk_1`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_classe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_classe` (
  `id_cms_classe` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_post_tipo` INT(11) NULL DEFAULT NULL,
  `nome` VARCHAR(245) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  `tipo_classe` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_classe`),
  INDEX `cms_classe_cms_post_tipo_id_cms_post_tipo_fk` (`id_cms_post_tipo` ASC) VISIBLE,
  CONSTRAINT `cms_classe_cms_post_tipo_id_cms_post_tipo_fk`
    FOREIGN KEY (`id_cms_post_tipo`)
    REFERENCES `thweb_homologacao`.`cms_post_tipo` (`id_cms_post_tipo`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_cp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_cp` (
  `id_cms_cp` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(75) NULL DEFAULT NULL,
  `post_tipo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_cp`),
  INDEX `fk_cms_cp_cms_post_tipo1_idx` (`post_tipo` ASC) VISIBLE,
  CONSTRAINT `cms_cp_ibfk_1`
    FOREIGN KEY (`post_tipo`)
    REFERENCES `thweb_homologacao`.`cms_post_tipo` (`id_cms_post_tipo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_cp_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_cp_campo` (
  `id_cms_cp_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id` INT(11) NULL DEFAULT NULL,
  `rotulo` VARCHAR(125) NULL DEFAULT NULL,
  `nome_campo` VARCHAR(75) NULL DEFAULT NULL,
  `tipo_campo` VARCHAR(45) NOT NULL,
  `obrigatorio` TINYINT(4) NULL DEFAULT NULL,
  `valor_padrao` VARCHAR(200) NULL DEFAULT NULL,
  `placeholder` VARCHAR(200) NULL DEFAULT NULL,
  `prefixo` VARCHAR(45) NULL DEFAULT NULL,
  `sufixo` VARCHAR(45) NULL DEFAULT NULL,
  `campo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_cp_campo`),
  INDEX `fk_cms_cp_campo_cms_cp_campo1_idx` (`campo` ASC) VISIBLE,
  CONSTRAINT `cms_cp_campo_ibfk_1`
    FOREIGN KEY (`campo`)
    REFERENCES `thweb_homologacao`.`cms_cp` (`id_cms_cp`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_curso` (
  `id_cms_curso` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `conselho_regional` VARCHAR(100) NOT NULL DEFAULT '',
  `data` DATETIME NULL DEFAULT NULL,
  `nome` VARCHAR(150) NULL DEFAULT NULL,
  `status` INT(11) NOT NULL,
  `observacoes` TEXT NULL DEFAULT NULL,
  `id_protocolo` INT(11) NOT NULL,
  PRIMARY KEY (`id_cms_curso`),
  INDEX `id_protocolo_fk_idx` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `id_protocolo_fk`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_curso_arquivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_curso_arquivo` (
  `id_cms_curso_arquivo` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cms_curso` INT(11) UNSIGNED NOT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  `arquivo` VARCHAR(150) NULL DEFAULT NULL,
  `diretorio` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_curso_arquivo`),
  INDEX `id_cms_curso` (`id_cms_curso` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_curso_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_curso_config` (
  `id_configuracao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `assunto` VARCHAR(100) NOT NULL,
  `mensagem` TEXT NOT NULL,
  `email_destinatarios` VARCHAR(150) NOT NULL,
  `nome_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `html_email` TEXT NULL DEFAULT NULL,
  `html_botao_imagens` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_configuracao`))
ENGINE = MyISAM
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_customizacao_cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_customizacao_cliente` (
  `id_cms_customizacao_cliente` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `titulo_customizacao` VARCHAR(100) NOT NULL,
  `descricao_customizacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_customizacao_cliente`),
  INDEX `fk_cms_customizacao_cliente_operadoras1_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `fk_cms_customizacao_cliente_aplicacao1_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `fk_cms_customizacao_cliente_aplicacao1`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`),
  CONSTRAINT `fk_cms_customizacao_cliente_operadoras1`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_customizacao_cliente_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_customizacao_cliente_item` (
  `id_cms_customizacao_cliente_item` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_customizacao_cliente` INT(11) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `subdescricao` TEXT NULL DEFAULT NULL,
  `valor` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_cms_customizacao_cliente_item`),
  INDEX `fk_cms_customizacao_cliente_item_cms_customizacao_cliente1_idx` (`id_cms_customizacao_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_cms_customizacao_cliente_item_cms_customizacao_cliente1`
    FOREIGN KEY (`id_cms_customizacao_cliente`)
    REFERENCES `thweb_homologacao`.`cms_customizacao_cliente` (`id_cms_customizacao_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_customizacao_cliente_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_customizacao_cliente_perfil` (
  `id_cms_customizacao_cliente_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_perfil_acesso` INT(11) NOT NULL,
  `id_cms_customizacao_cliente` INT(11) NOT NULL,
  PRIMARY KEY (`id_cms_customizacao_cliente_perfil`),
  INDEX `fk_perfil_acesso_cms_customizacao_cliente` (`id_perfil_acesso` ASC) VISIBLE,
  INDEX `fk_cms_customizacao_cliente_perfil_acesso` (`id_cms_customizacao_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_cms_customizacao_cliente_perfil_acesso`
    FOREIGN KEY (`id_cms_customizacao_cliente`)
    REFERENCES `thweb_homologacao`.`cms_customizacao_cliente` (`id_cms_customizacao_cliente`),
  CONSTRAINT `fk_perfil_acesso_cms_customizacao_cliente`
    FOREIGN KEY (`id_perfil_acesso`)
    REFERENCES `thweb_homologacao`.`perfil_acesso` (`id_perfil_acesso`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_evento_assembleias_presenca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_evento_assembleias_presenca` (
  `id_cms_evento_assembleias_presenca` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_post` INT(11) NULL DEFAULT NULL,
  `chave_cooperado` VARCHAR(75) NULL DEFAULT NULL,
  `numero_conselho_regional` VARCHAR(75) NULL DEFAULT NULL,
  `data_inscricao` DATETIME NULL DEFAULT NULL,
  `data_checkin` DATETIME NULL DEFAULT NULL,
  `titulo` VARCHAR(15) NULL DEFAULT NULL,
  `nome` VARCHAR(240) NULL DEFAULT NULL,
  `email` TEXT NULL DEFAULT NULL,
  `celular` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_evento_assembleias_presenca`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_menu_site_grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_menu_site_grupo` (
  `id_cms_menu_site_grupo` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(50) NOT NULL,
  `chave` VARCHAR(50) NOT NULL,
  `base_url_producao` VARCHAR(255) NULL DEFAULT NULL,
  `base_url_homologacao` VARCHAR(255) NULL DEFAULT NULL,
  `base_url_dev` VARCHAR(255) NULL DEFAULT NULL,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `qtd_niveis` INT(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_menu_site_grupo`),
  INDEX `id_operadora` (`id_operadora` ASC) VISIBLE,
  INDEX `id_aplicacao` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `cms_menu_site_grupo_ibfk_2`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `cms_menu_site_grupo_ibfk_3`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_menu_site`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_menu_site` (
  `id_menu` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT '',
  `tooltip` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `pai` INT(11) UNSIGNED NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT '0',
  `link` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `post_cms_id` INT(11) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  `tipo_link` VARCHAR(11) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `chave` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `tipo_conteudo` INT(11) NULL DEFAULT NULL,
  `menu_site_grupo` INT(11) UNSIGNED NULL DEFAULT NULL,
  `tags` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `post_tipo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`),
  INDEX `cliente` (`cliente` ASC) VISIBLE,
  INDEX `pai` (`pai` ASC) VISIBLE,
  INDEX `post_cms_id` (`post_cms_id` ASC) VISIBLE,
  INDEX `menu_site_grupo` (`menu_site_grupo` ASC) VISIBLE,
  CONSTRAINT `cms_menu_site_ibfk_2`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`),
  CONSTRAINT `cms_menu_site_ibfk_3`
    FOREIGN KEY (`pai`)
    REFERENCES `thweb_homologacao`.`cms_menu_site` (`id_menu`),
  CONSTRAINT `cms_menu_site_ibfk_5`
    FOREIGN KEY (`menu_site_grupo`)
    REFERENCES `thweb_homologacao`.`cms_menu_site_grupo` (`id_cms_menu_site_grupo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_permalink_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_permalink_config` (
  `id_cms_permalink_config` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_cliente` INT(11) NOT NULL,
  `id_cms_post_tipo` INT(11) NOT NULL,
  `titulo_prefixo` VARCHAR(255) NULL DEFAULT NULL,
  `cor_titulo` VARCHAR(7) NULL DEFAULT NULL,
  `cor_bg` VARCHAR(7) NULL DEFAULT NULL,
  `cor_data` VARCHAR(7) NULL DEFAULT NULL,
  `cor_link` VARCHAR(7) NULL DEFAULT NULL,
  `cor_categoria_bg` VARCHAR(7) NULL DEFAULT NULL,
  `cor_categoria_texto` VARCHAR(7) NULL DEFAULT NULL,
  `img_topo_logo` VARCHAR(75) NULL DEFAULT NULL,
  `img_topo_bg` VARCHAR(45) NULL DEFAULT NULL,
  `img_body_bg` VARCHAR(45) NULL DEFAULT NULL,
  `img_app_icon` VARCHAR(45) NULL DEFAULT NULL,
  `img_bg` VARCHAR(45) NULL DEFAULT NULL,
  `img_logo_principal` VARCHAR(45) NULL DEFAULT NULL,
  `img_logo_menor` VARCHAR(45) NULL DEFAULT NULL,
  `img_social` VARCHAR(45) NULL DEFAULT NULL,
  `share_fb` TINYINT(1) NULL DEFAULT NULL,
  `share_twitter` TINYINT(1) NULL DEFAULT NULL,
  `share_google` TINYINT(1) NULL DEFAULT NULL,
  `curtir_facebook` TINYINT(1) NULL DEFAULT NULL,
  `css_personalizado` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_permalink_config`),
  INDEX `fk_cms_permalink_config_cms_cliente1_idx` (`id_cms_cliente` ASC) VISIBLE,
  INDEX `fk_cms_permalink_config_cms_post_tipo1_idx` (`id_cms_post_tipo` ASC) VISIBLE,
  CONSTRAINT `fk_cms_permalink_config_cms_cliente1`
    FOREIGN KEY (`id_cms_cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`),
  CONSTRAINT `fk_cms_permalink_config_cms_post_tipo1`
    FOREIGN KEY (`id_cms_post_tipo`)
    REFERENCES `thweb_homologacao`.`cms_post_tipo` (`id_cms_post_tipo`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_plano`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_plano` (
  `id_cms_plano` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_cliente` INT(11) NOT NULL,
  `nome` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `descricao` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `imagem` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `acomodacao` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `abrangencia` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `segmentacao` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `carencias` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `tipo_contratacao` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_plano`),
  INDEX `id_cms_cliente` (`id_cms_cliente` ASC) VISIBLE,
  CONSTRAINT `cms_plano_ibfk_1`
    FOREIGN KEY (`id_cms_cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post` (
  `id_cms_post` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo` LONGTEXT NULL DEFAULT NULL,
  `chave` VARCHAR(255) NULL DEFAULT NULL,
  `conteudo` LONGTEXT NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_modificacao` DATETIME NULL DEFAULT NULL,
  `data_publicacao` DATETIME NULL DEFAULT NULL,
  `data_validade` DATETIME NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(30) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  `idioma` VARCHAR(10) NULL DEFAULT NULL,
  `post_tipo` INT(11) NULL DEFAULT NULL,
  `autor` INT(11) NULL DEFAULT NULL,
  `formato` VARCHAR(45) NULL DEFAULT NULL,
  `width_exibicao` INT(11) NULL DEFAULT NULL,
  `height_exibicao` INT(11) NULL DEFAULT NULL,
  `tipo_exibicao` VARCHAR(1) NULL DEFAULT NULL,
  `exibir_titulo` VARCHAR(1) NULL DEFAULT 'S',
  `foto_principal` VARCHAR(200) NULL DEFAULT NULL,
  `foto_principal_dir` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_post`),
  INDEX `fk_cms_post_cms_autor1_idx` (`autor` ASC) VISIBLE,
  INDEX `fk_cms_post_cms_cliente1_idx` (`cliente` ASC) VISIBLE,
  INDEX `fk_cms_post_cms_post_type1_idx` (`post_tipo` ASC) VISIBLE,
  CONSTRAINT `cms_post_ibfk_1`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`),
  CONSTRAINT `cms_post_ibfk_2`
    FOREIGN KEY (`post_tipo`)
    REFERENCES `thweb_homologacao`.`cms_post_tipo` (`id_cms_post_tipo`),
  CONSTRAINT `cms_post_ibfk_3`
    FOREIGN KEY (`autor`)
    REFERENCES `thweb_homologacao`.`cms_autor` (`id_cms_autor`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_taxonomia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_taxonomia` (
  `id_cms_taxonomia` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NULL DEFAULT NULL,
  `chave` VARCHAR(200) NULL DEFAULT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  `unico` TINYINT(4) NULL DEFAULT '0',
  `obrigatorio` TINYINT(4) NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_taxonomia`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_arquivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_arquivo` (
  `id_cms_post_arquivo` INT(11) NOT NULL AUTO_INCREMENT,
  `ordem` INT(11) NULL DEFAULT NULL,
  `post` INT(11) NULL DEFAULT NULL,
  `arquivo` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(75) NULL DEFAULT NULL,
  `taxonomia` INT(11) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_post_arquivo`),
  INDEX `fk_cms_post_arquivo_cms_taxonomia1_idx` (`taxonomia` ASC, `cliente` ASC) VISIBLE,
  INDEX `fk_cms_post_arquivo_type_cms_arquivo1_idx` (`arquivo` ASC) VISIBLE,
  INDEX `fk_cms_post_arquivo_type_cms_post1_idx` (`post` ASC) VISIBLE,
  CONSTRAINT `cms_post_arquivo_ibfk_1`
    FOREIGN KEY (`post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`),
  CONSTRAINT `cms_post_arquivo_ibfk_2`
    FOREIGN KEY (`arquivo`)
    REFERENCES `thweb_homologacao`.`cms_arquivo` (`id_cms_arquivo`),
  CONSTRAINT `cms_post_arquivo_ibfk_3`
    FOREIGN KEY (`taxonomia`)
    REFERENCES `thweb_homologacao`.`cms_taxonomia` (`id_cms_taxonomia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_classe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_classe` (
  `idcms_post_classe` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_classe` INT(11) NULL DEFAULT NULL,
  `id_cms_post` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idcms_post_classe`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_formulario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_formulario` (
  `id_cms_post_formulario` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_post` INT(11) NOT NULL,
  `id_form` VARCHAR(255) NOT NULL,
  `email_to` VARCHAR(255) NOT NULL,
  `email_cc` VARCHAR(255) NULL DEFAULT NULL,
  `assunto` VARCHAR(255) NOT NULL,
  `tipo` VARCHAR(255) NULL DEFAULT 'txt',
  `email_from` VARCHAR(255) NOT NULL DEFAULT 'nao-responda@mobilesaude.com.br',
  `submit_btn_text` VARCHAR(255) NOT NULL DEFAULT 'Enviar',
  `cliente` INT(11) NOT NULL,
  `mensagem` TEXT NOT NULL,
  PRIMARY KEY (`id_cms_post_formulario`),
  INDEX `cms_post_formulario_cms_cliente_id_cms_cliente_fk` (`cliente` ASC) VISIBLE,
  INDEX `cms_post_formulario_cms_post_id_cms_post_fk` (`id_cms_post` ASC) VISIBLE,
  CONSTRAINT `cms_post_formulario_cms_cliente_id_cms_cliente_fk`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`),
  CONSTRAINT `cms_post_formulario_cms_post_id_cms_post_fk`
    FOREIGN KEY (`id_cms_post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_termo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_termo` (
  `id_cms_termo` INT(11) NOT NULL AUTO_INCREMENT,
  `chave` VARCHAR(125) NULL DEFAULT NULL,
  `grupo` VARCHAR(125) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_termo`),
  INDEX `fk_cms_termo_cms_cliente1_idx` (`cliente` ASC) VISIBLE,
  CONSTRAINT `cms_termo_ibfk_1`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_meta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_meta` (
  `id_cms_post_meta` INT(11) NOT NULL AUTO_INCREMENT,
  `post` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(200) NULL DEFAULT NULL,
  `valor` LONGTEXT NULL DEFAULT NULL,
  `tipo` VARCHAR(125) NULL DEFAULT NULL,
  `arquivo` INT(11) NULL DEFAULT NULL,
  `termo` INT(11) NULL DEFAULT NULL,
  `cp_campo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_post_meta`),
  INDEX `fk_cms_post_meta_cms_arquivo1_idx` (`arquivo` ASC) VISIBLE,
  INDEX `fk_cms_post_meta_cms_cp_campo1_idx` (`cp_campo` ASC) VISIBLE,
  INDEX `fk_cms_post_meta_cms_post1_idx` (`post` ASC) VISIBLE,
  INDEX `fk_cms_post_meta_cms_termo1_idx` (`termo` ASC) VISIBLE,
  CONSTRAINT `cms_post_meta_ibfk_1`
    FOREIGN KEY (`post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`),
  CONSTRAINT `cms_post_meta_ibfk_2`
    FOREIGN KEY (`arquivo`)
    REFERENCES `thweb_homologacao`.`cms_arquivo` (`id_cms_arquivo`),
  CONSTRAINT `cms_post_meta_ibfk_3`
    FOREIGN KEY (`termo`)
    REFERENCES `thweb_homologacao`.`cms_termo` (`id_cms_termo`),
  CONSTRAINT `cms_post_meta_ibfk_4`
    FOREIGN KEY (`cp_campo`)
    REFERENCES `thweb_homologacao`.`cms_cp_campo` (`id_cms_cp_campo`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_midia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_midia` (
  `id_cms_post_midia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_post` INT(11) NULL DEFAULT NULL,
  `titulo` VARCHAR(245) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `texto_alternativo` VARCHAR(245) NULL DEFAULT NULL,
  `largura` INT(11) NULL DEFAULT NULL,
  `altura` INT(11) NULL DEFAULT NULL,
  `embed` TEXT NULL DEFAULT NULL,
  `tipo_midia` VARCHAR(50) NULL DEFAULT NULL,
  `dir` VARCHAR(200) NULL DEFAULT NULL,
  `arquivo` VARCHAR(200) NULL DEFAULT NULL,
  `arquivo_nome_original` VARCHAR(200) NULL DEFAULT NULL,
  `url` TEXT NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_post_midia`),
  UNIQUE INDEX `cms_post_midia_id_cms_post_midia_uindex` (`id_cms_post_midia` ASC) VISIBLE,
  INDEX `fk_cms_post` (`id_cms_post` ASC) VISIBLE,
  CONSTRAINT `fk_cms_post`
    FOREIGN KEY (`id_cms_post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_perfil` (
  `id_cms_post_perfil` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_post` INT(11) NOT NULL,
  `id_perfil_acesso` INT(11) NOT NULL,
  PRIMARY KEY (`id_cms_post_perfil`),
  INDEX `fk_id_cms_post` (`id_cms_post` ASC) VISIBLE,
  INDEX `fk_perfil_acesso` (`id_perfil_acesso` ASC) VISIBLE,
  CONSTRAINT `fk_id_cms_post`
    FOREIGN KEY (`id_cms_post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`),
  CONSTRAINT `fk_perfil_acesso`
    FOREIGN KEY (`id_perfil_acesso`)
    REFERENCES `thweb_homologacao`.`perfil_acesso` (`id_perfil_acesso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_sanfona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_sanfona` (
  `id_cms_post_sanfona` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_post` INT(11) NULL DEFAULT NULL,
  `titulo` VARCHAR(255) NULL DEFAULT NULL,
  `texto` LONGTEXT NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_post_sanfona`),
  INDEX `cms_post_sanfona_cms_post_id_cms_post_fk` (`id_cms_post` ASC) VISIBLE,
  CONSTRAINT `cms_post_sanfona_ibfk_1`
    FOREIGN KEY (`id_cms_post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_segmento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_segmento` (
  `id_cms_post_segmento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_msg_cliente_segmento` INT(11) NULL DEFAULT NULL,
  `id_cms_post` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(75) NULL DEFAULT NULL,
  `descricao` VARCHAR(145) NULL DEFAULT NULL,
  `valor` VARCHAR(75) NULL DEFAULT NULL,
  `operador` VARCHAR(5) NULL DEFAULT NULL,
  `valor_descricao` VARCHAR(145) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_post_segmento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_termo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_termo` (
  `id_cms_post_termo` INT(11) NOT NULL AUTO_INCREMENT,
  `post` INT(11) NULL DEFAULT NULL,
  `termo` INT(11) NOT NULL,
  `taxonomia` INT(11) NOT NULL,
  `ordem` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(75) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_post_termo`),
  INDEX `fk_cms_post_termo_cms_post1_idx` (`post` ASC) VISIBLE,
  INDEX `fk_cms_post_termo_cms_termo1_idx` (`termo` ASC) VISIBLE,
  INDEX `fk_cms_post_termo_cms_termo_taxonomia1_idx` (`taxonomia` ASC) VISIBLE,
  CONSTRAINT `cms_post_termo_ibfk_1`
    FOREIGN KEY (`post`)
    REFERENCES `thweb_homologacao`.`cms_post` (`id_cms_post`),
  CONSTRAINT `cms_post_termo_ibfk_2`
    FOREIGN KEY (`termo`)
    REFERENCES `thweb_homologacao`.`cms_termo` (`id_cms_termo`),
  CONSTRAINT `cms_post_termo_ibfk_3`
    FOREIGN KEY (`taxonomia`)
    REFERENCES `thweb_homologacao`.`cms_taxonomia` (`id_cms_taxonomia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_post_tipo_formato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_post_tipo_formato` (
  `id_cms_post_tipo_formato` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `post_tipo` INT(11) NULL DEFAULT NULL,
  `cliente` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_post_tipo_formato`),
  INDEX `fk_cms_post_tipo_formato_cms_cliente1_idx` (`cliente` ASC) VISIBLE,
  INDEX `fk_cms_post_tipo_formato_cms_post_tipo1_idx` (`post_tipo` ASC) VISIBLE,
  CONSTRAINT `cms_post_tipo_formato_ibfk_1`
    FOREIGN KEY (`post_tipo`)
    REFERENCES `thweb_homologacao`.`cms_post_tipo` (`id_cms_post_tipo`),
  CONSTRAINT `cms_post_tipo_formato_ibfk_2`
    FOREIGN KEY (`cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_rede_credenciada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_rede_credenciada` (
  `id_cms_rede_credenciada` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo_operadora` VARCHAR(125) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `nome` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `link_pagina` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `logo` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `id_cms_cliente` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_rede_credenciada`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_tabela_preco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_tabela_preco` (
  `id_cms_tabela_preco` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cms_plano` INT(11) NOT NULL,
  `id_cms_cliente` INT(11) NOT NULL,
  `ordem` INT(11) NULL DEFAULT NULL,
  `idade_inicial` INT(11) NOT NULL,
  `idade_final` INT(11) NOT NULL,
  `valor` DECIMAL(10,0) NOT NULL,
  PRIMARY KEY (`id_cms_tabela_preco`),
  INDEX `fk_cms_tabela_preco_1_idx` (`id_cms_plano` ASC) VISIBLE,
  INDEX `fk_cms_tabela_preco_2_idx` (`id_cms_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_cms_tabela_preco_1`
    FOREIGN KEY (`id_cms_plano`)
    REFERENCES `thweb_homologacao`.`cms_plano` (`id_cms_plano`)
    ON DELETE CASCADE,
  CONSTRAINT `fk_cms_tabela_preco_2`
    FOREIGN KEY (`id_cms_cliente`)
    REFERENCES `thweb_homologacao`.`cms_cliente` (`id_cms_cliente`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`cms_termo_string`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`cms_termo_string` (
  `id_cms_termo_string` INT(11) NOT NULL AUTO_INCREMENT,
  `valor` VARCHAR(255) NULL DEFAULT NULL,
  `chave` VARCHAR(200) NULL DEFAULT NULL,
  `termo` INT(11) NULL DEFAULT NULL,
  `idioma` VARCHAR(10) NULL DEFAULT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  `ordem` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_cms_termo_string`),
  INDEX `fk_cms_termo_string_cms_termo1_idx` (`termo` ASC) VISIBLE,
  CONSTRAINT `cms_termo_string_ibfk_1`
    FOREIGN KEY (`termo`)
    REFERENCES `thweb_homologacao`.`cms_termo` (`id_cms_termo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`configuracao_cemig`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`configuracao_cemig` (
  `id` INT(2) NOT NULL AUTO_INCREMENT,
  `ds_url_agendamento` VARCHAR(100) NOT NULL DEFAULT 'http://www2.cemigsaude.org.br/hs-apicemig-homologacao/',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`configuracao_epharma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`configuracao_epharma` (
  `id_configuracao_epharma` INT(11) NOT NULL,
  `flag_flood` INT(11) NOT NULL,
  `fator_flood` INT(11) NOT NULL,
  PRIMARY KEY (`id_configuracao_epharma`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`configuracao_importador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`configuracao_importador` (
  `id_configuracao` INT(11) NOT NULL,
  `email_remetente` VARCHAR(100) NOT NULL,
  `senha_remetente` VARCHAR(45) NOT NULL,
  `apelido_remetente` VARCHAR(45) NULL DEFAULT NULL,
  `host_smtp` VARCHAR(45) NOT NULL,
  `port_smtp` INT(11) NULL DEFAULT '25',
  `email_notificacao` VARCHAR(200) NULL DEFAULT NULL,
  `titulo_email` VARCHAR(200) NULL DEFAULT NULL,
  `titulo_email_advertencia` VARCHAR(200) NULL DEFAULT NULL,
  `titulo_email_erro` VARCHAR(200) NULL DEFAULT NULL,
  `endereco_especialidades_unimed` VARCHAR(100) NULL DEFAULT NULL,
  `endereco_tipo_prestadores_unimed` VARCHAR(100) NULL DEFAULT NULL,
  `endereco_sub_espec_unimed` VARCHAR(100) NULL DEFAULT NULL,
  `importador_habilitado` CHAR(1) NULL DEFAULT NULL,
  `geocoding_habilitado` CHAR(1) NULL DEFAULT NULL,
  `campanha_link_envio` VARCHAR(200) NULL DEFAULT NULL,
  `campanha_threads_envio` INT(11) NULL DEFAULT NULL,
  `versao_suportada_ptu_400` INT(2) NOT NULL DEFAULT '23',
  `versao_suportada_ptu_450` INT(2) NOT NULL DEFAULT '4',
  PRIMARY KEY (`id_configuracao`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`grupo_integracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`grupo_integracao` (
  `id_grupo_integracao` INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(255) NOT NULL COMMENT 'Código do grupo de integração:\\nautenticacao\\nseg_via_cartao\\nreembolso\\netc',
  `nome` VARCHAR(255) NOT NULL COMMENT 'Nome do Grupo de Integração:\\nAutenticação\\nSegunda via de Cartão\\nReembolso\\n',
  PRIMARY KEY (`id_grupo_integracao`),
  UNIQUE INDEX `code` (`code` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`integracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`integracao` (
  `id_integracao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_grupo_integracao` INT(11) NOT NULL,
  `code` VARCHAR(255) NOT NULL COMMENT 'Código da integração:\\nlogin | recuperar_senha | primeiro_acesso | etc.',
  `nome` VARCHAR(255) NOT NULL COMMENT 'Nome da integração:\\nLogin | Recuperar Senha | Esqueci Senha | Primeiro Acesso | etc.',
  `descricao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_integracao`),
  INDEX `fk_integracao_1_idx` (`id_grupo_integracao` ASC) VISIBLE,
  CONSTRAINT `fk_integracao_1`
    FOREIGN KEY (`id_grupo_integracao`)
    REFERENCES `thweb_homologacao`.`grupo_integracao` (`id_grupo_integracao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`configuracao_integracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`configuracao_integracao` (
  `id_configuracao_integracao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_integracao` INT(11) NOT NULL,
  `id_cliente` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_form` VARCHAR(255) NOT NULL,
  `url_webservice` VARCHAR(255) NOT NULL,
  `header` JSON NULL DEFAULT NULL,
  `timeout` INT(5) NOT NULL DEFAULT '5',
  `ativo` TINYINT(1) NOT NULL DEFAULT '0',
  `versao` INT(11) NOT NULL DEFAULT '1',
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_atualizacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `excluido` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_configuracao_integracao`),
  INDEX `fk_configuracao_integracao_1_idx` (`id_integracao` ASC) VISIBLE,
  INDEX `fk_configuracao_integracao_2_idx` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_configuracao_integracao_3_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `fk_configuracao_integracao_1`
    FOREIGN KEY (`id_integracao`)
    REFERENCES `thweb_homologacao`.`integracao` (`id_integracao`),
  CONSTRAINT `fk_configuracao_integracao_2`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_configuracao_integracao_3`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`configuracao_mensageria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`configuracao_mensageria` (
  `id_configuracao` INT(11) NOT NULL,
  `envia_sms` INT(11) NULL DEFAULT NULL,
  `envia_push` INT(11) NULL DEFAULT NULL,
  `token_sms` VARCHAR(90) NULL DEFAULT NULL,
  `tamanho_bloco_sms` INT(11) NULL DEFAULT NULL,
  `tamanho_bloco_push` INT(11) NULL DEFAULT NULL,
  `tempo_intervalo_sms` INT(11) NULL DEFAULT NULL,
  `tempo_intervalo_push` INT(11) NULL DEFAULT NULL,
  `timeout_bloco_sms` INT(11) NULL DEFAULT NULL,
  `qt_hora_download_cert` INT(2) NOT NULL DEFAULT '1',
  `fs_num_tokens_por_interacao` INT(11) NULL DEFAULT '500',
  `fs_intervalo_por_interacao_seg` INT(11) NULL DEFAULT '2',
  `qt_tentativa_envio_sms` INT(11) NOT NULL DEFAULT '3',
  `ds_url_zenvia` VARCHAR(100) NOT NULL DEFAULT 'https://api-rest.zenvia360.com.br/services/send-sms',
  `ds_url_zenvia_multiple` VARCHAR(100) NOT NULL DEFAULT 'https://api-rest.zenvia360.com.br/services/send-sms-multiple',
  `ds_url_memcache` VARCHAR(100) NOT NULL DEFAULT 'localhost',
  `nr_port_memcache` INT(11) NOT NULL DEFAULT '11211',
  `operadora_piloto` VARCHAR(250) NULL DEFAULT NULL,
  `mili_timeout_push` INT(11) NULL DEFAULT '1500',
  `qt_segundos_reenvio` INT(11) NULL DEFAULT '10',
  PRIMARY KEY (`id_configuracao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`configuracao_ms_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`configuracao_ms_usuario` (
  `id_configuracao_ms_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `senha_master` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id_configuracao_ms_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`contatos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`contatos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `descricao` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `ordem` INT(11) NOT NULL,
  `telefone` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `fax` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `observacoes` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `celular` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`corpo_clinico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`corpo_clinico` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `id_especialidade` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `codigo_legado` VARCHAR(6) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `nome` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `habilita_pesquisa_nome` VARCHAR(1) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `distancia` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `cpf_cnpj` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `sequencial_endereco` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `sigla_conselho_regional` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `uf_conselho_regional` VARCHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `numero_conselho_regional` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `empresa` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `especialidade` (`id_operadora` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxcorpo1` (`id_operadora` ASC, `cpf_cnpj` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxcorponome` (`id_operadora` ASC, `nome` ASC, `id_especialidade` ASC, `cpf_cnpj` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`corpo_clinico_tmp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`corpo_clinico_tmp` (
  `id_operadora` INT(10) NULL DEFAULT NULL,
  `id_especialidade` VARCHAR(10) NULL DEFAULT NULL,
  `codigo_legado` VARCHAR(6) NULL DEFAULT NULL,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `cpf_cnpj` VARCHAR(20) NULL DEFAULT NULL,
  `sequencial_endereco` VARCHAR(20) NULL DEFAULT NULL,
  `data_bloqueio` DATE NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT NULL,
  `operacao` VARCHAR(1) NULL DEFAULT NULL,
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT NULL,
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT NULL,
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL)
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`demo_agendamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`demo_agendamento` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_prestador` INT(11) NOT NULL,
  `especialidade` VARCHAR(255) NOT NULL,
  `horario` TIME NOT NULL,
  `data` DATE NOT NULL,
  `matricula` VARCHAR(100) NOT NULL,
  `nome_beneficiario` VARCHAR(100) NOT NULL,
  `id_unidade_atendimento` INT(11) NOT NULL,
  `email_beneficiario` VARCHAR(255) NULL DEFAULT NULL,
  `telefone_beneficiario` VARCHAR(15) NULL DEFAULT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `acesso_v2` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`demo_usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`demo_usuarios` (
  `id_demo_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `login` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `empresa` VARCHAR(100) NOT NULL,
  `telefone` VARCHAR(15) NULL DEFAULT NULL,
  `matricula` VARCHAR(20) NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `latitude` VARCHAR(50) NULL DEFAULT NULL,
  `longitude` VARCHAR(50) NULL DEFAULT NULL,
  `email_confirmado` TINYINT(1) NOT NULL DEFAULT '0',
  `token_validacao` VARCHAR(255) NULL DEFAULT NULL,
  `acesso_v2` TEXT NULL DEFAULT NULL,
  `cooperado` JSON NULL DEFAULT NULL,
  `agentes_relacionamento` JSON NULL DEFAULT NULL,
  `perfil_beneficiario` TINYINT(1) NOT NULL DEFAULT '1',
  `perfil_cooperado` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_demo_usuario`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC, `id_operadora` ASC, `id_aplicacao` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`dev_aposentados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`dev_aposentados` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `telefone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`dev_associados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`dev_associados` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `telefone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`dev_funcionarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`dev_funcionarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `telefone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`enderecos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`enderecos` (
  `id_endereco` INT(11) NOT NULL AUTO_INCREMENT,
  `cd_endereco` VARCHAR(255) NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `data_inclusao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_endereco`),
  UNIQUE INDEX `cd_endereco_UNIQUE` (`cd_endereco` ASC) VISIBLE)
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`enderecos_bkp_hom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`enderecos_bkp_hom` (
  `id_endereco` INT(11) NOT NULL DEFAULT '0',
  `cd_endereco` VARCHAR(255) NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `data_inclusao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`enderecos_prod`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`enderecos_prod` (
  `id_endereco` INT(11) NOT NULL DEFAULT '0',
  `cd_endereco` VARCHAR(255) NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_endereco`),
  INDEX `idx_cd_endereco` (`cd_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`especialidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`especialidades` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadoras` INT(10) NOT NULL,
  `codigo_CBO` VARCHAR(10) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxespecialidade1` (`id_operadoras` ASC, `codigo_CBO` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`estados` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uf` VARCHAR(2) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `codigo_IBGE` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxestados1` (`uf` ASC, `id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`excecao_ptu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`excecao_ptu` (
  `id_excecao_ptu` INT(11) NOT NULL AUTO_INCREMENT,
  `ID_OPERADORA` INT(11) NOT NULL,
  `REDE_ATEND` VARCHAR(4) NULL DEFAULT NULL,
  `NM_PRODUTO` VARCHAR(60) NULL DEFAULT NULL,
  `REG_PLANO_ANS` VARCHAR(20) NULL DEFAULT NULL,
  `ID_INCLU_EXCLU` INT(1) NOT NULL,
  `CD_UNI_PRE` INT(4) NOT NULL,
  `CD_CNPJ_CPF` VARCHAR(15) NOT NULL,
  `CD_GR_SERV` INT(3) NULL DEFAULT NULL,
  `FL_EXISTE_PRESTADOR` INT(1) NOT NULL DEFAULT '0',
  `FL_TODA_REDE` VARCHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_excecao_ptu`),
  INDEX `idx_operadora_excecao` (`ID_OPERADORA` ASC) VISIBLE,
  INDEX `idx_excecao` (`REDE_ATEND` ASC, `NM_PRODUTO` ASC, `REG_PLANO_ANS` ASC, `ID_INCLU_EXCLU` ASC) VISIBLE,
  INDEX `idx_excecao_ptu_CD_CNPJ_CPF` (`CD_CNPJ_CPF` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`exemplo_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`exemplo_log` (
  `id_exemplo_log` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `descricao` TEXT NOT NULL,
  `message` TEXT NULL DEFAULT NULL,
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_update` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_exemplo_log`),
  INDEX `fk_exemplo_log_operadoras` (`id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_exemplo_log_operadoras`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`financeiro_fatura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`financeiro_fatura` (
  `id_fatura` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NULL DEFAULT NULL,
  `data_emissao` DATETIME NULL DEFAULT NULL,
  `data_vencimento` DATE NULL DEFAULT NULL,
  `data_competencia_inicio` DATE NULL DEFAULT NULL COMMENT 'Informe que a cobrança é referente de tal dia a tal dia',
  `data_competencia_fim` DATE NULL DEFAULT NULL,
  `data_pagamento` DATETIME NULL DEFAULT NULL,
  `valor_total` DECIMAL(10,2) NULL DEFAULT NULL,
  `descricao` VARCHAR(145) NULL DEFAULT NULL,
  `id_aplicacao` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id_fatura`),
  INDEX `fk_financeiro_fatura_operadoras1_idx` (`id_operadora` ASC) VISIBLE,
  CONSTRAINT `fk_financeiro_fatura_operadoras1`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`financeiro_fatura_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`financeiro_fatura_item` (
  `id_fatura_item` INT(11) NOT NULL AUTO_INCREMENT,
  `id_fatura` INT(11) NULL DEFAULT NULL,
  `desc_servico` TEXT NULL DEFAULT NULL,
  `quantidade` INT(11) NULL DEFAULT NULL,
  `valor_unitario` DECIMAL(9,4) NULL DEFAULT NULL,
  `valor_total` DECIMAL(9,4) NULL DEFAULT NULL,
  PRIMARY KEY (`id_fatura_item`),
  INDEX `fk_financeiro_fatura_item_financeiro_fatura_idx` (`id_fatura` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`funcionalidade_grupo_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`funcionalidade_grupo_campo` (
  `id_funcionalidade_grupo_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `ordem` INT(2) NOT NULL,
  `funcionalidade` VARCHAR(155) NOT NULL,
  `id_funcionalidade_tipo` INT(11) NOT NULL,
  PRIMARY KEY (`id_funcionalidade_grupo_campo`),
  INDEX `fk_funcionalidade_grupo_campo_1_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `fk_funcionalidade_grupo_campo_2_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `fk_funcionalidade_grupo_campo_1`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_funcionalidade_grupo_campo_2`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`funcionalidade_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`funcionalidade_campo` (
  `id_funcionalidade_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_funcionalidade_grupo_campo` INT(11) NOT NULL,
  `label` VARCHAR(200) NOT NULL,
  `tamanho` INT(2) NOT NULL DEFAULT '12',
  `valor_esperado` VARCHAR(255) NULL DEFAULT NULL,
  `texto_ajuda` TEXT NULL DEFAULT NULL,
  `obrigatorio` TINYINT(1) NOT NULL DEFAULT '1',
  `visivel` TINYINT(1) NOT NULL DEFAULT '1',
  `editavel` TINYINT(1) NOT NULL DEFAULT '1',
  `nome` VARCHAR(255) NOT NULL,
  `classe` VARCHAR(100) NULL DEFAULT NULL,
  `tipo` VARCHAR(100) NOT NULL DEFAULT 'text',
  `valor_padrao` VARCHAR(255) NULL DEFAULT NULL,
  `ref_beneficiario` VARCHAR(100) NULL DEFAULT NULL,
  `max_itens` INT(11) NOT NULL DEFAULT '1',
  `validacao_msg` TEXT NULL DEFAULT NULL,
  `validacao_regex` VARCHAR(255) NULL DEFAULT NULL,
  `ordem` INT(2) NOT NULL,
  `ref_nao_editavel` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_funcionalidade_campo`),
  INDEX `fk_funcionalidade_campo_1_idx` (`id_funcionalidade_grupo_campo` ASC) VISIBLE,
  CONSTRAINT `fk_funcionalidade_campo_1`
    FOREIGN KEY (`id_funcionalidade_grupo_campo`)
    REFERENCES `thweb_homologacao`.`funcionalidade_grupo_campo` (`id_funcionalidade_grupo_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`funcionalidade_campo_obrigatoriedade_cond`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`funcionalidade_campo_obrigatoriedade_cond` (
  `id_funcionalidade_campo_obrigatoriedade_cond` INT(11) NOT NULL AUTO_INCREMENT,
  `id_funcionalidade_campo` INT(11) NOT NULL,
  `id_funcionalidade_campo_obrigatorio` INT(11) NOT NULL,
  `tipo_obrigatoriedade` VARCHAR(100) NOT NULL DEFAULT 'alteracao',
  PRIMARY KEY (`id_funcionalidade_campo_obrigatoriedade_cond`),
  INDEX `fk_funcionalidade_campo_obrigatoriedade_cond_1_idx` (`id_funcionalidade_campo` ASC) VISIBLE,
  INDEX `fk_funcionalidade_campo_obrigatoriedade_cond_2_idx` (`id_funcionalidade_campo_obrigatorio` ASC) VISIBLE,
  CONSTRAINT `fk_funcionalidade_campo_obrigatoriedade_cond_1`
    FOREIGN KEY (`id_funcionalidade_campo`)
    REFERENCES `thweb_homologacao`.`funcionalidade_campo` (`id_funcionalidade_campo`),
  CONSTRAINT `fk_funcionalidade_campo_obrigatoriedade_cond_2`
    FOREIGN KEY (`id_funcionalidade_campo_obrigatorio`)
    REFERENCES `thweb_homologacao`.`funcionalidade_campo` (`id_funcionalidade_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`funcionalidade_campo_opcoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`funcionalidade_campo_opcoes` (
  `id_funcionalidade_campo_opcoes` INT(11) NOT NULL AUTO_INCREMENT,
  `id_funcionalidade_campo` INT(11) NOT NULL,
  `label` VARCHAR(100) NOT NULL,
  `valor` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_funcionalidade_campo_opcoes`),
  INDEX `fk_funcionalidade_campo_opcoes_1_idx` (`id_funcionalidade_campo` ASC) VISIBLE,
  CONSTRAINT `fk_funcionalidade_campo_opcoes_1`
    FOREIGN KEY (`id_funcionalidade_campo`)
    REFERENCES `thweb_homologacao`.`funcionalidade_campo` (`id_funcionalidade_campo`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`funcionalidade_protocolo_dados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`funcionalidade_protocolo_dados` (
  `id_funcionalidade_protocolo_dados` INT(11) NOT NULL AUTO_INCREMENT,
  `id_funcionalidade_campo` INT(11) NOT NULL,
  `id_funcionalidade_protocolo` INT(11) NOT NULL,
  `funcionalidade` VARCHAR(150) NOT NULL,
  `valor` TEXT NULL DEFAULT NULL,
  `data_atualizacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `label` VARCHAR(200) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  `id_funcionalidade_grupo_campo` INT(11) NULL DEFAULT NULL,
  `grupo_campo_nome` VARCHAR(255) NULL DEFAULT NULL,
  `tipo` VARCHAR(100) NULL DEFAULT NULL,
  `option_label` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_funcionalidade_protocolo_dados`),
  INDEX `fk_funcionalidade_protocolo_dados_1_idx` (`id_funcionalidade_campo` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_acos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_acos` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` INT(10) NULL DEFAULT NULL,
  `model` VARCHAR(255) NULL DEFAULT '',
  `foreign_key` INT(10) UNSIGNED NULL DEFAULT NULL,
  `alias` VARCHAR(255) NULL DEFAULT '',
  `lft` INT(10) NULL DEFAULT NULL,
  `rght` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_aros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_aros` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` INT(10) NULL DEFAULT NULL,
  `model` VARCHAR(255) NULL DEFAULT '',
  `foreign_key` INT(10) UNSIGNED NULL DEFAULT NULL,
  `alias` VARCHAR(255) NULL DEFAULT '',
  `lft` INT(10) NULL DEFAULT NULL,
  `rght` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_aros_acos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_aros_acos` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `aro_id` INT(10) UNSIGNED NOT NULL,
  `aco_id` INT(10) UNSIGNED NOT NULL,
  `_create` CHAR(2) NOT NULL DEFAULT '0',
  `_read` CHAR(2) NOT NULL DEFAULT '0',
  `_update` CHAR(2) NOT NULL DEFAULT '0',
  `_delete` CHAR(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_builds`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_builds` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ios_application_id` INT(11) NOT NULL,
  `version` VARCHAR(100) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `ipa_url` VARCHAR(255) NOT NULL,
  `plist_url` VARCHAR(255) NOT NULL,
  `manifest_url` VARCHAR(255) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_companies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_employees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_employees` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `company_id` INT(11) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_ios_applications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_ios_applications` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `company_id` INT(11) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `icon_url` VARCHAR(255) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_uploads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_uploads` (
  `id` CHAR(36) NOT NULL,
  `user_id` CHAR(36) NOT NULL,
  `filename` VARCHAR(255) NOT NULL,
  `filesize` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `filemime` VARCHAR(45) NOT NULL DEFAULT 'text/plain',
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ga_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ga_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) NOT NULL,
  `username` VARCHAR(100) NOT NULL,
  `password` CHAR(40) NOT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `modified` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`geocoding_erro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`geocoding_erro` (
  `id_geocoding_erro` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(200) NULL DEFAULT NULL,
  `dt_erro` DATETIME NULL DEFAULT NULL,
  `id_prestador` INT(11) NULL DEFAULT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_geocoding_erro`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`historico_buscas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`historico_buscas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `latitude` VARCHAR(20) NULL DEFAULT NULL,
  `longitude` VARCHAR(20) NULL DEFAULT NULL,
  `dispositivo` TINYINT(4) NULL DEFAULT NULL,
  `id_operadora` TINYINT(4) NOT NULL,
  `desc_plano` VARCHAR(100) NOT NULL,
  `desc_tp_prest` VARCHAR(100) NULL DEFAULT NULL,
  `desc_espec` VARCHAR(100) NULL DEFAULT NULL,
  `nome_prest` VARCHAR(100) NULL DEFAULT NULL,
  `acessib` VARCHAR(1) NULL DEFAULT NULL,
  `sigla_uf` VARCHAR(2) NULL DEFAULT NULL,
  `codigo_ibge` VARCHAR(50) NULL DEFAULT NULL,
  `bairro` VARCHAR(50) NULL DEFAULT NULL,
  `matr_benef` VARCHAR(30) NULL DEFAULT NULL,
  `data_hora_busca` DATETIME NOT NULL,
  `atend_vq_horas` VARCHAR(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`historico_prestador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`historico_prestador` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_busca` INT(11) NULL DEFAULT NULL,
  `id_operadora` INT(10) NOT NULL,
  `nome_prestador` VARCHAR(100) NOT NULL,
  `latitude` VARCHAR(20) NOT NULL,
  `longitude` VARCHAR(20) NOT NULL,
  `endereco` VARCHAR(100) NULL DEFAULT NULL,
  `numero` VARCHAR(5) NULL DEFAULT NULL,
  `bairro` VARCHAR(100) NOT NULL,
  `id_cidade` VARCHAR(10) NOT NULL,
  `id_estado` VARCHAR(2) NOT NULL,
  `cpf_cnpj` VARCHAR(20) NULL DEFAULT NULL,
  `matricula_benef` VARCHAR(30) NULL DEFAULT NULL,
  `id_acao` TINYINT(4) NULL DEFAULT NULL,
  `datahora_visual` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`horario_prestador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`horario_prestador` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `cpf_cnpj` VARCHAR(20) NOT NULL,
  `id_especialidade` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco` VARCHAR(20) NULL DEFAULT NULL,
  `titulo_horario` VARCHAR(40) NOT NULL,
  `horario_inicial` VARCHAR(5) NOT NULL,
  `horario_final` VARCHAR(5) NOT NULL,
  `inicio_intervalo` VARCHAR(5) NULL DEFAULT NULL,
  `final_intervalo` VARCHAR(5) NULL DEFAULT NULL,
  `tag_dia_da_semana` VARCHAR(40) NOT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_horarioprestador` (`id_operadora` ASC, `cpf_cnpj` ASC, `id_especialidade` ASC, `sequencial_endereco` ASC) VISIBLE)
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`horario_prestador_tmp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`horario_prestador_tmp` (
  `id_operadora` INT(10) NOT NULL,
  `cpf_cnpj` VARCHAR(20) NOT NULL,
  `sequencial_endereco` VARCHAR(20) NULL DEFAULT NULL,
  `titulo_horario` VARCHAR(40) NOT NULL,
  `horario_inicial` VARCHAR(5) NOT NULL,
  `horario_final` VARCHAR(5) NOT NULL,
  `inicio_intervalo` VARCHAR(5) NULL DEFAULT NULL,
  `final_intervalo` VARCHAR(5) NULL DEFAULT NULL,
  `tag_dia_da_semana` VARCHAR(40) NOT NULL,
  `operacao` VARCHAR(1) NOT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL)
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`hub_autenticacao_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`hub_autenticacao_log` (
  `id_hub_autenticacao_log` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `usuario` VARCHAR(255) NULL DEFAULT NULL,
  `senha` VARCHAR(255) NULL DEFAULT NULL,
  `token` VARCHAR(255) NULL DEFAULT NULL,
  `mshash` VARCHAR(60) NULL DEFAULT NULL,
  `response` TEXT NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `tipo_login` VARCHAR(45) NULL DEFAULT NULL,
  `duracao` DECIMAL(9,6) NULL DEFAULT NULL,
  `hub_versao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_hub_autenticacao_log`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`hub_autenticacao_log_v2`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`hub_autenticacao_log_v2` (
  `id_hub_autenticacao_log` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `usuario` VARCHAR(255) NULL DEFAULT NULL,
  `senha` VARCHAR(255) NULL DEFAULT NULL,
  `token` VARCHAR(255) NULL DEFAULT NULL,
  `mshash` VARCHAR(60) NULL DEFAULT NULL,
  `response` LONGTEXT NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `tipo_login` VARCHAR(45) NULL DEFAULT NULL,
  `hub_versao` TINYINT(1) NULL DEFAULT NULL,
  `duracao` DECIMAL(9,6) NULL DEFAULT NULL,
  PRIMARY KEY (`id_hub_autenticacao_log`),
  INDEX `index_operadora_usuario` (`id_operadora` ASC, `usuario` ASC) VISIBLE,
  INDEX `index_operadora` (`id_operadora` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`layout_carteirinha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`layout_carteirinha` (
  `id_layout_carteirinha` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `nickname` VARCHAR(45) NULL DEFAULT NULL,
  `html_frente` LONGTEXT NULL DEFAULT NULL,
  `html_verso` LONGTEXT NULL DEFAULT NULL,
  `imagem_frente` VARCHAR(100) NULL DEFAULT NULL,
  `imagem_verso` VARCHAR(100) NULL DEFAULT NULL,
  `imagem_miniatura` VARCHAR(100) NULL DEFAULT NULL,
  `nome_plano` VARCHAR(60) NULL DEFAULT NULL,
  `nome_operadora` VARCHAR(60) NULL DEFAULT NULL,
  `cor_carteirinhabg` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_layout_carteirinha`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`legendas_acreditacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`legendas_acreditacao` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sigla` VARCHAR(10) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `detalhamento` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_sigla` (`sigla` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`lista_tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`lista_tokens` (
  `id_lista_tokens` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `nome` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `descricao` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `lista_principal` TINYINT(1) NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `cod_referencia` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_lista_tokens`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tipo_protocolo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tipo_protocolo` (
  `id_tipo_protocolo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `identificacao` VARCHAR(255) NOT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Caso \'proprio\' seja \'0 - Não\' o campo é obrigatório.',
  `header` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Header a ser enviado quando solicitar um protocolo de terceiros.',
  `origem` TINYINT(4) NOT NULL COMMENT '0 - Mobile Saúde\\n1 - Terceiros',
  `padrao` TINYINT(4) NULL DEFAULT NULL COMMENT '0 - ANS\\n1 - Mosia\\n2 - Sequencial Simples\\n\\nCampo obrigatóri caso \'origem\' seja \'0 - Mobile saúde\';',
  `proprio` TINYINT(4) NULL DEFAULT NULL COMMENT '0 - Não\\n1 - Sim\\n\\nCaso \'1 - Sim\', ao gerar um protocolo novo desse tipo será obrigatório informar o número de protocolo já gerado.',
  `range_inicio` INT(6) NULL DEFAULT '0',
  `range_fim` INT(6) NULL DEFAULT '999999',
  `id_tipo_protocolo_contingencia` INT(11) NULL DEFAULT NULL COMMENT 'Caso o tipo de protocolo seja de terceiros, deve ser escolhido um tipo de protocolo que seja interno para ser usado caso ocorra um erro na hora de buscar o protocolo na url informada.',
  PRIMARY KEY (`id_tipo_protocolo`),
  INDEX `fk_tipo_protocolo_operadoras` (`id_cliente` ASC) VISIBLE,
  CONSTRAINT `fk_tipo_protocolo_operadoras`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tipo_ocorrencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tipo_ocorrencia` (
  `id_tipo_ocorrencia` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(255) NOT NULL,
  `id_cliente` INT(11) NOT NULL,
  `prazo_vencimento_sla` INT(11) NULL DEFAULT NULL COMMENT 'Horas',
  `form_solicitante` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Opcional\\n\\nHavera futuramente o form do form_atendente que tratara, hj fica tudo na',
  `url_detalhe` VARCHAR(255) NOT NULL,
  `header_detalhe` VARCHAR(255) NULL DEFAULT NULL COMMENT 'A url sempre vai receber via GET um base64 de uma concatenação de client_key + protocolo\\n\\n',
  `id_tipo_protocolo` INT(11) NOT NULL,
  PRIMARY KEY (`id_tipo_ocorrencia`),
  INDEX `fk_tipo_ocorrencia_operadoras` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_tipo_ocorrencia_tipo_protocolo1_idx` (`id_tipo_protocolo` ASC) VISIBLE,
  CONSTRAINT `fk_tipo_ocorrencia_operadoras`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_tipo_ocorrencia_tipo_protocolo1`
    FOREIGN KEY (`id_tipo_protocolo`)
    REFERENCES `thweb_homologacao`.`tipo_protocolo` (`id_tipo_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`status` (
  `id_status` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_ocorrencia` INT(11) NOT NULL,
  `cor` VARCHAR(10) NOT NULL,
  `descricao_interna` VARCHAR(255) NOT NULL,
  `descricao_externa` VARCHAR(255) NOT NULL,
  `exibe_ocorrencia` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Controla se vai exibir ou não a ocorrência, ja que tem status internos não exibidos aos clientes',
  `padrao` TINYINT(4) NOT NULL DEFAULT '0' COMMENT 'Status de criação da ocorrência, somente um por tipo de ocorrência ',
  `encerrado` TINYINT(4) NOT NULL DEFAULT '0' COMMENT 'Indica que aquele ou qualquer outro status é final, e encerra  a ocorrência ',
  `resolvido` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '1 = resolvido\\n0 = nao resolvido\\n \\nControla se indica como resolvido ou não resolvido',
  `exibe_historico_cliente` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 - Não exibe\\n1 - Exibe',
  `ativo` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '0 - Desativado\\n1 - Ativado',
  PRIMARY KEY (`id_status`),
  INDEX `fk_status_tipo_ocorrencia1_idx` (`id_tipo_ocorrencia` ASC) VISIBLE,
  CONSTRAINT `fk_status_tipo_ocorrencia1`
    FOREIGN KEY (`id_tipo_ocorrencia`)
    REFERENCES `thweb_homologacao`.`tipo_ocorrencia` (`id_tipo_ocorrencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`sub_tipo_ocorrencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`sub_tipo_ocorrencia` (
  `id_sub_tipo_ocorrencia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_ocorrencia` INT(11) NOT NULL,
  `descricao` VARCHAR(255) NOT NULL,
  `vencimento_sla` INT(11) NULL DEFAULT NULL COMMENT 'Minutos\\n',
  `form_solicitante` VARCHAR(255) NULL DEFAULT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_sub_tipo_ocorrencia`),
  INDEX `fk_sub_tipo_tipo_ocorrencia1_idx` (`id_tipo_ocorrencia` ASC) VISIBLE,
  CONSTRAINT `fk_sub_tipo_tipo_ocorrencia1`
    FOREIGN KEY (`id_tipo_ocorrencia`)
    REFERENCES `thweb_homologacao`.`tipo_ocorrencia` (`id_tipo_ocorrencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ocorrencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ocorrencia` (
  `id_ocorrencia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `protocolo` VARCHAR(75) NULL DEFAULT NULL COMMENT 'Essa tabela precisa nascer particionada',
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_atualizacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_vencimento_sla` DATETIME NOT NULL,
  `id_tipo_ocorrencia` INT(11) NOT NULL,
  `id_status` INT(11) NOT NULL,
  `id_sub_tipo_ocorrencia` INT(11) NULL DEFAULT NULL,
  `solicitante` VARCHAR(255) NOT NULL,
  `identificador_solicitante` VARCHAR(255) NOT NULL COMMENT 'Localizador, matricula etc',
  `contato_solicitante` VARCHAR(255) NOT NULL,
  `em_atendimento` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 - livre\\n1 - em atendimento',
  `id_atendente` INT(11) NULL DEFAULT NULL,
  `beneficiario` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nome do executante, para quem será executado o procedimento',
  `identificador_beneficiario` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_ocorrencia`),
  INDEX `fk_ocorrencias_operadoras` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_ocorrencias_tipo_ocorrencia_idx` (`id_tipo_ocorrencia` ASC) VISIBLE,
  INDEX `fk_ocorrencias_status1_idx` (`id_status` ASC) VISIBLE,
  INDEX `fk_ocorrencias_sub_tipo1_idx` (`id_sub_tipo_ocorrencia` ASC) VISIBLE,
  CONSTRAINT `fk_ocorrencias_operadoras`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_ocorrencias_status1`
    FOREIGN KEY (`id_status`)
    REFERENCES `thweb_homologacao`.`status` (`id_status`),
  CONSTRAINT `fk_ocorrencias_sub_tipo1`
    FOREIGN KEY (`id_sub_tipo_ocorrencia`)
    REFERENCES `thweb_homologacao`.`sub_tipo_ocorrencia` (`id_sub_tipo_ocorrencia`),
  CONSTRAINT `fk_ocorrencias_tipo_ocorrencia`
    FOREIGN KEY (`id_tipo_ocorrencia`)
    REFERENCES `thweb_homologacao`.`tipo_ocorrencia` (`id_tipo_ocorrencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`log_ocorrencia_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`log_ocorrencia_status` (
  `id_log_ocorrencia_status` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ocorrencia` INT(11) NOT NULL,
  `id_status` INT(11) NOT NULL,
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_log_ocorrencia_status`),
  INDEX `fk_log_ocorrencia_status_ocorrencias1_idx` (`id_ocorrencia` ASC) VISIBLE,
  INDEX `fk_log_ocorrencia_status_status1_idx` (`id_status` ASC) VISIBLE,
  CONSTRAINT `fk_log_ocorrencia_status_ocorrencias1`
    FOREIGN KEY (`id_ocorrencia`)
    REFERENCES `thweb_homologacao`.`ocorrencia` (`id_ocorrencia`),
  CONSTRAINT `fk_log_ocorrencia_status_status1`
    FOREIGN KEY (`id_status`)
    REFERENCES `thweb_homologacao`.`status` (`id_status`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`log_tempo_requisicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`log_tempo_requisicao` (
  `id_log_tempo_requisicao` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `tipo_hub` VARCHAR(100) NOT NULL,
  `metodo` VARCHAR(100) NOT NULL,
  `requisicao` TEXT NOT NULL,
  `response` TEXT NOT NULL,
  `inputs` TEXT NOT NULL,
  `duracao_requisicao_cliente` FLOAT NOT NULL,
  `duracao_requisicao_total` FLOAT NOT NULL,
  `data_hora` DATETIME NOT NULL,
  `matricula_login` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_log_tempo_requisicao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`medicamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`medicamento` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `id_plano` VARCHAR(10) NOT NULL,
  `nome_medicamento` VARCHAR(100) NOT NULL,
  `principio_ativo` VARCHAR(100) NULL DEFAULT NULL,
  `fabricante` VARCHAR(70) NULL DEFAULT NULL,
  `percentual_subsidio` FLOAT NULL DEFAULT NULL,
  `codigo_ean` VARCHAR(45) NULL DEFAULT NULL,
  `descricao` VARCHAR(500) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `indice1` (`id_operadora` ASC, `id_plano` ASC, `nome_medicamento` ASC, `principio_ativo` ASC, `fabricante` ASC) VISIBLE,
  FULLTEXT INDEX `indice2` (`id_plano`, `nome_medicamento`, `principio_ativo`, `fabricante`, `descricao`) VISIBLE)
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`mensagens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`mensagens` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_legado` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `data_inicio` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_final` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `assunto` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `mensagem` LONGTEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `id_beneficiario` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `imagem` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `file` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `departamento` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `introducao` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_backend`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_backend` (
  `id_ms_backend` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id_ms_backend`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_backend_funcionalidade_grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_backend_funcionalidade_grupo` (
  `id_ms_backend_func_grupo` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NULL DEFAULT NULL,
  `id_backend` INT(11) NOT NULL,
  `especifica` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_ms_backend_func_grupo`),
  INDEX `id_backend` (`id_backend` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_backend_funcionalidade_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_backend_funcionalidade_categoria` (
  `id_ms_backend_func_cat` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_ms_backend_func_cat`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_backend_funcionalidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_backend_funcionalidade` (
  `id_ms_backend_func` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ms_backend_func_grupo` INT(11) NOT NULL,
  `id_ms_backend_func_cat` INT(11) NOT NULL,
  PRIMARY KEY (`id_ms_backend_func`),
  INDEX `id_ms_backend_func_grupo` (`id_ms_backend_func_grupo` ASC) VISIBLE,
  INDEX `id_ms_backend_func_cat` (`id_ms_backend_func_cat` ASC) VISIBLE,
  CONSTRAINT `ms_backend_funcionalidade_ibfk_1`
    FOREIGN KEY (`id_ms_backend_func_grupo`)
    REFERENCES `thweb_homologacao`.`ms_backend_funcionalidade_grupo` (`id_ms_backend_func_grupo`),
  CONSTRAINT `ms_backend_funcionalidade_ibfk_2`
    FOREIGN KEY (`id_ms_backend_func_cat`)
    REFERENCES `thweb_homologacao`.`ms_backend_funcionalidade_categoria` (`id_ms_backend_func_cat`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_usuario` (
  `id_ms_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(75) NOT NULL,
  `telefone1` VARCHAR(15) NULL DEFAULT NULL,
  `telefone2` VARCHAR(15) NULL DEFAULT NULL,
  `nome` VARCHAR(115) NULL DEFAULT NULL,
  `ultimo_login` DATETIME NULL DEFAULT NULL,
  `ultima_tentativa_login` DATETIME NULL DEFAULT NULL,
  `ultima_alteracao_senha` DATETIME NULL DEFAULT NULL,
  `senha` VARCHAR(50) NULL DEFAULT NULL,
  `senha_anterior` VARCHAR(50) NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT NULL,
  `num_acessos` INT(11) NULL DEFAULT NULL,
  `hash_recuperacao_senha` VARCHAR(50) NULL DEFAULT NULL,
  `avatar` VARCHAR(125) NULL DEFAULT NULL,
  `usuario_master` INT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id_ms_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_backend_funcionalidade_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_backend_funcionalidade_usuario` (
  `id_ms_backend_func_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ms_usuario` INT(11) NOT NULL,
  `id_ms_backend_func` INT(11) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  PRIMARY KEY (`id_ms_backend_func_usuario`),
  INDEX `id_ms_usuario` (`id_ms_usuario` ASC) VISIBLE,
  INDEX `id_ms_backend_func` (`id_ms_backend_func` ASC) VISIBLE,
  CONSTRAINT `ms_backend_funcionalidade_usuario_ibfk_1`
    FOREIGN KEY (`id_ms_usuario`)
    REFERENCES `thweb_homologacao`.`ms_usuario` (`id_ms_usuario`),
  CONSTRAINT `ms_backend_funcionalidade_usuario_ibfk_2`
    FOREIGN KEY (`id_ms_backend_func`)
    REFERENCES `thweb_homologacao`.`ms_backend_funcionalidade` (`id_ms_backend_func`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_cargo` (
  `id_ms_cargo` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(75) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `acesso_total` TINYINT(4) NULL DEFAULT NULL,
  `id_backend` INT(11) NULL DEFAULT NULL,
  `key` VARCHAR(75) NULL DEFAULT NULL,
  `permissoes_especificas` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ms_cargo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ms_usuario_cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ms_usuario_cargo` (
  `id_ms_usuario_cargo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ms_usuario` INT(11) NOT NULL,
  `id_ms_cargo` INT(11) NOT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_msg_centro_custos` INT(11) NULL DEFAULT NULL,
  `quota` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id_ms_usuario_cargo`),
  INDEX `fk_ms_usuario_cargo_ms_usuario_idx` (`id_ms_usuario` ASC) VISIBLE,
  INDEX `fk_ms_usuario_cargo_ms_cargo1_idx` (`id_ms_cargo` ASC) VISIBLE,
  INDEX `fk_ms_usuario_cargo_1_idx` (`id_msg_centro_custos` ASC) VISIBLE,
  CONSTRAINT `fk_ms_usuario_cargo_ms_cargo1`
    FOREIGN KEY (`id_ms_cargo`)
    REFERENCES `thweb_homologacao`.`ms_cargo` (`id_ms_cargo`),
  CONSTRAINT `fk_ms_usuario_cargo_ms_usuario`
    FOREIGN KEY (`id_ms_usuario`)
    REFERENCES `thweb_homologacao`.`ms_usuario` (`id_ms_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_campanha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_campanha` (
  `id_campanha` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_fatura` INT(11) NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `status` INT(11) NULL DEFAULT NULL COMMENT '0 - Rascunho \\n1 - Configurado / Agendado\\n2 - Em processamento \\n3 - Enviada\\n4 - Recusado\\n11 - Aguardando aturorização de envio\\n\\n\\n\\n11 - Recusado\\n',
  `nome_campanha` VARCHAR(145) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `request_push` LONGTEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `request_sms` LONGTEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `request_email` LONGTEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `titulo_enviado` VARCHAR(90) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `texto_enviado` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `texto_enviado_alternativo` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'Texto que será enviado caso a campanha utilize MACROS na mensagem, e, o token selecionado não tenha preenchido o campo que será utilizado\\n\\nEx.\\n\\nOriginal: \\n\\nFeliz aniversario {nome}\\n\\n=== ====\\n\\nAlternativo: \\n\\nFeliz aniversário!!!',
  `data_hora_criacao` DATETIME NULL DEFAULT NULL,
  `data_hora_agendada` DATETIME NULL DEFAULT NULL,
  `data_hora_inicio_envio` DATETIME NULL DEFAULT NULL,
  `data_hora_fim_envio` DATETIME NULL DEFAULT NULL,
  `id_registro` VARCHAR(245) CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'Valor enviado para o id_funcionalidade (pode ser um link ou id)',
  `tipo_campanha` INT(11) NULL DEFAULT NULL COMMENT '0 - Campanha Criada pela interface\\n1 - Campanha Importada (CSV de campanha)\\n2 - Campanha via integração / painel (o divulgar da notícia) \\n',
  `qtd_push` INT(11) NULL DEFAULT NULL,
  `qtd_sms` INT(11) NULL DEFAULT NULL,
  `qtd_email` INT(11) NULL DEFAULT NULL,
  `valor_sms` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `valor_push` DECIMAL(9,4) NULL DEFAULT NULL,
  `valor_email` DECIMAL(9,4) NULL DEFAULT NULL,
  `valor_total` DECIMAL(9,4) NULL DEFAULT NULL,
  `envia_push` TINYINT(1) NULL DEFAULT NULL,
  `envia_sms` TINYINT(1) NULL DEFAULT NULL,
  `envia_email` TINYINT(1) NULL DEFAULT NULL,
  `qtd_sms_por_mensagem` INT(11) NULL DEFAULT NULL COMMENT 'Para registrar se uma mensagem de SMS precisou de ser quebrada em 2 SMSs ou mais',
  `broadcast` TINYINT(1) NULL DEFAULT NULL COMMENT 'Registra se a campanha que esta sendo enviada será tarifada como tarifa única, ou individual \\n',
  `tipo_callback` VARCHAR(75) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `id_cliente_tarifa` INT(11) NULL DEFAULT NULL,
  `utiliza_macro` INT(1) UNSIGNED NULL DEFAULT '0',
  `sms_enviar_para_contatos_com_app` INT(1) NULL DEFAULT '1',
  `sms_enviar_para_contatos_sem_app` INT(1) NULL DEFAULT '1',
  `id_upload_arquivo` INT(11) NULL DEFAULT NULL,
  `segmentacao_str` TEXT NULL DEFAULT NULL,
  `segmentacao_str_desc` TEXT NULL DEFAULT NULL,
  `id_lista_tokens` INT(11) NULL DEFAULT NULL,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  `motivo_cancelamento` INT(11) NULL DEFAULT NULL,
  `push_visivel` TINYINT(1) NULL DEFAULT '1',
  `comando_remoto` TINYINT(1) NOT NULL DEFAULT '0',
  `id_referencia_remetente` VARCHAR(25) NULL DEFAULT NULL,
  PRIMARY KEY (`id_campanha`),
  INDEX `fk_msg_campanha_operadoras1_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `index3` (`id_operadora` ASC, `tipo_campanha` ASC, `status` ASC) VISIBLE,
  INDEX `index4` (`id_operadora` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_centro_custos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_centro_custos` (
  `id_msg_centro_custos` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `identificacao` VARCHAR(245) NULL DEFAULT NULL,
  `quota` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`id_msg_centro_custos`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_cliente_aplicacao_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_cliente_aplicacao_config` (
  `id_cliente_aplicacao_config` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `ws_get_funcionalidades` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `ws_get_registros` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `ws_header_login` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `ws_header_senha` VARCHAR(75) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `ws_callback_resposta` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `ws_callback_resposta_login` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `ws_callback_resposta_senha` VARCHAR(245) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  PRIMARY KEY (`id_cliente_aplicacao_config`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_cliente_segmento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_cliente_segmento` (
  `id_msg_cliente_segmento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `descricao` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `chave` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `tipo_valor` INT(11) NULL DEFAULT '0' COMMENT 'Define o tipo de valor armazenado\\n0 = lista\\n1 = numérico',
  PRIMARY KEY (`id_msg_cliente_segmento`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_cliente_tarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_cliente_tarifa` (
  `id_cliente_tarifa` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `push_broad` DECIMAL(9,4) NULL DEFAULT '0.0000' COMMENT 'Valor fechado para envio de Msg inapp POR ENVIO independente da quantidade\\n\\n\\nEx.: R$ 30,00 por envio',
  `push_single` DECIMAL(9,4) NULL DEFAULT '0.0000' COMMENT 'Valor fechado para Push unitário',
  `sms` DECIMAL(9,4) NULL DEFAULT NULL,
  `email` DECIMAL(9,4) NULL DEFAULT '0.0000',
  `vigencia_inicio` DATE NULL DEFAULT NULL,
  `vigencia_fim` DATE NULL DEFAULT NULL,
  `plano_push` TINYINT(1) NOT NULL DEFAULT '1',
  `plano_sms` TINYINT(1) NOT NULL DEFAULT '1',
  `situacao` TINYINT(4) NULL DEFAULT '0' COMMENT 'Situação da tarifação: \\n0= Inativo \\n1= Ativa',
  `data_atualizacao` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dia_vencimento_mes` INT(2) NOT NULL DEFAULT '20',
  PRIMARY KEY (`id_cliente_tarifa`),
  INDEX `fk_msg_cliente_tarifa_operadoras_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `indice_idx1` (`id_operadora` ASC, `situacao` ASC, `vigencia_inicio` ASC, `vigencia_fim` ASC) VISIBLE,
  CONSTRAINT `fk_msg_cliente_tarifa_operadoras`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_envio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_envio` (
  `id_msg_envio` INT(11) NOT NULL AUTO_INCREMENT COMMENT '- apagar o campo \"origem\"',
  `id_campanha` INT(11) NULL DEFAULT NULL,
  `id_operadoras` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `mshash` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  `id_registro` VARCHAR(245) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `id_fatura` INT(11) NULL DEFAULT NULL,
  `tipo_notificacao` TINYINT(4) NULL DEFAULT NULL COMMENT '\'Tipo da notificação enviada : 0=push, 1=sms, 2=email\'',
  `tipo_envio` TINYINT(4) NULL DEFAULT NULL COMMENT 'Indica o tipo de mensagem enviada:  0=Notificação Direta | 1=Broadcast | 2 = campanha\\nPara cobrança',
  `plataforma` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `token` VARCHAR(400) CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'Para mensagens in-app, é gerada pelo aplicativo',
  `localizador` VARCHAR(254) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `telefone` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `email` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `texto` VARCHAR(160) CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'Texto da mensagem',
  `data_hora_agendada` DATETIME NULL DEFAULT NULL COMMENT 'Data e hora em que a mensagem deve ser enviada (agendamento)',
  `data_hora_enviada` DATETIME NULL DEFAULT NULL COMMENT 'Data e Hora em que foi enviada a mensagem',
  `data_hora_validade` DATETIME NULL DEFAULT NULL,
  `autorizado` TINYINT(4) NULL DEFAULT '0' COMMENT 'Em um sistema prépago eu posso não liberar o envio,  e ao colocar mais crédito o envio fica liberado e no próximo cron ele é enviado \\n',
  `situacao` TINYINT(4) NULL DEFAULT '0' COMMENT '0=Pendente  \\n1=Enviado \\n2=Cancelado \\n3=lido\\n\\n11=Em processamento\\n4=erro',
  `copia` TINYINT(4) NULL DEFAULT '0' COMMENT 'Registra que é um device do mesmo usuároi, cópias não devem ser cobradas',
  `parse_rest_key` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_master_key` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_app_key` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_client_key` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `parse_android_action` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `email_assunto` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `email_remetente_nome` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `msg_request` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'o JSON enviado na requisição para o Zenvia ou para o parse',
  `msg_request_status` INT(11) NULL DEFAULT NULL COMMENT '0 - Não entregue\\n1 - Entregue\\n2 - Lido (e-mail, mensagem in-app) \\n ',
  `msg_response` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `msg_response_date` DATETIME NULL DEFAULT NULL,
  `valor_tarifado` DECIMAL(9,4) NULL DEFAULT NULL COMMENT 'Esse é o valor tarfiado vingente ',
  `tipo_callback` VARCHAR(75) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `sms_agrupador` INT(11) NULL DEFAULT NULL,
  `id_cliente_tarifa` INT(11) NULL DEFAULT NULL,
  `tipo_click` INT(2) NULL DEFAULT NULL COMMENT 'Define se a mensagem foi clicada no inbox ou na notificacao\\n0 = Clicado Notificacao\\n1 = Clicado Inbox\\nNULL = Nao clicado',
  `excluido` INT(1) NULL DEFAULT '0',
  `utiliza_macro` INT(1) NULL DEFAULT '0',
  `id_msg_envio_original` INT(11) NULL DEFAULT NULL,
  `data_recebimento` DATETIME NULL DEFAULT NULL,
  `importado` TINYINT(4) NULL DEFAULT NULL,
  `id_msg_envio_copia` INT(11) NULL DEFAULT NULL,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  `push_visivel` TINYINT(1) NULL DEFAULT '1',
  `comando_remoto` TINYINT(1) NOT NULL DEFAULT '0',
  `qt_tentativa_envio` INT(11) NULL DEFAULT '0',
  `id_referencia_remetente` VARCHAR(25) NULL DEFAULT NULL,
  `fl_enviada_fila` TINYINT(4) NOT NULL DEFAULT '0',
  `dt_hora_recebida_fila` DATETIME NULL DEFAULT NULL,
  `sns_endpoint` VARCHAR(200) NULL DEFAULT NULL,
  `sns_id_mensagem` VARCHAR(36) NULL DEFAULT NULL,
  `data_criacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_msg_envio`),
  UNIQUE INDEX `sns_id_mensagem_UNIQUE` (`sns_id_mensagem` ASC) VISIBLE,
  INDEX `msg_envio_por_campanha` (`id_campanha` ASC, `tipo_notificacao` ASC, `id_operadoras` ASC, `id_aplicacao` ASC) VISIBLE,
  INDEX `update_msg_envio` (`id_msg_envio_original` ASC, `tipo_notificacao` ASC, `situacao` ASC) VISIBLE,
  INDEX `index4` (`id_operadoras` ASC, `id_aplicacao` ASC, `situacao` ASC, `excluido` ASC, `tipo_notificacao` ASC, `localizador` ASC, `token` ASC, `tipo_envio` ASC) VISIBLE,
  INDEX `operadora_aplicacao` (`id_operadoras` ASC, `id_aplicacao` ASC) VISIBLE,
  INDEX `msg_envio_original_index` (`id_msg_envio_original` ASC) VISIBLE,
  INDEX `msg_envio_individual` (`situacao` ASC, `id_campanha` ASC) VISIBLE,
  INDEX `index_relatorio_painel` (`id_operadoras` ASC, `id_aplicacao` ASC, `data_hora_agendada` ASC) COMMENT '\'Indice usado no relatorio do painel web\'' VISIBLE,
  INDEX `index_msg_disparador` (`id_operadoras` ASC, `situacao` ASC, `data_hora_agendada` DESC, `data_hora_validade` DESC, `id_campanha` ASC, `sns_endpoint` DESC) VISIBLE,
  INDEX `idx_financeiro_fatura` (`id_operadoras` ASC, `id_aplicacao` ASC, `id_fatura` ASC, `data_hora_agendada` ASC, `data_hora_enviada` ASC, `situacao` ASC, `tipo_notificacao` ASC, `tipo_envio` ASC) VISIBLE,
  CONSTRAINT `msg_envio_original_fk`
    FOREIGN KEY (`id_msg_envio_original`)
    REFERENCES `thweb_homologacao`.`msg_envio` (`id_msg_envio`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_envio_resposta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_envio_resposta` (
  `id_msg_envio_resposta` INT(11) NOT NULL AUTO_INCREMENT,
  `id_msg_envio` INT(11) NULL DEFAULT NULL,
  `token` VARCHAR(225) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `localizador` VARCHAR(125) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `resposta` TEXT CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `tipo_callback` VARCHAR(75) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `id_registro` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `status` INT(2) NULL DEFAULT NULL COMMENT 'Registra o status da resposta\\n0 = nova\\n1 = processada\\n2 = erro\\n',
  `valor_tarifado` DECIMAL(9,4) NULL DEFAULT NULL,
  `id_cliente_tarifa` INT(11) NULL DEFAULT NULL,
  `id_fatura` INT(11) NULL DEFAULT NULL,
  `callback_response` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `callback_status` TINYINT(4) NULL DEFAULT '0',
  PRIMARY KEY (`id_msg_envio_resposta`),
  INDEX `fkmsgenvio_idx` (`id_msg_envio` ASC) VISIBLE,
  CONSTRAINT `fkmsgenvio`
    FOREIGN KEY (`id_msg_envio`)
    REFERENCES `thweb_homologacao`.`msg_envio` (`id_msg_envio`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_envio_situacao_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_envio_situacao_historico` (
  `id_msg_envio_situacao_historico` INT(11) NOT NULL AUTO_INCREMENT,
  `id_msg_envio` INT(11) NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `situacao` INT(2) NULL DEFAULT NULL,
  `excluido` INT(2) NULL DEFAULT '0',
  `msg` TEXT NULL DEFAULT NULL,
  `response` TEXT NULL DEFAULT NULL,
  `id_msg_envio_copia` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_msg_envio_situacao_historico`),
  INDEX `msg_detalhes` (`id_msg_envio` ASC, `id_msg_envio_copia` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Armazena um historico de alterações de situacao de uma mensagem';


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_mensagem_pre_definida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_mensagem_pre_definida` (
  `id_mensagem_pre_definida` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `descricao` VARCHAR(255) NOT NULL DEFAULT '',
  `mensagem` TEXT NOT NULL,
  PRIMARY KEY (`id_mensagem_pre_definida`),
  INDEX `idx_mensagem_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `idx_mensagem_pre_definida_aplicacao_fk_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `idx_mensagem_pre_definida_aplicacao_fk`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`),
  CONSTRAINT `idx_mensagem_pre_definida_operadora_fk`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`msg_tipo_callback`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`msg_tipo_callback` (
  `id_msg_tipo_callback` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo_callback` VARCHAR(75) NULL DEFAULT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_funcionalidade` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_msg_tipo_callback`),
  INDEX `fk_id_operadora_idx` (`id_operadora` ASC) VISIBLE,
  CONSTRAINT `fk_id_operadora`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`notificacao_inconsistencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`notificacao_inconsistencia` (
  `id_notificacao_inconsistencia` INT(9) NOT NULL AUTO_INCREMENT,
  `is_web` INT(1) NULL DEFAULT NULL,
  `is_endereco` INT(1) NULL DEFAULT NULL,
  `is_telefone` INT(1) NULL DEFAULT NULL,
  `is_email` INT(1) NULL DEFAULT NULL,
  `is_site` INT(1) NULL DEFAULT NULL,
  `is_mapa` INT(1) NULL DEFAULT NULL,
  `id_operadora` INT(9) NULL DEFAULT NULL,
  `id_prestador` INT(9) NULL DEFAULT NULL,
  `observacao` VARCHAR(455) NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `sended` INT(1) NULL DEFAULT NULL,
  `id_especialidade` VARCHAR(10) NULL DEFAULT NULL,
  `ds_especialidade` VARCHAR(100) NULL DEFAULT NULL,
  `id_plano` VARCHAR(10) NULL DEFAULT NULL,
  `ds_plano` VARCHAR(100) NULL DEFAULT NULL,
  `id_tipoPrestador` VARCHAR(10) NULL DEFAULT NULL,
  `ds_tipo_prestador` VARCHAR(100) NULL DEFAULT NULL,
  `codigo_legado` VARCHAR(45) NULL DEFAULT NULL,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `sexo` VARCHAR(1) NULL DEFAULT NULL,
  `endereco` VARCHAR(100) NULL DEFAULT NULL,
  `numero` VARCHAR(5) NULL DEFAULT NULL,
  `complemento` VARCHAR(50) NULL DEFAULT NULL,
  `bairro` VARCHAR(50) NULL DEFAULT NULL,
  `id_cidade` VARCHAR(10) NULL DEFAULT NULL,
  `id_estado` VARCHAR(10) NULL DEFAULT NULL,
  `cep` VARCHAR(20) NULL DEFAULT NULL,
  `telefone_primario` VARCHAR(20) NULL DEFAULT NULL,
  `telefone_secundario` VARCHAR(20) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `site_url` VARCHAR(50) NULL DEFAULT NULL,
  `nome_logomarca` VARCHAR(10) NULL DEFAULT NULL,
  `latitude` VARCHAR(20) NULL DEFAULT NULL,
  `longitude` VARCHAR(20) NULL DEFAULT NULL,
  `cpf_cnpj` VARCHAR(20) NULL DEFAULT NULL,
  `prioridade` VARCHAR(2) NULL DEFAULT NULL,
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT NULL,
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT NULL,
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT NULL,
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT NULL,
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT NULL,
  `facebook` VARCHAR(250) NULL DEFAULT NULL,
  `twitter` VARCHAR(250) NULL DEFAULT NULL,
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(50) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(20) NULL DEFAULT NULL,
  `login` VARCHAR(250) NULL DEFAULT NULL,
  `nome_usuario` VARCHAR(250) NULL DEFAULT NULL,
  `telefone_usuario` VARCHAR(250) NULL DEFAULT NULL,
  `email_usuario` VARCHAR(250) NULL DEFAULT NULL,
  `matricula_usuario` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id_notificacao_inconsistencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`notificacao_inconsistencia_old`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`notificacao_inconsistencia_old` (
  `id_notificacao_inconsistencia` INT(9) NOT NULL AUTO_INCREMENT,
  `is_web` INT(1) NULL DEFAULT NULL,
  `is_endereco` INT(1) NULL DEFAULT NULL,
  `is_telefone` INT(1) NULL DEFAULT NULL,
  `is_email` INT(1) NULL DEFAULT NULL,
  `is_site` INT(1) NULL DEFAULT NULL,
  `is_mapa` INT(1) NULL DEFAULT NULL,
  `id_operadora` INT(9) NULL DEFAULT NULL,
  `id_prestador` INT(9) NULL DEFAULT NULL,
  `observacao` VARCHAR(455) NULL DEFAULT NULL,
  `data` DATETIME NULL DEFAULT NULL,
  `sended` INT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_notificacao_inconsistencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ocorrencia_complementado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ocorrencia_complementado` (
  `id_ocorrencia_complementado` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `id_ocorrencia` INT(11) NOT NULL,
  `plataforma` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Mobile\\nWeb\\nAPI\\nPainel (api interna)',
  `sistema_operacional` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Windows\\nLinux\\nAndroid\\niOS',
  `browser` VARCHAR(255) NULL DEFAULT NULL,
  `versao_sistema_operacional` VARCHAR(255) NULL DEFAULT NULL,
  `versao_browser` VARCHAR(255) NULL DEFAULT NULL,
  `ip` VARCHAR(255) NULL DEFAULT NULL,
  `natureza_atendimento` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Auto atendimento\\nAtendimento',
  `canal_atendimento` VARCHAR(255) NULL DEFAULT NULL COMMENT 'App\\nTotem\\nChat\\nTelefone\\n',
  PRIMARY KEY (`id_ocorrencia_complementado`),
  INDEX `fk_ocorrencia_complementado_operadoras` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_ocorrencia_complementado_ocorrencias1_idx` (`id_ocorrencia` ASC) VISIBLE,
  CONSTRAINT `fk_ocorrencia_complementado_ocorrencias1`
    FOREIGN KEY (`id_ocorrencia`)
    REFERENCES `thweb_homologacao`.`ocorrencia` (`id_ocorrencia`),
  CONSTRAINT `fk_ocorrencia_complementado_operadoras`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`ocorrencia_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`ocorrencia_historico` (
  `id_ocorrencia_historico` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ocorrencia` INT(11) NOT NULL,
  `tipo_ocorrencia_historico` VARCHAR(255) NOT NULL COMMENT 'Inserção/Edição de um registro de um nó\\nAlteração de status\\nAlteração na atribuição da ocorrência\\nAtualização de um item',
  `descricao` TEXT NOT NULL,
  `id_relacional` VARCHAR(255) NOT NULL,
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_ocorrencia_historico`),
  INDEX `fk_ocorrencia_historico_ocorrencias1_idx` (`id_ocorrencia` ASC) VISIBLE,
  CONSTRAINT `fk_ocorrencia_historico_ocorrencias1`
    FOREIGN KEY (`id_ocorrencia`)
    REFERENCES `thweb_homologacao`.`ocorrencia` (`id_ocorrencia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`operadora_geocoding`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`operadora_geocoding` (
  `id_operadora` INT(11) NOT NULL,
  `dt_geocoding` DATETIME NOT NULL,
  `quantidade` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_operadora`, `dt_geocoding`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`operadoras_produtos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`operadoras_produtos` (
  `id_operadora_produto` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) UNSIGNED NOT NULL,
  `id_produto` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_operadora_produto`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`operadoras_token_apple`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`operadoras_token_apple` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `iss` VARCHAR(45) NOT NULL,
  `kid` VARCHAR(45) NOT NULL,
  `private_key` VARCHAR(250) NOT NULL,
  `topic` VARCHAR(45) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_operadora_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `fk_aplicacao_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `fk_aplicacao`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`),
  CONSTRAINT `fk_operadora`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`operadoras_usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`operadoras_usuarios` (
  `id_usuario` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) UNSIGNED NOT NULL,
  `id_estado` INT(10) UNSIGNED NOT NULL,
  `id_cidade` INT(10) UNSIGNED NOT NULL,
  `usuario` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `sexo` VARCHAR(1) NOT NULL,
  `cpf` VARCHAR(14) NOT NULL,
  `rg` VARCHAR(255) NOT NULL,
  `logradouro` VARCHAR(255) NOT NULL,
  `numero` VARCHAR(255) NOT NULL,
  `complemento` VARCHAR(255) NOT NULL,
  `bairro` VARCHAR(255) NOT NULL,
  `cep` VARCHAR(10) NOT NULL,
  `telefone1` VARCHAR(255) NOT NULL,
  `telefone2` VARCHAR(255) NOT NULL,
  `celular` VARCHAR(255) NOT NULL,
  `medico` VARCHAR(1) NOT NULL,
  `auditor` VARCHAR(1) NOT NULL,
  `administrador` VARCHAR(1) NOT NULL,
  `crm` VARCHAR(40) NOT NULL,
  `situacao` TINYINT(1) NULL DEFAULT '0',
  `ultimo_acesso` DATETIME NULL DEFAULT NULL,
  `ip` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`operadoras_usuarios_acessos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`operadoras_usuarios_acessos` (
  `id_usuario_acesso` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_usuario` INT(10) UNSIGNED NOT NULL,
  `id_acesso` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_usuario_acesso`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`planos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`planos` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `codigo_legado` VARCHAR(30) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `eletivo` VARCHAR(1) NOT NULL DEFAULT ' ',
  `emergencia` VARCHAR(1) NOT NULL DEFAULT ' ',
  `registro_plano_ANS` VARCHAR(30) NOT NULL DEFAULT ' ',
  `classificacao_fins_comercializacao` VARCHAR(50) NOT NULL DEFAULT ' ',
  `situacao_plano_comercializacao` VARCHAR(50) NOT NULL DEFAULT ' ',
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial` INT(3) NULL DEFAULT '0',
  `rede` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Campo para informar de qual rede esse plano faz parte. Usado inicialmente para carga dos planos da Unimed (RB06, RB07 e etc)',
  `abrangencia` INT(2) NULL DEFAULT NULL COMMENT '1= Nacional\\r2 = Regional A – Grupo de Estados\\r3 = Estadual 4 = Regional B – Grupo de Municípios 5 = Municipal',
  `subplano` VARCHAR(1) NOT NULL DEFAULT 'N',
  `exibir` VARCHAR(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `indice1` (`id_operadora` ASC, `codigo_legado` ASC) VISIBLE,
  INDEX `indice2` (`id_operadora` ASC, `codigo_legado` ASC, `exibir` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`planos_preferencias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`planos_preferencias` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_plano` VARCHAR(60) NULL DEFAULT NULL,
  `livreto_capa` VARCHAR(150) NULL DEFAULT NULL,
  `livreto_introducao` VARCHAR(150) NULL DEFAULT NULL,
  `livreto_contra_capa` VARCHAR(150) NULL DEFAULT NULL,
  `livreto_rodape` VARCHAR(180) NULL DEFAULT NULL,
  `livreto_validade` INT(11) NULL DEFAULT NULL,
  `livreto_arquivo` VARCHAR(150) NULL DEFAULT NULL,
  `cartao_imagem` VARCHAR(150) NULL DEFAULT NULL,
  `status_livreto` TINYINT(1) NULL DEFAULT '1' COMMENT '0=Excessão;1 = configurado\\\\\\\\n2 = baixado pro servidor mobile\\\\\\\\n3 = enviado para o FTP',
  `livreto_url` VARCHAR(200) NULL DEFAULT NULL,
  `posicao_x_validade` INT(11) NULL DEFAULT '0',
  `posicao_y_validade` INT(11) NULL DEFAULT '0',
  `background_cidade` VARCHAR(10) NULL DEFAULT '323232',
  `font_cidade` VARCHAR(10) NULL DEFAULT 'FFFFFF',
  `background_esp` VARCHAR(10) NULL DEFAULT '969696',
  `font_esp` VARCHAR(10) NULL DEFAULT 'FFFFFF',
  `pagina_inicial` INT(11) NULL DEFAULT '1',
  `margin_vertical` INT(11) NULL DEFAULT '8',
  `margin_horizontal` INT(11) NULL DEFAULT '15',
  `background_prestador` VARCHAR(10) NULL DEFAULT '969696',
  `font_prestador` VARCHAR(10) NULL DEFAULT 'FFFFFF',
  PRIMARY KEY (`id`),
  INDEX `indice1` (`id_operadora` ASC, `id_plano` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COMMENT = '\\n';


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`planos_rede`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`planos_rede` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `codigo_legado` VARCHAR(30) NOT NULL,
  `codigo_plano` VARCHAR(30) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `eletivo` VARCHAR(1) NULL DEFAULT NULL,
  `emergencia` VARCHAR(1) NULL DEFAULT NULL,
  `registro_plano_ANS` VARCHAR(30) NULL DEFAULT NULL,
  `classificacao_fins_comercializacao` VARCHAR(50) NULL DEFAULT NULL,
  `situacao_plano_comercializacao` VARCHAR(50) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial` INT(3) NULL DEFAULT '0',
  `abrangencia` INT(2) NULL DEFAULT NULL COMMENT '1= Nacional\\r2 = Regional A – Grupo de Estados\\r3 = Estadual 4 = Regional B – Grupo de Municípios 5 = Municipal',
  PRIMARY KEY (`id`),
  INDEX `indice1` (`id_operadora` ASC, `codigo_legado` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(1000) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC, `key_maps` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_op_102`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_op_102` (
  `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  `hash_endereco_bkp` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_op_119`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_op_119` (
  `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  `hash_endereco_bkp` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_op_19`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_op_19` (
  `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  `hash_endereco_bkp` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_op_39`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_op_39` (
  `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  `hash_endereco_bkp` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_op_50`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_op_50` (
  `id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  `hash_endereco_bkp` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_part`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_part` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `versao` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `id_operadora`, `versao`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC, `key_maps` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1 PARTITION BY HASH(versao) PARTITIONS 4 ;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_resumido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_resumido` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NULL DEFAULT NULL,
  `nm_plano` VARCHAR(100) NULL DEFAULT NULL,
  `bairro` VARCHAR(50) NULL DEFAULT NULL,
  `id_cidade` VARCHAR(10) NULL DEFAULT NULL,
  `nm_cidade` VARCHAR(100) NULL DEFAULT NULL,
  `id_estado` VARCHAR(10) NULL DEFAULT NULL,
  `nm_estado` VARCHAR(100) NULL DEFAULT NULL,
  `id_tipo_prestador` VARCHAR(10) NULL DEFAULT NULL,
  `nm_tipo_prestador` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_temp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_temp` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `versao` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `id_operadora`, `versao`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC, `key_maps` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_teste`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_teste` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` VARCHAR(6) NULL DEFAULT NULL,
  `id_plano` VARCHAR(30) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(100) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `usuario_acesso` VARCHAR(100) NOT NULL DEFAULT ' ',
  `senha_acesso` VARCHAR(10) NOT NULL DEFAULT ' ',
  `ip_ultimo_acesso` VARCHAR(15) NOT NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `status_maps` VARCHAR(40) NOT NULL DEFAULT ' ',
  `key_maps` VARCHAR(20) NOT NULL DEFAULT ' ',
  `distancia` VARCHAR(5) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL,
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `status_georef` INT(1) NOT NULL DEFAULT '3' COMMENT '0- erro, 1- pendente, 2- sucesso, 3 - importado',
  `razao_social` VARCHAR(250) NULL DEFAULT '',
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT '',
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT '',
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT '',
  `facebook` VARCHAR(250) NULL DEFAULT '',
  `twitter` VARCHAR(250) NULL DEFAULT '',
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(70) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(60) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  `cpf_cnpj_subst` VARCHAR(20) NULL DEFAULT NULL,
  `id_especialidade_subst` VARCHAR(10) NULL DEFAULT NULL,
  `id_tipoprestador_subst` VARCHAR(10) NULL DEFAULT NULL,
  `sequencial_endereco_subst` VARCHAR(20) NULL DEFAULT NULL,
  `tipo_estabelecimento` VARCHAR(150) NULL DEFAULT NULL,
  `data_inicio_atendimento` DATETIME NULL DEFAULT NULL,
  `mensagem_alerta` VARCHAR(500) NULL DEFAULT NULL,
  `cod_sub_especialidade` VARCHAR(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxprestadores` (`id_operadora` ASC, `id_plano` ASC, `id_tipoPrestador` ASC) VISIBLE,
  INDEX `idxprestadores2` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores3` (`id_operadora` ASC, `codigo_legado` ASC, `id_especialidade` ASC) VISIBLE,
  INDEX `idxprestadores4` (`id_operadora` ASC, `nome` ASC, `endereco` ASC, `numero` ASC, `bairro` ASC, `id_cidade` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores5` (`id_operadora` ASC, `latitude` ASC, `longitude` ASC) VISIBLE,
  INDEX `idxprestadores6` (`id_operadora` ASC, `key_maps` ASC) VISIBLE,
  INDEX `idxprestadores7` (`id_operadora` ASC, `id_cidade` ASC, `bairro` ASC) VISIBLE,
  INDEX `idxprestadores8` (`id_operadora` ASC, `id_estado` ASC, `id_cidade` ASC) VISIBLE,
  INDEX `idxprestadores10` (`id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `georref` (`id_operadora` ASC, `status_georef` ASC, `id_estado` ASC) VISIBLE,
  INDEX `idxprestadores11` (`id_estado` ASC, `id_operadora` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores12` (`id_operadora` ASC, `id_estado` ASC, `data_bloqueio` ASC, `id_plano` ASC) VISIBLE,
  INDEX `idxprestadores13` (`id_operadora` ASC, `id_plano` ASC, `id_estado` ASC, `id_cidade` ASC, `bairro` ASC, `id_tipoPrestador` ASC, `id_especialidade` ASC, `id_subespecialidade` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `georref1` (`id_operadora` ASC, `status_georef` ASC, `cpf_cnpj` ASC) VISIBLE,
  INDEX `idxprestadores14` (`id_operadora` ASC, `id_plano` ASC, `id_especialidade` ASC, `id_estado` ASC, `id_cidade` ASC, `id_tipoPrestador` ASC, `data_bloqueio` ASC) VISIBLE,
  INDEX `index15` (`id_operadora` ASC, `id_cidade` ASC, `id_plano` ASC, `bairro` ASC, `latitude` ASC) VISIBLE,
  INDEX `idx_hashendereco` (`hash_endereco` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`prestadores_tmp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`prestadores_tmp` (
  `id_operadora` INT(10) NOT NULL DEFAULT '0',
  `id_especialidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_subespecialidade` INT(10) NOT NULL DEFAULT '0',
  `id_plano` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_tipoPrestador` VARCHAR(10) NOT NULL DEFAULT '0',
  `codigo_legado` VARCHAR(45) NOT NULL DEFAULT ' ',
  `nome` VARCHAR(100) NOT NULL DEFAULT ' ',
  `sexo` VARCHAR(1) NOT NULL DEFAULT ' ',
  `endereco` VARCHAR(100) NOT NULL DEFAULT '  ',
  `numero` VARCHAR(5) NOT NULL DEFAULT ' ',
  `complemento` VARCHAR(50) NOT NULL DEFAULT ' ',
  `bairro` VARCHAR(50) NOT NULL DEFAULT ' ',
  `id_cidade` VARCHAR(10) NOT NULL DEFAULT '0',
  `id_estado` VARCHAR(10) NOT NULL DEFAULT '0',
  `cep` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_primario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `telefone_secundario` VARCHAR(20) NOT NULL DEFAULT ' ',
  `email` VARCHAR(100) NOT NULL DEFAULT ' ',
  `site_url` VARCHAR(50) NOT NULL DEFAULT ' ',
  `nome_logomarca` VARCHAR(10) NULL DEFAULT ' ',
  `latitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `longitude` VARCHAR(20) NOT NULL DEFAULT ' ',
  `cpf_cnpj` VARCHAR(20) NOT NULL DEFAULT ' ',
  `prioridade` VARCHAR(2) NOT NULL DEFAULT ' ',
  `sequencial_endereco` VARCHAR(20) NOT NULL DEFAULT ' ',
  `data_bloqueio` DATETIME NULL DEFAULT NULL,
  `motivo_bloqueio` VARCHAR(255) NULL DEFAULT ' ',
  `operacao` VARCHAR(1) NOT NULL,
  `razao_social` VARCHAR(250) NULL DEFAULT NULL,
  `sigla_conselho_regional` VARCHAR(20) NULL DEFAULT NULL,
  `uf_conselho_regional` VARCHAR(2) NULL DEFAULT NULL,
  `numero_conselho_regional` VARCHAR(15) NULL DEFAULT NULL,
  `facebook` VARCHAR(250) NULL DEFAULT NULL,
  `twitter` VARCHAR(250) NULL DEFAULT NULL,
  `observacoes` VARCHAR(250) NULL DEFAULT NULL,
  `acessibilidade` VARCHAR(1) NULL DEFAULT NULL,
  `atend_24_horas` VARCHAR(1) NULL DEFAULT NULL,
  `detalhe_acessibilidade` VARCHAR(200) NULL DEFAULT NULL,
  `hash_endereco` VARCHAR(250) NULL DEFAULT NULL,
  `nome_responsavel_tecnico` VARCHAR(200) NULL DEFAULT NULL,
  `link_agenda_online` VARCHAR(250) NULL DEFAULT NULL,
  `secao_resultado` VARCHAR(2) NULL DEFAULT NULL,
  `regime_atendimento` VARCHAR(20) NULL DEFAULT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL)
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`produto` (
  `id_produto` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `preco` DOUBLE NOT NULL DEFAULT '0',
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_atualizacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_produto`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`produtos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`produtos` (
  `id_produto` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `exibir` INT(1) NOT NULL,
  `produto` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `descricao` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `link` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `externo` INT(1) NOT NULL,
  `thumb` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  PRIMARY KEY (`id_produto`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`produtos_acessos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`produtos_acessos` (
  `id_acesso` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_produto` INT(10) UNSIGNED NOT NULL,
  `acesso` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL,
  `online` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_acesso`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`protocolo_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`protocolo_log` (
  `id_protocolo_log` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NOT NULL,
  `protocolo` VARCHAR(255) NOT NULL,
  `request` TEXT NULL DEFAULT NULL,
  `response` TEXT NULL DEFAULT NULL,
  `data_registro` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_tipo_protocolo` INT(11) NOT NULL,
  PRIMARY KEY (`id_protocolo_log`),
  INDEX `fk_protocolo_log_operadoras` (`id_cliente` ASC) VISIBLE,
  INDEX `fk_protocolo_log_tipo_protocolo1_idx` (`id_tipo_protocolo` ASC) VISIBLE,
  CONSTRAINT `fk_protocolo_log_operadoras`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_protocolo_log_tipo_protocolo1`
    FOREIGN KEY (`id_tipo_protocolo`)
    REFERENCES `thweb_homologacao`.`tipo_protocolo` (`id_tipo_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`range_protocolo_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`range_protocolo_config` (
  `id_range_protocolo_config` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `range_protocolo_de` INT(11) NULL DEFAULT NULL,
  `range_protocolo_ate` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_range_protocolo_config`),
  INDEX `fk_range_protocolo_ibkf_1_idx` (`id_operadora` ASC) VISIBLE,
  INDEX `fk_range_protocolo_ibkf_2_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `fk_range_protocolo_ibkf_1`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`),
  CONSTRAINT `fk_range_protocolo_ibkf_2`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_aceite_termo_uso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_aceite_termo_uso` (
  `id_termo_uso_reembolso` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` VARCHAR(11) NOT NULL,
  `id_aplicacao` VARCHAR(11) NOT NULL,
  `matricula_beneficiario` VARCHAR(100) NOT NULL DEFAULT '',
  `id_sistema_interno` VARCHAR(50) NULL DEFAULT NULL,
  `data` DATETIME NOT NULL,
  PRIMARY KEY (`id_termo_uso_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_status` (
  `id_status_reembolso` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(100) NULL DEFAULT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `imagem` VARCHAR(200) NULL DEFAULT NULL,
  `cor` VARCHAR(40) NULL DEFAULT NULL,
  `texto_email_beneficiario` LONGTEXT NULL DEFAULT NULL,
  `texto_email_operadora` LONGTEXT NULL DEFAULT NULL,
  `texto_inapp` TEXT NULL DEFAULT NULL,
  `texto_sms` VARCHAR(160) NULL DEFAULT NULL,
  `envia_email_operadora` TINYINT(1) NULL DEFAULT '0',
  `envia_email_beneficiario` TINYINT(1) NOT NULL DEFAULT '1',
  `informa_data_manual` TINYINT(1) NOT NULL DEFAULT '0',
  `envia_notificacao_inapp` TINYINT(1) NOT NULL DEFAULT '0',
  `envia_notificacao_sms` TINYINT(1) NOT NULL DEFAULT '0',
  `exige_valor_reembolsado` TINYINT(1) NOT NULL DEFAULT '0',
  `exige_campo_observacao` TINYINT(1) NOT NULL DEFAULT '0',
  `informa_numero_lote` TINYINT(1) NOT NULL DEFAULT '0',
  `status_solicitado` TINYINT(1) NOT NULL DEFAULT '0',
  `se_aplica_em_despesa` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_status_reembolso`),
  INDEX `fk_reemb_status_aplicacao_1_idx` (`id_aplicacao` ASC) VISIBLE,
  CONSTRAINT `fk_reemb_status_aplicacao_1`
    FOREIGN KEY (`id_aplicacao`)
    REFERENCES `thweb_homologacao`.`aplicacao` (`id_aplicacao`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reembolso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reembolso` (
  `id_reembolso` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_protocolo` INT(11) NOT NULL,
  `id_status_reembolso` INT(11) NOT NULL,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  `numero_lote` VARCHAR(40) NULL DEFAULT NULL,
  `nome_titular` VARCHAR(120) NULL DEFAULT NULL,
  `cpf_titular` VARCHAR(120) NULL DEFAULT NULL,
  `telefone` VARCHAR(45) NULL DEFAULT NULL,
  `data_pagamento` DATE NULL DEFAULT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  `matricula_titular` VARCHAR(100) NULL DEFAULT NULL,
  `matricula_beneficiario` VARCHAR(100) NULL DEFAULT NULL,
  `Id_sistema_interno` VARCHAR(50) NULL DEFAULT NULL,
  `data_solicitacao` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `email_titular` VARCHAR(120) NULL DEFAULT NULL,
  `observacao_interna` TEXT NULL DEFAULT NULL COMMENT 'Observação utilizada apenas pelo analista para controle interno e não é enviada ao beneficiário, seja por email ou pelos aplicativos e portais.',
  PRIMARY KEY (`id_reembolso`),
  INDEX `fk_reembolso_protocolo1_idx` (`id_protocolo` ASC) VISIBLE,
  INDEX `fk_reembolso_reem_status1_idx` (`id_status_reembolso` ASC) VISIBLE,
  CONSTRAINT `fk_reembolso_protocolo1`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`protocolo` (`id_protocolo`),
  CONSTRAINT `fk_reembolso_reem_status1`
    FOREIGN KEY (`id_status_reembolso`)
    REFERENCES `thweb_homologacao`.`reemb_status` (`id_status_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_tipo_reembolso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_tipo_reembolso` (
  `id_tipo_reembolso` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(120) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `deletado_em` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipo_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_despesa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_despesa` (
  `id_despesa_reembolso` INT(11) NOT NULL AUTO_INCREMENT,
  `id_reembolso` INT(11) NOT NULL,
  `id_status_reembolso` INT(11) NOT NULL,
  `id_tipo_reembolso` INT(11) NOT NULL,
  `utilizador_matricula` VARCHAR(120) NOT NULL,
  `utilizador_nome` VARCHAR(120) NOT NULL,
  `data_despesa` DATE NOT NULL,
  `prestador_nome` VARCHAR(120) NULL DEFAULT '',
  `observacao` TEXT NULL DEFAULT NULL,
  `prestador_documento` VARCHAR(120) NOT NULL,
  `valor_despesa` DECIMAL(20,2) NOT NULL,
  `valor_glosado` DECIMAL(20,2) NULL DEFAULT NULL,
  `valor_coparticipacao` DECIMAL(20,2) NULL DEFAULT NULL,
  `valor_uso_indevido` DECIMAL(20,2) NULL DEFAULT NULL,
  `valor_reembolsado` DECIMAL(20,2) NULL DEFAULT NULL,
  `prestador_estado` VARCHAR(40) NULL DEFAULT NULL,
  `prestador_cidade` VARCHAR(255) NULL DEFAULT NULL,
  `observacao_interna` TEXT NULL DEFAULT NULL COMMENT 'Observação utilizada apenas pelo analista para controle interno e não é enviada ao beneficiário, seja por email ou pelos aplicativos e portais.',
  PRIMARY KEY (`id_despesa_reembolso`),
  INDEX `fk_reem_despesa_reembolso_idx` (`id_reembolso` ASC) VISIBLE,
  INDEX `fk_reem_despesa_reem_status1_idx` (`id_status_reembolso` ASC) VISIBLE,
  INDEX `id_tipo_reembolso` (`id_tipo_reembolso` ASC) VISIBLE,
  CONSTRAINT `fk_reem_despesa_reem_status1`
    FOREIGN KEY (`id_status_reembolso`)
    REFERENCES `thweb_homologacao`.`reemb_status` (`id_status_reembolso`),
  CONSTRAINT `fk_reem_despesa_reembolso`
    FOREIGN KEY (`id_reembolso`)
    REFERENCES `thweb_homologacao`.`reembolso` (`id_reembolso`),
  CONSTRAINT `reemb_despesa_ibfk_1`
    FOREIGN KEY (`id_tipo_reembolso`)
    REFERENCES `thweb_homologacao`.`reemb_tipo_reembolso` (`id_tipo_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_arquivos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_arquivos` (
  `id_arquivo` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(120) NULL DEFAULT NULL,
  `diretorio` VARCHAR(120) NULL DEFAULT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  `tamanho` FLOAT NULL DEFAULT NULL,
  `id_despesa_reembolso` INT(11) NOT NULL,
  PRIMARY KEY (`id_arquivo`),
  INDEX `fk_reem_arquivos_reem_despesa1_idx` (`id_despesa_reembolso` ASC) VISIBLE,
  CONSTRAINT `fk_reem_arquivos_reem_despesa1`
    FOREIGN KEY (`id_despesa_reembolso`)
    REFERENCES `thweb_homologacao`.`reemb_despesa` (`id_despesa_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_configuracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_configuracao` (
  `id_config_reembolso` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email_operadora` VARCHAR(200) NULL DEFAULT NULL,
  `telefone_contato` VARCHAR(100) NULL DEFAULT NULL,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `email_nome_remetente` VARCHAR(200) NULL DEFAULT NULL,
  `termo_de_uso` TEXT NULL DEFAULT NULL,
  `local_prestador_obrigatorio` TINYINT(1) NULL DEFAULT '0',
  `url_callback` VARCHAR(255) NULL DEFAULT NULL,
  `callback_authorization` VARCHAR(255) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `callback_email` VARCHAR(255) NULL DEFAULT NULL,
  `data_ultimo_alerta_erros` DATETIME NULL DEFAULT NULL,
  `label_prestador_fornecedor` VARCHAR(45) NULL DEFAULT NULL,
  `label_valor_glosado` VARCHAR(45) NULL DEFAULT NULL,
  `label_coparticipacao` VARCHAR(45) NULL DEFAULT NULL,
  `label_uso_indevido` VARCHAR(45) NULL DEFAULT NULL,
  `label_valor_reembolsado` VARCHAR(45) NULL DEFAULT NULL,
  `terceiro_passo` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_config_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_edu_tipos_reembolso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_edu_tipos_reembolso` (
  `id_reemb_edu_tipos_reembolso` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_reemb_edu_tipos_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_historico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_historico` (
  `id_historico_reembolso` INT(11) NOT NULL AUTO_INCREMENT,
  `id_reembolso` INT(11) NOT NULL,
  `id_status_reembolso` INT(11) NOT NULL,
  `id_usuario_reembolso` INT(11) NULL DEFAULT NULL,
  `data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descricao` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_historico_reembolso`),
  INDEX `fk_reem_historico_reembolso1_idx` (`id_reembolso` ASC) VISIBLE,
  INDEX `fk_reem_historico_reem_status1_idx` (`id_status_reembolso` ASC) VISIBLE,
  CONSTRAINT `fk_reem_historico_reem_status1`
    FOREIGN KEY (`id_status_reembolso`)
    REFERENCES `thweb_homologacao`.`reemb_status` (`id_status_reembolso`),
  CONSTRAINT `fk_reem_historico_reembolso1`
    FOREIGN KEY (`id_reembolso`)
    REFERENCES `thweb_homologacao`.`reembolso` (`id_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`reemb_msg_status_operadora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`reemb_msg_status_operadora` (
  `id_mensagem_reembolso` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  `id_status_reembolso` INT(11) NULL DEFAULT NULL,
  `mensagem` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_mensagem_reembolso`),
  INDEX `id_status_reembolso` (`id_status_reembolso` ASC) VISIBLE,
  CONSTRAINT `reemb_msg_status_operadora_ibfk_1`
    FOREIGN KEY (`id_status_reembolso`)
    REFERENCES `thweb_homologacao`.`reemb_status` (`id_status_reembolso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`regimes_atendimento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`regimes_atendimento` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `codigo_regime` VARCHAR(2) NOT NULL,
  `descricao_regime` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`regioes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`regioes` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(4) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `distancia` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `codigo` (`codigo` ASC) VISIBLE)
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`scan_ftp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`scan_ftp` (
  `id_scan_ftp` INT(11) NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NOT NULL,
  `id_operadora` INT(9) NOT NULL,
  `usuario` VARCHAR(45) NULL DEFAULT NULL,
  `senha` VARCHAR(45) NULL DEFAULT NULL,
  `ativo` INT(1) NOT NULL DEFAULT '1',
  `local` INT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id_scan_ftp`),
  INDEX `fk_operadora_idx` (`id_operadora` ASC) VISIBLE)
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`secoes_resultado_vinculo_ptu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`secoes_resultado_vinculo_ptu` (
  `id_secoes_resultado_vinculo_ptu` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `tipo_vinculo` VARCHAR(5) NOT NULL COMMENT 'Tipo do vinculo que está no arquivo PTU',
  `codigo_secao` VARCHAR(2) NOT NULL,
  PRIMARY KEY (`id_secoes_resultado_vinculo_ptu`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`secoes_resultados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`secoes_resultados` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `codigo_secao` VARCHAR(2) NOT NULL,
  `descricao_secao` VARCHAR(50) NOT NULL,
  `detalhamento_secao` VARCHAR(2000) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = MyISAM

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`setor_brasilia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`setor_brasilia` (
  `id_setor_brasilia` INT(11) NOT NULL,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_setor_brasilia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`setor_detalhe_brasilia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`setor_detalhe_brasilia` (
  `id_setor_detalhe_brasilia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_setor_brasilia` INT(11) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `latitude` DECIMAL(15,9) NOT NULL,
  `longitude` DECIMAL(15,9) NOT NULL,
  PRIMARY KEY (`id_setor_detalhe_brasilia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_grupo_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_grupo_campo` (
  `id_grupo_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `ordem` INT(2) NOT NULL,
  PRIMARY KEY (`id_grupo_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_tipo_guia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_tipo_guia` (
  `id_tipo_guia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_tipo_guia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_campo` (
  `id_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_guia` INT(11) NOT NULL,
  `id_grupo_campo` INT(11) NOT NULL COMMENT 'ID do grupo que o campo pertence',
  `label` VARCHAR(200) NOT NULL,
  `tamanho` INT(2) NOT NULL DEFAULT '12' COMMENT 'DE 1 A 12',
  `valor_esperado` VARCHAR(255) NULL DEFAULT NULL,
  `texto_ajuda` TEXT NULL DEFAULT NULL COMMENT 'Texto descritivo que está ao lado do campo',
  `obrigatorio` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `visivel` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `editavel` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `nome` VARCHAR(255) NOT NULL COMMENT 'valor que fica no name do campo',
  `classe` VARCHAR(100) NULL DEFAULT NULL COMMENT 'CAMPO_CLASSE_CEP => \"cep\"\\nCAMPO_CLASSE_TELEFONE => \"telefone\"\\nCAMPO_CLASSE_CELULAR => \"celular\"\\nCAMPO_CLASSE_EMAIL => \"email\"\\nCAMPO_CLASSE_CPF => \"cpf\'\"',
  `tipo` VARCHAR(100) NOT NULL DEFAULT 'text' COMMENT 'CAMPO_TIPO_TEXT => \"text\"\\nCAMPO_TIPO_EMAIL => \"email\"\\nCAMPO_TIPO_FILE => \"file\"\\nCAMPO_TIPO_OPTION => \"select\"',
  `valor_padrao` VARCHAR(255) NULL DEFAULT NULL,
  `ref_beneficiario` VARCHAR(100) NULL DEFAULT NULL,
  `max_itens` INT(11) NOT NULL DEFAULT '1',
  `validacao_msg` TEXT NULL DEFAULT NULL,
  `validacao_regex` VARCHAR(255) NULL DEFAULT NULL,
  `ordem` INT(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id_campo`),
  INDEX `fk_solic_autorizacoes_campo_id_grupo_campo_idx` (`id_grupo_campo` ASC) VISIBLE,
  INDEX `fk_solic_autorizacoes_campo_id_tipo_guia1_idx` (`id_tipo_guia` ASC) VISIBLE,
  CONSTRAINT `fk_solic_autorizacoes_campo_id_grupo_campo1`
    FOREIGN KEY (`id_grupo_campo`)
    REFERENCES `thweb_homologacao`.`solic_autorizacoes_grupo_campo` (`id_grupo_campo`),
  CONSTRAINT `fk_solic_autorizacoes_campo_id_tipo_guia1`
    FOREIGN KEY (`id_tipo_guia`)
    REFERENCES `thweb_homologacao`.`solic_autorizacoes_tipo_guia` (`id_tipo_guia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_campo_obrigatoriedade_cond`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_campo_obrigatoriedade_cond` (
  `id_campo_obrigatoriedade_cond` INT(11) NOT NULL AUTO_INCREMENT,
  `id_campo` INT(11) NULL DEFAULT NULL,
  `id_campo_obrigatorio` INT(11) NULL DEFAULT NULL,
  `tipo_obrigatoriedade` VARCHAR(75) NULL DEFAULT NULL,
  PRIMARY KEY (`id_campo_obrigatoriedade_cond`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_campo_opcoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_campo_opcoes` (
  `id_campo_opcoes` INT(11) NOT NULL AUTO_INCREMENT,
  `id_campo` INT(11) NOT NULL COMMENT 'ID do campo que a opção pertence',
  `label` VARCHAR(100) NOT NULL,
  `valor` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_campo_opcoes`),
  INDEX `id_campo_idx` (`id_campo` ASC) VISIBLE,
  CONSTRAINT `fk_solic_autorizacoes_campo_opcoes_id_campo1`
    FOREIGN KEY (`id_campo`)
    REFERENCES `thweb_homologacao`.`solic_autorizacoes_campo` (`id_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_config` (
  `id_config` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `email_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `nome_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `email_destinatario` VARCHAR(100) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `texto_email_solicitado_operadora` TEXT NULL DEFAULT NULL,
  `texto_email_solicitado_beneficiario` TEXT NULL DEFAULT NULL,
  `texto_email_aprovado_beneficiario` TEXT NULL DEFAULT NULL,
  `texto_email_recusado_beneficiario` TEXT NULL DEFAULT NULL,
  `css_personalizado` TEXT NULL DEFAULT NULL,
  `email_contato` VARCHAR(100) NULL DEFAULT NULL,
  `telefone` VARCHAR(45) NULL DEFAULT NULL,
  `permite_autorizacao_dependentes` TINYINT(1) NULL DEFAULT '0',
  `assunto_solicitado_operadora` TEXT NULL DEFAULT NULL,
  `assunto_solicitado_beneficiario` TEXT NULL DEFAULT NULL,
  `assunto_aprovado_beneficiario` TEXT NULL DEFAULT NULL,
  `assunto_recusado_beneficiario` TEXT NULL DEFAULT NULL,
  `url_callback` VARCHAR(255) NULL DEFAULT NULL,
  `callback_authorization` VARCHAR(255) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `callback_email` VARCHAR(255) NULL DEFAULT NULL,
  `data_ultimo_alerta_erros` DATETIME NULL DEFAULT NULL,
  `inicio_titulo` VARCHAR(245) NULL DEFAULT NULL,
  `inicio_texto_cabecalho` TEXT NULL DEFAULT NULL,
  `inicio_texto_corpo` TEXT NULL DEFAULT NULL,
  `dependente_solicita_para_dependente` TINYINT(1) NOT NULL DEFAULT '0',
  `dependente_solicita_texto_aviso` TEXT NULL DEFAULT NULL,
  `texto_apresentacao` TEXT NULL DEFAULT NULL,
  `ocultar_protocolo` TINYINT(1) NULL DEFAULT '0',
  PRIMARY KEY (`id_config`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_histor_protocol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_histor_protocol` (
  `id_histor_protocol` INT(11) NOT NULL AUTO_INCREMENT,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  `data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_anterior` TINYINT(1) NULL DEFAULT NULL COMMENT 'NULL;\\nSTATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `status_atual` TINYINT(1) NOT NULL COMMENT 'STATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `protocolo` VARCHAR(100) NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_histor_protocol`),
  INDEX `fk_solic_autorizacoes_histor_protocol_id_ms_usuario_idx` (`id_ms_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_solic_autorizacoes_histor_protocol_id_ms_usuario_1`
    FOREIGN KEY (`id_ms_usuario`)
    REFERENCES `thweb_homologacao`.`ms_usuario` (`id_ms_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_protocolo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_protocolo` (
  `id_protocolo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_tipo_guia` INT(11) NOT NULL,
  `protocolo` VARCHAR(100) NOT NULL,
  `data` DATETIME NOT NULL,
  `id_ms_usuario` INT(11) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'STATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `matricula` VARCHAR(45) NOT NULL,
  `login` VARCHAR(255) NULL DEFAULT NULL,
  `tipo` VARCHAR(1) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  `matricula_titular` VARCHAR(45) NOT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_protocolo`),
  INDEX `fk_solic_autorizacoes_protocolo_id_ms_usuario_idx` (`id_ms_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_solic_autorizacoes_protocolo_id_ms_usuario_1`
    FOREIGN KEY (`id_ms_usuario`)
    REFERENCES `thweb_homologacao`.`ms_usuario` (`id_ms_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`solic_autorizacoes_protocolo_dados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`solic_autorizacoes_protocolo_dados` (
  `id_protocolo_dados` INT(11) NOT NULL AUTO_INCREMENT,
  `id_protocolo` INT(11) NOT NULL,
  `id_campo` INT(11) NOT NULL,
  `valor` TEXT NOT NULL,
  `data_solicitacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `label` VARCHAR(200) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id_protocolo_dados`),
  INDEX `id_protocolo_idx` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `fk_solic_autorizacoes_protocolo_dados_id_protocolo_idx1`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`solic_autorizacoes_protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`sub_especialidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`sub_especialidade` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_legado` VARCHAR(6) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  `id_operadora` INT(11) NOT NULL,
  `cfm` VARCHAR(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sub_esp_operadora` (`id_operadora` ASC, `cod_legado` ASC) VISIBLE,
  CONSTRAINT `fk_sub_esp_operadora`
    FOREIGN KEY (`id_operadora`)
    REFERENCES `thweb_homologacao`.`operadoras` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tabela_particionada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tabela_particionada` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_prestador` INT(11) NOT NULL,
  `dado1` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`, `id_prestador`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1 PARTITION BY LISTE(id_prestador) PARTITIONS 1 ;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tipo_prestador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tipo_prestador` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_operadoras` INT(10) NOT NULL,
  `classe` VARCHAR(10) NOT NULL DEFAULT ' ',
  `descricao` VARCHAR(100) NOT NULL,
  `empresa` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idxtpprestadores1` (`id_operadoras` ASC, `classe` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_grupo_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_grupo_campo` (
  `id_grupo_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `ordem` INT(2) NOT NULL,
  PRIMARY KEY (`id_grupo_campo`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_tipo_guia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_tipo_guia` (
  `id_tipo_guia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `descricao` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_tipo_guia`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_campo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_campo` (
  `id_campo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_guia` INT(11) NOT NULL,
  `id_grupo_campo` INT(11) NOT NULL COMMENT 'ID do grupo que o campo pertence',
  `label` VARCHAR(200) NOT NULL,
  `tamanho` INT(2) NOT NULL DEFAULT '12' COMMENT 'DE 1 A 12',
  `valor_esperado` VARCHAR(255) NULL DEFAULT NULL,
  `texto_ajuda` TEXT NULL DEFAULT NULL COMMENT 'Texto descritivo que está ao lado do campo',
  `obrigatorio` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `visivel` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `editavel` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0 OU 1',
  `nome` VARCHAR(255) NOT NULL COMMENT 'valor que fica no name do campo',
  `classe` VARCHAR(100) NULL DEFAULT NULL COMMENT 'CAMPO_CLASSE_CEP => \"cep\"\\nCAMPO_CLASSE_TELEFONE => \"telefone\"\\nCAMPO_CLASSE_CELULAR => \"celular\"\\nCAMPO_CLASSE_EMAIL => \"email\"\\nCAMPO_CLASSE_CPF => \"cpf\'\"',
  `tipo` VARCHAR(100) NOT NULL DEFAULT 'text' COMMENT 'CAMPO_TIPO_TEXT => \"text\"\\nCAMPO_TIPO_EMAIL => \"email\"\\nCAMPO_TIPO_FILE => \"file\"\\nCAMPO_TIPO_OPTION => \"select\"',
  `valor_padrao` VARCHAR(255) NULL DEFAULT NULL,
  `ref_beneficiario` VARCHAR(100) NULL DEFAULT NULL,
  `max_itens` INT(11) NOT NULL DEFAULT '1',
  `validacao_msg` TEXT NULL DEFAULT NULL,
  `validacao_regex` VARCHAR(255) NULL DEFAULT NULL,
  `ordem` INT(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id_campo`),
  INDEX `fk_tmp_autorizacoes_campo_id_grupo_campo_idx` (`id_grupo_campo` ASC) VISIBLE,
  INDEX `fk_tmp_autorizacoes_campo_id_tipo_guia1_idx` (`id_tipo_guia` ASC) VISIBLE,
  CONSTRAINT `fk_tmp_autorizacoes_campo_id_grupo_campo1`
    FOREIGN KEY (`id_grupo_campo`)
    REFERENCES `thweb_homologacao`.`tmp_autorizacoes_grupo_campo` (`id_grupo_campo`),
  CONSTRAINT `fk_tmp_autorizacoes_campo_id_tipo_guia1`
    FOREIGN KEY (`id_tipo_guia`)
    REFERENCES `thweb_homologacao`.`tmp_autorizacoes_tipo_guia` (`id_tipo_guia`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_campo_obrigatoriedade_cond`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_campo_obrigatoriedade_cond` (
  `id_campo_obrigatoriedade_cond` INT(11) NOT NULL AUTO_INCREMENT,
  `id_campo` INT(11) NULL DEFAULT NULL,
  `id_campo_obrigatorio` INT(11) NULL DEFAULT NULL,
  `tipo_obrigatoriedade` VARCHAR(75) NULL DEFAULT NULL,
  PRIMARY KEY (`id_campo_obrigatoriedade_cond`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_campo_opcoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_campo_opcoes` (
  `id_campo_opcoes` INT(11) NOT NULL AUTO_INCREMENT,
  `id_campo` INT(11) NOT NULL COMMENT 'ID do campo que a opção pertence',
  `label` VARCHAR(100) NOT NULL,
  `valor` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_campo_opcoes`),
  INDEX `id_campo_idx` (`id_campo` ASC) VISIBLE,
  CONSTRAINT `fk_tmp_autorizacoes_campo_opcoes_id_campo1`
    FOREIGN KEY (`id_campo`)
    REFERENCES `thweb_homologacao`.`tmp_autorizacoes_campo` (`id_campo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_config`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_config` (
  `id_config` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `email_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `nome_remetente` VARCHAR(100) NULL DEFAULT NULL,
  `email_destinatario` VARCHAR(100) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `texto_email_solicitado_operadora` TEXT NULL DEFAULT NULL,
  `texto_email_solicitado_beneficiario` TEXT NULL DEFAULT NULL,
  `texto_email_aprovado_beneficiario` TEXT NULL DEFAULT NULL,
  `texto_email_recusado_beneficiario` TEXT NULL DEFAULT NULL,
  `css_personalizado` TEXT NULL DEFAULT NULL,
  `email_contato` VARCHAR(100) NULL DEFAULT NULL,
  `telefone` VARCHAR(45) NULL DEFAULT NULL,
  `permite_autorizacao_dependentes` TINYINT(1) NULL DEFAULT '0',
  `assunto_solicitado_operadora` TEXT NULL DEFAULT NULL,
  `assunto_solicitado_beneficiario` TEXT NULL DEFAULT NULL,
  `assunto_aprovado_beneficiario` TEXT NULL DEFAULT NULL,
  `assunto_recusado_beneficiario` TEXT NULL DEFAULT NULL,
  `url_callback` VARCHAR(255) NULL DEFAULT NULL,
  `callback_authorization` VARCHAR(255) NULL DEFAULT NULL,
  `callback_status` TINYINT(1) NULL DEFAULT NULL,
  `callback_status_msg` TEXT NULL DEFAULT NULL,
  `callback_email` VARCHAR(255) NULL DEFAULT NULL,
  `data_ultimo_alerta_erros` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id_config`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_histor_protocol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_histor_protocol` (
  `id_histor_protocol` INT(11) NOT NULL AUTO_INCREMENT,
  `data` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_anterior` TINYINT(1) NULL DEFAULT NULL COMMENT 'NULL;\\nSTATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `status_atual` TINYINT(1) NOT NULL COMMENT 'STATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `protocolo` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_histor_protocol`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_protocolo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_protocolo` (
  `id_protocolo` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(11) NOT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `id_tipo_guia` INT(11) NOT NULL,
  `protocolo` VARCHAR(100) NOT NULL,
  `data` DATETIME NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'STATUS_SOLICITADO => 0;\\nSTATUS_APROVADO   => 1;\\nSTATUS_RECUSADO   => 2;',
  `matricula` VARCHAR(45) NOT NULL,
  `tipo` VARCHAR(1) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  `matricula_titular` VARCHAR(45) NOT NULL,
  `observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tmp_autorizacoes_protocolo_dados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tmp_autorizacoes_protocolo_dados` (
  `id_protocolo_dados` INT(11) NOT NULL AUTO_INCREMENT,
  `id_protocolo` INT(11) NOT NULL,
  `id_campo` INT(11) NOT NULL,
  `valor` TEXT NOT NULL,
  `data_solicitacao` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `label` VARCHAR(200) NOT NULL,
  `nome` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id_protocolo_dados`),
  INDEX `id_protocolo_idx` (`id_protocolo` ASC) VISIBLE,
  CONSTRAINT `fk_tmp_autorizacoes_protocolo_dados_id_protocolo_idx1`
    FOREIGN KEY (`id_protocolo`)
    REFERENCES `thweb_homologacao`.`tmp_autorizacoes_protocolo` (`id_protocolo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`token_segmento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`token_segmento` (
  `id_token_segmento` INT(11) NOT NULL AUTO_INCREMENT,
  `id_msg_cliente_segmento` INT(11) NULL DEFAULT NULL,
  `id_token` INT(11) NULL DEFAULT NULL,
  `chave` VARCHAR(75) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `valor` VARCHAR(90) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `descricao` VARCHAR(75) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  PRIMARY KEY (`id_token_segmento`),
  INDEX `fk_token_token_segmento_idx` (`id_token` ASC) VISIBLE,
  INDEX `fk_msg_cliente_segmento_idx` (`id_msg_cliente_segmento` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tokens` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `token` VARCHAR(400) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `matricula` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `localizador` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'chave de localização:\\n\\nmatricula\\ntelefone\\nemail\\n\\n\\nO localizador pode ser customizado (futuro)',
  `nome` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `sexo` VARCHAR(1) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `data_nascimento` DATETIME NULL DEFAULT NULL,
  `telefone` VARCHAR(15) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `email` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `estado` VARCHAR(2) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `cidade` VARCHAR(50) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `plataforma` INT(11) NULL DEFAULT NULL,
  `plataforma_versao` VARCHAR(10) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `optout_push` INT(11) NULL DEFAULT '0' COMMENT 'Se tiver como 1, ele NÃO DEVE SER CONSIDERADO no envio nem na simulação',
  `optout_sms` INT(11) NULL DEFAULT '0',
  `optout_email` TINYINT(4) NULL DEFAULT '0',
  `optout_push_motivo` VARCHAR(145) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `optout_sms_motivo` VARCHAR(145) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `optout_email_motivo` VARCHAR(200) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `empresa` VARCHAR(10) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_primeiro_login` DATETIME NULL DEFAULT NULL,
  `gcm_verificado` VARCHAR(1) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `last_gcm_cycle` DATETIME NULL DEFAULT NULL,
  `device_tipo` INT(2) NULL DEFAULT NULL COMMENT '0 = telefone\\n1 = tablet\\n2,3,4,5 = ainda será definido, para outros devices',
  `device_token_anterior` VARCHAR(400) NULL DEFAULT NULL,
  `localizador_anterior` VARCHAR(100) NULL DEFAULT NULL,
  `sns_endpoint` VARCHAR(200) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id` (`id` ASC) VISIBLE,
  INDEX `gettokens` (`id_operadora` ASC, `id_aplicacao` ASC, `localizador` ASC) VISIBLE,
  INDEX `index_operadora_aplicacao` (`id_operadora` ASC, `id_aplicacao` ASC) VISIBLE,
  INDEX `operadora_aplicacao_loc` (`id_operadora` ASC, `id_aplicacao` ASC, `localizador` ASC) VISIBLE,
  INDEX `operadora_aplic_loc_tok` (`id_operadora` ASC, `id_aplicacao` ASC, `localizador` ASC, `token` ASC) VISIBLE,
  INDEX `index_device_token_anterior` (`id_operadora` ASC, `id_aplicacao` ASC, `localizador` ASC, `token` ASC, `device_token_anterior`(255) ASC) VISIBLE,
  INDEX `gettokens2` (`id_operadora` ASC, `id_aplicacao` ASC, `telefone` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tokens_lista_conflito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tokens_lista_conflito` (
  `id_tokens_lista_conflito` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tokens` INT(11) NOT NULL,
  `id_lista_tokens` INT(11) NOT NULL,
  `linha_importada` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL COMMENT 'A linha da importação que contém o conflito',
  PRIMARY KEY (`id_tokens_lista_conflito`),
  INDEX `fk_lista_tokens_conflito_tokens1_idx` (`id_tokens` ASC) VISIBLE,
  INDEX `fk_lista_tokens_conflito_lista_tokens1_idx` (`id_lista_tokens` ASC) VISIBLE,
  CONSTRAINT `fk_lista_tokens_conflito_lista_tokens1`
    FOREIGN KEY (`id_lista_tokens`)
    REFERENCES `thweb_homologacao`.`lista_tokens` (`id_lista_tokens`),
  CONSTRAINT `fk_lista_tokens_conflito_tokens1`
    FOREIGN KEY (`id_tokens`)
    REFERENCES `thweb_homologacao`.`tokens` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tokens_lista_tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tokens_lista_tokens` (
  `id_tokens_lista_tokens` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tokens` INT(11) NOT NULL,
  `id_lista_tokens` INT(11) NOT NULL,
  PRIMARY KEY (`id_tokens_lista_tokens`),
  INDEX `fk_tokens_lista_contato_tokens_idx` (`id_tokens` ASC) VISIBLE,
  INDEX `fk_tokens_lista_contato_lista_contato1_idx` (`id_lista_tokens` ASC) VISIBLE,
  CONSTRAINT `fk_tokens_lista_contato_lista_contato1`
    FOREIGN KEY (`id_lista_tokens`)
    REFERENCES `thweb_homologacao`.`lista_tokens` (`id_lista_tokens`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tokens_lista_contato_tokens`
    FOREIGN KEY (`id_tokens`)
    REFERENCES `thweb_homologacao`.`tokens` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`tokens_removidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`tokens_removidos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `token` VARCHAR(400) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `matricula` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `id_aplicacao` INT(11) NOT NULL,
  `localizador` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL COMMENT 'chave de localização:\\n\\nmatricula\\ntelefone\\nemail\\n\\n\\nO localizador pode ser customizado (futuro)',
  `nome` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `sexo` VARCHAR(1) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `data_nascimento` DATETIME NULL DEFAULT NULL,
  `telefone` VARCHAR(15) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `email` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `estado` VARCHAR(2) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `cidade` VARCHAR(50) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `plataforma` INT(11) NULL DEFAULT NULL,
  `plataforma_versao` VARCHAR(10) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `optout_push` INT(11) NULL DEFAULT '0' COMMENT 'Se tiver como 1, ele NÃO DEVE SER CONSIDERADO no envio nem na simulação',
  `optout_sms` INT(11) NULL DEFAULT '0',
  `optout_email` TINYINT(4) NULL DEFAULT '0',
  `optout_push_motivo` VARCHAR(145) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `optout_sms_motivo` VARCHAR(145) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `optout_email_motivo` VARCHAR(200) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `empresa` VARCHAR(10) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_primeiro_login` DATETIME NULL DEFAULT NULL,
  `gcm_verificado` VARCHAR(1) CHARACTER SET 'latin1' NULL DEFAULT NULL,
  `last_gcm_cycle` DATETIME NULL DEFAULT NULL,
  `device_tipo` INT(2) NULL DEFAULT NULL COMMENT '0 = telefone\\n1 = tablet\\n2,3,4,5 = ainda será definido, para outros devices',
  `device_token_anterior` VARCHAR(400) NULL DEFAULT NULL,
  `localizador_anterior` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id` (`id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`upload_arquivos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`upload_arquivos` (
  `id_upload_arquivo` INT(100) NOT NULL AUTO_INCREMENT,
  `id_operadora` INT(10) NOT NULL,
  `nome_arquivo` VARCHAR(200) NOT NULL,
  `nome_original` VARCHAR(200) NOT NULL,
  `tipo_arquivo` INT(1) NOT NULL,
  `status` INT(1) NOT NULL COMMENT '1 = PENDENTE\\\\n2 = PROCESSADO\\\\n3 = PARCIALMENTE\\\\n4 = REJEITADO\\\\n5 = CANCELADO',
  `referenciado` INT(1) NOT NULL,
  `caminho` TEXT NOT NULL,
  `usuario` VARCHAR(200) NOT NULL,
  `quantidade_registros` INT(10) NULL DEFAULT NULL,
  `quantidade_importado` INT(10) NULL DEFAULT NULL,
  `quantidade_georef` INT(10) NULL DEFAULT NULL,
  `data_recebimento` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_inicio_processamento` TIMESTAMP NULL DEFAULT NULL,
  `data_final_processamento` TIMESTAMP NULL DEFAULT NULL,
  `tamanho_arquivo_bytes` INT(200) NOT NULL,
  `apaga_base_atual` INT(1) NOT NULL,
  `status_livreto` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_upload_arquivo`),
  INDEX `id_operadora` (`id_operadora` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`upload_arquivos_detalhe`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`upload_arquivos_detalhe` (
  `id_detalhe` INT(11) NOT NULL AUTO_INCREMENT,
  `id_upload_arquivo` INT(11) NOT NULL,
  `descricao` VARCHAR(100) NULL DEFAULT NULL,
  `tipo_detalhe` INT(1) NULL DEFAULT NULL,
  `linha` INT(9) NULL DEFAULT NULL,
  `cd_erro` INT(3) NULL DEFAULT NULL,
  `conteudo` VARCHAR(200) NULL DEFAULT NULL,
  `cpf_cnpj` VARCHAR(45) NULL DEFAULT NULL,
  `id_plano` VARCHAR(45) NULL DEFAULT NULL,
  `classe_prestador` VARCHAR(45) NULL DEFAULT NULL,
  `codigo_cbo` VARCHAR(45) NULL DEFAULT NULL,
  `sequencial_endereco` VARCHAR(45) NULL DEFAULT NULL,
  `uf_prestador` VARCHAR(10) NULL DEFAULT NULL,
  `codigo_legado` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_detalhe`),
  INDEX `idx_id_upload_arquivo` (`id_upload_arquivo` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`upload_arquivos_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`upload_arquivos_log` (
  `id_log` INT(11) NOT NULL AUTO_INCREMENT,
  `id_upload_arquivos` INT(11) NOT NULL,
  `qt_erros` INT(11) NULL DEFAULT NULL,
  `qt_advertencia` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_log`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `thweb_homologacao`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `thweb_homologacao`.`usuario` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` INT(11) NULL DEFAULT NULL,
  `login` VARCHAR(45) NULL DEFAULT NULL,
  `contra_assinatura` VARCHAR(20) NULL DEFAULT NULL,
  `assinatura` VARCHAR(20) NULL DEFAULT NULL,
  `senha` VARCHAR(20) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `sexo` VARCHAR(1) NULL DEFAULT NULL COMMENT 'Sexo do beneficário\\nF = Feminino\\nM = Masculino\\n',
  `tipo_beneficiario_id` VARCHAR(4) NULL DEFAULT NULL,
  `tipo_beneficiario_descricao` VARCHAR(50) NULL DEFAULT NULL,
  `grau_parentesco_id` VARCHAR(4) NULL DEFAULT NULL,
  `grau_parentesco_descricao` VARCHAR(50) NULL DEFAULT NULL,
  `convenio_segmentacao` VARCHAR(4) NULL DEFAULT NULL,
  `convenio_tipo_contrato` VARCHAR(4) NULL DEFAULT NULL,
  `bloqueado` VARCHAR(1) NULL DEFAULT NULL COMMENT 'O usuário está bloqueado ?\\n\\nS = Sim\\nN = Não\\nVazio = Não',
  `motivo_bloqueio` VARCHAR(100) NULL DEFAULT NULL,
  `telefone` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `titular` VARCHAR(45) NULL DEFAULT NULL,
  `inclusao` DATETIME NULL DEFAULT NULL,
  `dependencia_id` VARCHAR(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
